%=========================================================================%
%                                                                         %
%                               QUANDALF                                  %
%    QUANtum x Data Analysis tool using Light or Fluoresence microscopy   %
%            by J. Weinacker, P. Kiefer, S. Kalt, and P. Rietz            %
%                                                                         %
%                                   ....                                  %
%                                 .'' .'''                                %
% .                             .'   :                                    %
% \\                          .:    :                                     %
%  \\                        _:    :       ..----.._                      %
%   \\                    .:::.....:::.. .'         ''.                   %
%    \\                 .'  #-. .-######'     #        '.                 %
%     \\                 '.##'/ ' ################       :                %
%      \\  you shall not   #####################         :                %
%       \\    shrink!    ..##.-.#### .''''###'.._        :                %
%        \\             :--:########:            '.    .' :               %
%         \\..__...--.. :--:#######.'   '.         '.     :               %
%         :     :  : : '':'-:'':'::        .         '.  .'               %
%         '---'''..: :    ':    '..'''.      '.        :'                 %
%            \\  :: : :     '      ''''''.     '.      .:                 %
%             \\ ::  : :     '            '.      '      :                %
%              \\::   : :           ....' ..:       '     '.              %
%               \\::  : :    .....####\\ .~~.:.             :             %
%                \\':.:.:.:'#########.===. ~ |.'-.   . '''.. :            %
%                 \\    .'  ########## \ \ _.' '. '-.       '''.          %
%                 :\\  :     ########   \ \      '.  '-.        :         %
%                :  \\'    '   #### :    \ \      :.    '-.      :        %
%               :  .'\\   :'  :     :     \ \       :      '-.    :       %
%              : .'  .\\  '  :      :     :\ \       :        '.   :      %
%              ::   :  \\'  :.      :     : \ \      :          '. :      %
%              ::. :    \\  : :      :    ;  \ \     :           '.:      %
%               : ':    '\\ :  :     :     :  \:\     :        ..'        %
%                  :    ' \\ :        :     ;  \|      :   .'''           %
%                  '.   '  \\:                         :.''               %
%                   .:..... \\:       :            ..''                   %
%                  '._____|'.\\......'''''''.:..'''                       %
%                             \\                                          %
%                                                                         %
% current version: V2.1             last modified: January 09, 2024 by SK %
%=========================================================================%


clear all
close all

addpath(genpath(strcat(pwd,'\functions')));

welcome_to_quandalf();

userinput = Cuserinput();
%%
progressGUI = CprogressGUI();


%% Import of design data
design.Z = importdesign(userinput,progressGUI);
design.Z = repmat(design.Z,userinput.tilingY,userinput.tilingX);
[design.x, design.y] = calculate_xy_design(design.Z, userinput,progressGUI);

%% Import of confocal data 
[measurement_raw.x, measurement_raw.y, measurement_raw.Z] = ...
    importmeasurementdata(userinput,progressGUI);

%% Offset Correction + Coarse Positioning of Confocal Data
measurement_raw.Z  = offsetandtiltcorrection(measurement_raw,progressGUI);

% Remove alignment marks (or dirt particles) on the substrate
measurement_raw.Z = remove_alignment_marks(measurement_raw,design,userinput,progressGUI);

% Determine rough ROI and latheral scling factors
[ROI.coarseposition, x_scale_coarse, y_scale_coarse] = coarsepositioning(measurement_raw, userinput,progressGUI);

% Expand ROI by 100 pixels in each direction
ROI.coarsepositionexpand = ROIexpand(ROI.coarseposition, 100);

plotbeforecoarsepositioning(design, measurement_raw, ROI.coarseposition, userinput,progressGUI);
plotaftercoarsepositioning(design, measurement_raw, ROI.coarsepositionexpand, userinput,progressGUI);

%% Fine Positioning and Roation by Cross Correlation
[ROI.finepositionexpand, rotationangle, x_scaling, y_scaling] = xcorr_finepositioning(design, measurement_raw,...
    ROI.coarsepositionexpand, x_scale_coarse, y_scale_coarse, userinput, progressGUI);
ROI.fineposition = ROIexpand(ROI.finepositionexpand, -100);

measurement_structure = croprotaterescale(ROI.fineposition, rotationangle, x_scaling, y_scaling, measurement_raw, design, progressGUI);
plotafterxcorrpositioning(design, measurement_structure, userinput, progressGUI);

%% Analyze Shrinkage
[shrinkage,z_offset] = analyzeshrinkage(design, measurement_structure, userinput, progressGUI);

%% Calculate and Export Difference Map
difference = calculatedifference(design, measurement_structure, userinput, progressGUI);
plotdifference(measurement_structure, difference, userinput, progressGUI);

%% Create and Plot 1D Cuts
performcutplots(design, measurement_structure, userinput,progressGUI);

%% Process difference data
close all

difference_choice = choosedifferences(difference, measurement_structure, userinput,progressGUI);
difference_mean = calcmeandifference(difference, difference_choice, userinput,progressGUI);

%% Evaluate difference data
[mean_difference, std_difference] = evaluatedifference(difference_mean, measurement_structure, difference_choice, userinput, progressGUI, 'Mean');

%% Close file and save data
savemeandifference(measurement_structure, difference_mean, mean_difference, std_difference, userinput,progressGUI);

savevariables(userinput, measurement_structure, difference, difference_mean,progressGUI);

closeinformationfile(userinput,progressGUI);


%% Filtering of difference data and calculation of new design - interactive or automatic depending on user input

% Convert back to double float type, otherwise inpaint_nans will not work
difference_mean = double(difference_mean);
design.x = double(design.x);
design.y = double(design.y);
design.Z = double(design.Z);
if userinput.interactive_filtering
    update_progressGUI_filteringApp_initial(progressGUI);
    fprintf(1,'Starting Create filtered difference application...\n');
    createfiltereddifference_app(difference_mean, design, userinput);
    update_progressGUI_filteringApp_final(progressGUI);
else
    fprintf(1,'Performing script based automatic filtering...\n');
    difference_filtered = run_filtering_script(difference_mean, design, userinput, progressGUI);
    plot_filtered_difference(difference_mean, difference_filtered, design, userinput, progressGUI);
    previous_design = load_previous_save_fil_diff(difference_filtered, userinput, progressGUI);
    calc_save_new_design(difference_filtered, previous_design, design, userinput, progressGUI);
end