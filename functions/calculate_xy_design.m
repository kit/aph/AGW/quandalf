function [x, y] = calculate_xy_design(design_data, userinput,progressGUI)
%========================================================================== 
%                          CALCULATE XY DESIGN
%==========================================================================
% function to provide arrays for the lateral coordinates of the design
%--------------------------------------------------------------------------
% written for: QUANDALF V1.0
% last modified on: June 03, 2023
% last modified by: S. Kalt
%--------------------------------------------------------------------------
% INPUTS:
% - design_data:    [single-matrix] design data in meter
% - userinput:      [Cuserinput object] collection of all user inputs
% - progressGUI:    [CprogressGUI object] everything GUI-related
%-------------------------------------------------------------------------
% OUTPUTS:
% - x:              [single-matrix] x-values in meter 
% - y:              [single-matrix] y-values in meter 
%==========================================================================
    
    % Update progress GUI
    %----------------------------------------------------------------------
    progressGUI.tool_main           = 'import';
    progressGUI.tool_sub            = 'calc_xy';
    progressGUI.status              = 'calculating xy-coordinates';
    progressGUI.structure           = 0;
    progressGUI.n_structures        = 0;
    progressGUI.pb_global_nfun      = 2;
    progressGUI.pb_local_percentage = 0;
    progressGUI.update();
    %----------------------------------------------------------------------


    [Ny, Nx] = size(design_data); % determine number of pixels in x- and y-direction
    
    % Calculate the x- and y-components of the individual pixels
    pixelsizeX = userinput.designsizeX / Nx;
    pixelsizeY = userinput.designsizeY / Ny;
    x = linspace( -(userinput.designsizeX - pixelsizeX) / 2, (userinput.designsizeX - pixelsizeX) / 2, Nx );
    y = linspace( -(userinput.designsizeY - pixelsizeY) / 2, (userinput.designsizeY - pixelsizeY) / 2, Ny );
    
    % convert to single-precision floating point
    x = single(x);
    y = single(y);


    % Update progress GUI
    %----------------------------------------------------------------------
    progressGUI.pb_local_percentage = 1;
    progressGUI.update();
    %----------------------------------------------------------------------
end

