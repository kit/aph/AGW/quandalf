function [x, y, Z] = importmeasurementdata(userinput,progressGUI)
%========================================================================== 
%                         IMPORT MEASUREMENT DATA
%==========================================================================
% function to import the 2D measurement data
% -------------------------------------------------------------------------
% written for: QUANDALF V1.2
% last modified on: June 03, 2023
% last modified by: Sebastian Kalt
%--------------------------------------------------------------------------
% INPUTS:
% - userinput   [object] class object containing information about design,
%                           exports etc.
% - progressGUI [object] class object containing everything GUI-related
%--------------------------------------------------------------------------
% OUTPUTS:
% - x:        [cell of single-arrays]   x-coordinates of the measurement data
% - y:        [cell of single-arrays]   y-coordinates of the measurement data
% - Z:        [cell of single-matrices] z-coordinates of the measurement data
%==========================================================================

    % Update progress GUI                           
    %----------------------------------------------------------------------
    progressGUI.tool_main           = 'import';
    progressGUI.tool_sub            = 'load_meas';
    progressGUI.status              = 'importing measurement data';
    progressGUI.structure           = 0;
    progressGUI.n_structures        = length(userinput.measurement_file);
    pb_global_nfun_initial          = 3;
    progressGUI.pb_global_nfun      = pb_global_nfun_initial;
    progressGUI.pb_local_percentage = 0;
    progressGUI.update();
    %----------------------------------------------------------------------
    
    


%--------------------------------------------------------------------------
%                 import data from the confocal microscope
%--------------------------------------------------------------------------
    
    % x, y, and Z are now cell arrays. Preallocate them here.
    x = cell(length(userinput.measurement_file),1);
    y = cell(length(userinput.measurement_file),1);
    Z = cell(length(userinput.measurement_file),1);

    % Separation for different data types
    for i = 1:length(userinput.measurement_file)
        if strcmp(userinput.measurement_filename{i}(end-2:end),'sur')

            measurement_data = b_readSur(userinput.measurement_file{i});
            x{i} = measurement_data.xAxis'         * 1e-3;
            y{i} = measurement_data.yAxis'         * 1e-3;
            Z{i} = measurement_data.pointsAligned' * 1e-3;

        elseif strcmp(userinput.measurement_filename{i}(end-2:end),'mat')

            measurement       = load(userinput.measurement_file{i});
            measurement_names = fieldnames(measurement);

            filename_no_file_ext = userinput.measurement_filename{i}(1:end-4);
            measurement_names_str = convertCharsToStrings(measurement_names);
            endings = erase(measurement_names_str,filename_no_file_ext);

            x_index = contains(string(cellfun(@(x) x(1), endings)),'x');
            y_index = contains(string(cellfun(@(x) x(1), endings)),'y');
            z_index = contains(string(cellfun(@(x) x(1), endings)),'z');
            x{i}    = measurement.(cell2mat(measurement_names(find(x_index))));
            y{i}    = measurement.(cell2mat(measurement_names(find(y_index))));
            Z{i}    = measurement.(cell2mat(measurement_names(find(z_index))));

        elseif strcmp(userinput.measurement_filename{i}(end-2:end),'txt')

            fprintf(['if possible, consider using >sur< files instead of >txt<'...
                     ' files to improve the overall performance']);

            delimiterIn = '\t';                    % delimiter of the txt file
            headerlinesIn = 0;                     % ignore input headers
            measurement_data = 1e-6 * importdata(userinput.measurement_file{i}, delimiterIn, headerlinesIn);

            dim_x_exp = find(any(measurement_data(:,2)~=0,2),1)-1;
            dim_y_exp = length(measurement_data)/dim_x_exp;

            x{i} = rawdata_confocal(1:dim_x_exp,1);
            y{i} = rawdata_confocal(1:dim_x_exp:length(measurement_data),2);
            Z{i} = transpose(reshape(measurement_data(:,3),[dim_x_exp,dim_y_exp])) + abs(min(measurement_data(:,3)));

        else
            errordlg(['ERROR: WRONG FILE FORMAT: %s\n' userinput.measurement_file{i}(end-2:end)]);
        end
        
        % Update progress GUI
        %------------------------------------------------------------------
        progressGUI.structure = i;
        progressGUI.pb_local_percentage = i/length(userinput.measurement_file);
        progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
        progressGUI.update();
        %------------------------------------------------------------------

    end
        
   
    %----------------------------------------------------------------------
    %               convert data to single-precision floating point
    %----------------------------------------------------------------------
    
    x = cellfun(@single, x, 'UniformOutput', false);
    y = cellfun(@single, y, 'UniformOutput', false);
    Z = cellfun(@single, Z, 'UniformOutput', false);
        
end