function [shrinkage,z_offset] = analyzeshrinkage(design, measurement_structure, userinput, progressGUI)
%========================================================================== 
%                          SHRINKAGE ANALYSIS 
%==========================================================================
% function to analyze axial and lateral shrinkage of a measured structure
% in comparison to the original design. Please note that enabling the
% shrinkage analysis can significantly increase the run time.
%--------------------------------------------------------------------------
% written for: QUANDALF V1.0
% last modified on: December 19, 2023
% last modified by: Sebastian Kalt
%--------------------------------------------------------------------------
% INPUTS:
% - design:                [struct]  design data
%    - design.x            [vector]  x coordinates of the design data
%    - design.y            [vector]  y coordinates of the design data
%    - design.Z            [matrix]  design data (height profile)
% - measurement_structure: [struct]  corrected measurement data
%    - ...structure.x      [vector]  x coordinates of the measured data
%    - ...structure.y      [vector]  y coordinates of the measured data
%    - ...structure.Z      [matrix]  measured height profile (corrected)
% - userinput              [object]  class object containing information
%                                    about design, exports etc.
% - progressGUI            [object]  containing everything GUI-related
%-------------------------------------------------------------------------
% OUTPUTS:
% - shrinkage:             [struct]  lateral and axial shrinkage
%    - shrinkage.x         [scalar]  lateral shrinkage in x-direction
%    - shrinkage.y         [scalar]  lateral shrinkage in y-direction
%    - shrinkage.z         [scalar]  axial shrinkage (z-direction)
% - z_offset               [scalar]  global z-offset of the structure 
%==========================================================================
    


    % Initialize return variables
    shrinkage.x = cell(length(measurement_structure.Z),1);
    shrinkage.y = cell(length(measurement_structure.Z),1);
    shrinkage.z = cell(length(measurement_structure.Z),1);
    z_offset    = cell(length(measurement_structure.Z),1);

    if userinput.shrinkageanalysis

        % update progress GUI
        %------------------------------------------------------------------
        progressGUI.tool_main           = 'difference';
        progressGUI.tool_sub            = 'shrinkage';
        progressGUI.status              = 'analyzing axial shrinkage';
        progressGUI.structure           = 0;
        progressGUI.n_structures        = length(measurement_structure.Z);
        progressGUI.pb_local_percentage = 0;
        pb_global_nfun_initial          = 12;
        progressGUI.pb_global_nfun      = pb_global_nfun_initial;
        progressGUI.update();
        %------------------------------------------------------------------

        for i = 1:length(measurement_structure.Z)


        %---------------------------------------------------------------------%
        %                     calculate lateral shrinkage                     %
        %---------------------------------------------------------------------%
            % will not be added before Quandalf V3.0
            shrinkage.x{i} = 0.0;
            shrinkage.y{i} = 0.0;
        
            
            
        %---------------------------------------------------------------------%
        %                      calculate axial shrinkage                      %
        %---------------------------------------------------------------------%
        
        % calculate axial shrinkage:
        %----------------------------------------------------------------------
            measurement_data_Z = measurement_structure.Z{i};

            % sort the design data
            [theo_heights_sorted, index] = sort(design.Z(:));
        
            % Find unique rows and corresponding indices
            shrinkage_plot_xdata = theo_heights_sorted;
            shrinkage_plot_ydata = measurement_data_Z(index);
        
            % calculate the axial shrinkage by fitting a linear model
            max_height_x = max(design.Z(:));
            max_height_y = max(measurement_data_Z(:));
            
            cFit = fit(double(theo_heights_sorted),double(measurement_data_Z(index)),'poly1');
            shrinkage.z{i} = (1-cFit.p1)*100;
            z_offset{i} = cFit(0);
            
        
        % calculate a point cloud for visualizing the 
        %----------------------------------------------------------------------
            point_cloud = sortrows([shrinkage_plot_xdata,shrinkage_plot_ydata]);
            [unique_point_cloud,~] = unique(point_cloud, 'rows');
        
        
        % plot axial shrinkage:
        %----------------------------------------------------------------------
        
            if userinput.shrinkageplot
        
                f_shrinkage = figure('Color','w','WindowStyle','normal','Position',progressGUI.figuresize_small,'Name','Shrinkage');
        
                % Create scatter plot
                %--------------------------------------------------------------
                binscatter(unique_point_cloud(1:5:end, 1).*10^6, unique_point_cloud(1:5:end, 2).*10^6); hold on;%, 2, cMatrix, 'filled'); hold on;
                
                plot([0 max_height_x.*10^6],cFit.p1.*[0 max_height_x.*10^6]+cFit.p2.*10^6,'-','LineWidth',2.0,'Color',[252/255, 186/255, 0/255]); hold on;
                plot([0 max_height_x.*10^6],[0 max_height_x.*10^6],'-','LineWidth',2.0,'Color',[255/255, 56/255, 20/255]);
        
                % plot settings:
                %--------------------------------------------------------------
                grid 'on';
                box on;
                xlim([0 max_height_x.*10^6]);
                ylim([0 max_height_y.*10^6]);
                pbaspect([1 1 1]);
                xlabel('Designed Height / µm');
                ylabel('Measured Height / µm');
                legend({'Measured Data','Linear Fit','Ideal Slope'},'Location','northwest');
                h = colorbar;
                ylabel(h, 'Occurrence');
                h.FontSize = 12;    
                

                % print axial shrinkage and add it to the plot
                %--------------------------------------------------------------
                fprintf(1,'axial shrinkage: %1.1f%%\n',shrinkage.z{i});
                text(0.05*max_height_x.*10^6,0.75*max_height_y.*10^6,sprintf('axial shrinkage: %1.2f%%\n z offset: %1.2f µm',shrinkage.z{i},z_offset{i}*10^6),'BackgroundColor','w','LineWidth',1.0,'FontSize',10,'EdgeColor',[0 0 0]);
        
                % export axial shrinkage plot
                %--------------------------------------------------------------
                if userinput.saveplots
                    exportgraphics(f_shrinkage,strcat(userinput.exportpath,userinput.analysis_filename{i},'_plot_shrinkage.png'),'BackgroundColor','w');
                end
            end

            % Update progress GUI
            %--------------------------------------------------------------
            progressGUI.structure = i;
            progressGUI.pb_local_percentage = i/length(measurement_structure.Z);
            progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
            progressGUI.update();
            %--------------------------------------------------------------

        end
    else
         
        % update progress GUI
        %------------------------------------------------------------------
        progressGUI.tool_main           = 'difference';
        progressGUI.tool_sub            = 'shrinkage';
        progressGUI.status              = 'skipping axial shrinkage analysis';
        progressGUI.structure           = 0;
        progressGUI.n_structures        = length(measurement_structure.Z);
        progressGUI.pb_local_percentage = 0;
        pb_global_nfun_initial          = 12;
        progressGUI.pb_global_nfun      = pb_global_nfun_initial;
        progressGUI.update();
        %------------------------------------------------------------------

        for i = 1:length(measurement_structure.Z)
            shrinkage.x{i} = NaN;
            shrinkage.y{i} = NaN;
            shrinkage.z{i} = NaN;
            z_offset{i} = NaN;
        end
    end
 
end