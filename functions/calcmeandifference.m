function difference_mean = calcmeandifference(difference, difference_choice, userinput, progressGUI)
%========================================================================== 
%                           DIFFERENCE MEAN
%==========================================================================
% This function calculates the mean of the absolute difference values. 
%--------------------------------------------------------------------------
% written for: QUANDALF V1.3
% last modified on: June 06, 2023
% last modified by: S. Kalt
%--------------------------------------------------------------------------
% INPUTS:
% - difference:            [cell-array]  differences for all measurements
% - difference_choice:     [logical array]  choices made by the user for
%                                       using or ignoring measuremnts
% - userinput:             [object] class object containing information
%                                    about design, exports etc.
% - progressGUI:           [class object] containing everything GUI-related
%-------------------------------------------------------------------------
% OUTPUTS:
% - difference_mean:       [single-array]  resulting averaged difference data
%==========================================================================


    % update progress GUI
    %----------------------------------------------------------------------
    progressGUI.tool_main           = 'difference';
    progressGUI.tool_sub            = 'sel_eval_diff';
    progressGUI.status              = 'calculating mean difference';
    progressGUI.structure           = 0;
    progressGUI.n_structures        = 0;
    progressGUI.pb_local_percentage = 0;
    pb_global_nfun_initial          = 17;
    progressGUI.pb_global_nfun      = pb_global_nfun_initial;
    progressGUI.update();
    %----------------------------------------------------------------------    

    % Pick only the selected difference data (given by user before)
    difference_select = difference(difference_choice);

    % Calculate the average of all the selected differences
    difference_mean = sum(cat(3,difference_select{:}),3) ./ length(difference_select);

    % Write general information about used files and general numbers in
    % separate txt file for documentation
    if userinput.saveplots
        fprintf(userinput.generaloutputfileID, "General outputs for all selected measurement data \n\n");
        fprintf(userinput.generaloutputfileID, "Selected data sets: \n");
    
        for i = 1:length(difference_choice)
    
            if difference_choice(i)
                usecase = 'selected';
            else
                usecase = 'excluded';
            end
    
            fprintf(userinput.generaloutputfileID, '%s: %s \n', userinput.analysis_filename{i}, usecase);
        end
        fprintf(userinput.generaloutputfileID, '\n');
    end

    % Update progress GUI
    %----------------------------------------------------------------------
    progressGUI.pb_local_percentage = 1;
    progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
    progressGUI.update();
    %----------------------------------------------------------------------


end