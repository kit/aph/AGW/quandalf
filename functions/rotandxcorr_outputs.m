function [x_shift_cross, y_shift_cross] = rotandxcorr_outputs(design_Z_resized, measurement_Z, xcorrsize_design, xcorrsize_measurement, angle, maximumshift, gpu_enable)
%========================================================================== 
%                       ROT AND XCORR OUTPUTS
%==========================================================================
% Rotates the measurement about the given angle and calculates the
% cross-correlation with the design. The output is the the shift of the two
% images. 
%--------------------------------------------------------------------------
% written for: QUANDALF V1.1
% last modified on: March 08, 2023
% last modified by: Jannis Weinacker
%--------------------------------------------------------------------------
% INPUTS:
% - design_Z_resized:            [single-matrix]  design data
% - measurement_Z:               [single-matrix]  measurement data
% - xcorrsize_design:            [double]  number of pixels to cross
%                                        correlate in design
% - xcorrsize_measurement:       [double]  number of pixels to cross
%                                        correlate in measurement
% - angle:                       [double]  rotation angle
% - maximumshift:                [int] 2x size of the cross correlation
%                                        function and therefore the maximum 
%                                        displacement that is considered
% - userinput:                   [object] class object containing information
%                                        about design, exports etc.
%-------------------------------------------------------------------------
% OUTPUTS:
% - x_shift_cross:               [int]  x-coordinate of the shift of the 
%                                           two images
% - y_shift_cross:               [int]  y-coordinate of the shift of the 
%                                           two images
%==========================================================================

    % Create a region of interest for the measurement data (rectangle)
    crosscorr_ROI_exp =...
        [round(size(design_Z_resized,2)/2)-round(xcorrsize_measurement/2)... x-coordinate of the upper left corner
        round(size(design_Z_resized,1)/2)-round(xcorrsize_measurement/2)...  y-coordinate of the upper left corner
        xcorrsize_measurement...                                             size of the rectangle (pixels)
        xcorrsize_measurement];                                            % size of the rectangle (pixels)
    
    % Create a region of interest for the design data (rectangle)
    crosscorr_ROI_theo =...
        [round(size(design_Z_resized,2)/2)-round(xcorrsize_design/2)... x-coordinate of the upper left corner
        round(size(design_Z_resized,1)/2)-round(xcorrsize_design/2)...  y-coordinate of the upper left corner
        xcorrsize_design...                                             size of the rectangle (pixels)
        xcorrsize_design];                                            % size of the rectangle (pixels)

    % Rotate the measurement data about the angle and crop it to the ROI
    measurement_Z_crosscrop = imcrop(imrotate(measurement_Z,angle,'crop'),crosscorr_ROI_exp);      
    % Crop the design data to the ROI
    design_Z_resized_crosscrop = imcrop(design_Z_resized,crosscorr_ROI_theo); 
    
    % Copy data to GPU if set by user
    if gpu_enable
        measurement_Z_crosscrop    = gpuArray(measurement_Z_crosscrop); 
        design_Z_resized_crosscrop = gpuArray(design_Z_resized_crosscrop);
    end
    
    % Normalize design and measurement
    measurement_Z_crosscrop    = measurement_Z_crosscrop    ./ max(measurement_Z_crosscrop(:));
    design_Z_resized_crosscrop = design_Z_resized_crosscrop ./ max(design_Z_resized_crosscrop(:));
    
    % Pad array with zero to control the size of the calculated cross
    % correlation
    measurement_Z_crosscrop = padarray(measurement_Z_crosscrop, [maximumshift maximumshift]);

    % perform xcorr (xcorr2_small acts like xcorr2 for smaller
    % displacements)
    XCorr = xcorr2_small(measurement_Z_crosscrop, design_Z_resized_crosscrop) / sum(sum(design_Z_resized_crosscrop.^2));
    
    % Determine the shift between the two images from the cross-correlation
    [~, imax] = max(abs(XCorr(:)));
    [ypeak, xpeak] = ind2sub(size(XCorr),imax(1));
    x_shift_cross = xpeak - maximumshift - 1;
    y_shift_cross = ypeak - maximumshift - 1;
%     corr_offset = [(xpeak-size(design_Z_resized_crosscrop,2)) (ypeak-size(design_Z_resized_crosscrop,1))];
%     
%     x_shift_cross = gather( corr_offset(1) - (crosscorr_ROI_theo(1) - crosscorr_ROI_exp(1)) );
%     y_shift_cross = gather( corr_offset(2) - (crosscorr_ROI_theo(2) - crosscorr_ROI_exp(2)) );
    
end