function [prev] = load_previous_save_fil_diff(difference_filtered, userinput, progressGUI)
%========================================================================== 
%                     <LOAD_PREVIOUS_SAVE_FIL_DIFF>
%==========================================================================
% Loads previous design as .mat file and saves the filtered difference data
% as .mat file
%--------------------------------------------------------------------------
% written for: QUANDALF V1.x
% last modified on: June 07, 2023
% last modified by: S. Kalt
%--------------------------------------------------------------------------
% INPUTS:
% - difference_filtered:  [double-matrix]  filtered difference data
% - userinput:            [object]         class object containing parameters 
%                                          about design, export etc
% - progressGUI:          [class object] containing everything GUI-related
%--------------------------------------------------------------------------
% OUTPUTS:
% - prev:                 [double-matrix]  previous design data
%==========================================================================
    
    % update progress GUI
    %----------------------------------------------------------------------
    progressGUI.tool_main           = 'filtering';
    progressGUI.tool_sub            = 'load_previous';
    progressGUI.status              = 'select previous design';
    progressGUI.status_message.FontColor = [1 0 0];
    progressGUI.structure           = 0;
    progressGUI.n_structures        = 0;
    progressGUI.pb_local_percentage = 0;
    pb_global_nfun_initial          = 24;
    progressGUI.pb_global_nfun      = pb_global_nfun_initial;
    progressGUI.update();
    %----------------------------------------------------------------------


    fprintf(1,'Loading previous design for compensation...\n');
    
    % Load previous design data to compensate as .mat file

    importui_size = [progressGUI.guisize(1),progressGUI.guisize(2)-160,progressGUI.guisize(3),120];
    importui = uifigure('Name','Select previous design','Position',importui_size ,'WindowStyle','modal');
    importui.Icon = 'images\Q_logo.png';
    continue_button = uibutton(importui,"state","Position",[200 20 200 30],"Text","Continue","FontWeight","bold","BackgroundColor",[167/255 199/255 231/255],"FontSize",13,"Value",false,"Enable","off");

    prev_design_path_lb     = uilabel(importui,"Position",[18,90,280,30],"Text","Path to previous design file:","FontWeight",'bold',"VerticalAlignment",'center',"HorizontalAlignment",'left');
    prev_design_path_file   = uieditfield(importui,"Text","Position",[15 65 450 25],"Value","");
    prev_design_path_button = uibutton(importui,"Position",[470 65 110 25],"Text","Browse","FontWeight",'bold',"BackgroundColor",[0.9 0.9 0.9],"ButtonPushedFcn", @(prev_design_path_button,event) BrowseButtonPushed(prev_design_path_file,'',importui,'file','previous design',{'*.mat'},'','','',continue_button));

    waitfor(continue_button,'Value')

    prev_file = prev_design_path_file.Value;
    close(importui)
    
    % update progress GUI
    %----------------------------------------------------------------------
    progressGUI.tool_main           = 'filtering';
    progressGUI.tool_sub            = 'load_previous';
    progressGUI.status              = 'importing previous design';
    progressGUI.status_message.FontColor = [0 0 0];
    %----------------------------------------------------------------------
    

    
    prev_load = load(prev_file);
    prev_field = fieldnames(prev_load);
    prev = prev_load.(prev_field{1});
    
    fprintf(1,'...finished\n');

    % Update progress GUI
    %----------------------------------------------------------------------
    progressGUI.pb_local_percentage = 1;
    progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
    progressGUI.update();
    %----------------------------------------------------------------------

    % Save filtered difference as a .mat file
    
    % update progress GUI
    %----------------------------------------------------------------------
    progressGUI.tool_main           = 'filtering';
    progressGUI.tool_sub            = 'save_filtered';
    progressGUI.status              = 'saving filtered difference data';
    progressGUI.structure           = 0;
    progressGUI.n_structures        = 0;
    progressGUI.pb_local_percentage = 0;
    pb_global_nfun_initial          = 25;
    progressGUI.pb_global_nfun      = pb_global_nfun_initial;
    progressGUI.update();
    %----------------------------------------------------------------------


    fprintf(1, 'Saving filtered difference...\n');
    
    array_to_save = difference_filtered;
    
    if iscell(userinput.analysis_filename)
        save(strcat(userinput.exportpath,userinput.analysis_filename{1},'_difference_filtered.mat'), 'array_to_save');
    else
        save(strcat(userinput.exportpath,userinput.analysis_filename,'_difference_filtered.mat'), 'array_to_save');
    end
    
    fprintf(1, '...finished\n');

    % Update progress GUI
    %----------------------------------------------------------------------
    progressGUI.pb_local_percentage = 1;
    progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
    progressGUI.update();
    %----------------------------------------------------------------------
end









