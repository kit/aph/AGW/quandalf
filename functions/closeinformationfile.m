function closeinformationfile(userinput, progressGUI)
    
    % update progress GUI
    %----------------------------------------------------------------------
    progressGUI.tool_main           = 'difference';
    progressGUI.tool_sub            = 'save_diff';
    progressGUI.status              = 'closing information file';
    progressGUI.structure           = 0;
    progressGUI.n_structures        = 0;
    progressGUI.pb_local_percentage = 0;
    pb_global_nfun_initial          = 21;
    progressGUI.pb_global_nfun      = pb_global_nfun_initial;
    progressGUI.update();
    %----------------------------------------------------------------------
    
    if userinput.saveplots
        
        arrayfun(@fclose, userinput.informationfileID);
        fclose(userinput.generaloutputfileID);
        
    end
    
    

end