function savevariables(userinput, measurement_structure, difference, difference_mean, progressGUI)
    


    % update progress GUI
    %----------------------------------------------------------------------
    progressGUI.tool_main           = 'difference';
    progressGUI.tool_sub            = 'save_diff';
    progressGUI.status              = 'saving processed measurement data';
    progressGUI.structure           = 0;
    progressGUI.n_structures        = length(measurement_structure.Z);
    progressGUI.pb_local_percentage = 0;
    pb_global_nfun_initial          = 20;
    progressGUI.pb_global_nfun      = pb_global_nfun_initial;
    progressGUI.update();
    %----------------------------------------------------------------------

    if userinput.saveplots

        N = length(measurement_structure.Z);
        for i = 1:N

            measurement_structure_temp.x = measurement_structure.x{i};
            measurement_structure_temp.y = measurement_structure.y{i};
            measurement_structure_temp.Z = measurement_structure.Z{i};

            difference_temp = difference{i};
            
             % Update progress GUI
            %--------------------------------------------------------------
            progressGUI.status = 'saving measurement data to .mat-file';
            progressGUI.structure = i;
            progressGUI.update();
            %--------------------------------------------------------------

            save(strcat(userinput.exportpath,userinput.analysis_filename{i},'_data.mat'), 'measurement_structure_temp');
            
            % Update progress GUI
            %--------------------------------------------------------------
            progressGUI.status = 'saving difference data to .mat-file';
            progressGUI.pb_local_percentage = i/3/N;
            progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
            progressGUI.update();
            %--------------------------------------------------------------
                        
            save(strcat(userinput.exportpath,userinput.analysis_filename{i},'_data.mat'), 'difference_temp', '-append');
            
            % Update progress GUI
            %--------------------------------------------------------------
            progressGUI.status = 'saving mean difference data to .mat-file';
            progressGUI.pb_local_percentage = 2*i/3/N;
            progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
            progressGUI.update();
            %--------------------------------------------------------------
            
            save(strcat(userinput.exportpath,userinput.analysis_filename{i},'_data.mat'), 'difference_mean', '-append');

            % Update progress GUI
            %--------------------------------------------------------------
            progressGUI.pb_local_percentage = i/N;
            progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
            progressGUI.update();
            %--------------------------------------------------------------
        
        end
        
        % Update progress GUI
        %--------------------------------------------------------------
        progressGUI.status = 'all data sets saved';
        progressGUI.update();
        %--------------------------------------------------------------

    else

        % Update progress GUI
        %------------------------------------------------------------------
        progressGUI.status = 'skipping difference plot export';
        progressGUI.pb_local_percentage = 1;
        progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
        progressGUI.update();
        %------------------------------------------------------------------


    end
    
end