function difference = calculatedifference(design, measurement_structure, userinput,progressGUI)
%========================================================================== 
%                       CALCULATE DIFFERENCE
%==========================================================================
% Here the difference between the measurements and the design is
% calculated. The design is shifted so that there is no constant offset
% remaining and the difference data is centered around 0 µm.
%--------------------------------------------------------------------------
% written for: QUANDALF V1.3
% last modified on: June 06, 2023
% last modified by: S. Kalt
%--------------------------------------------------------------------------
% INPUTS:
% - design:                 [struct, single-arrays]  design data
% - measurement_structure:  [struct, single-arrays]  measurement data
% - userinput               [object] class object containing information
%                                    about design, exports etc.
% - progressGUI:            [class object] containing everything GUI-related
%-------------------------------------------------------------------------
% OUTPUTS:
% - difference:             [cell-array]  differences for each measurement
%                                           to design
%==========================================================================
    
    % update progress GUI
    %----------------------------------------------------------------------
    progressGUI.tool_main           = 'difference';
    progressGUI.tool_sub            = 'calc_diff';
    progressGUI.status              = 'calculating difference data';
    progressGUI.structure           = 0;
    progressGUI.n_structures        = length(measurement_structure.Z);
    progressGUI.pb_local_percentage = 0;
    pb_global_nfun_initial          = 13;
    progressGUI.pb_global_nfun      = pb_global_nfun_initial;
    progressGUI.update();
    %----------------------------------------------------------------------

    % Empty cell array for all the difference data
    difference = cell ( length ( measurement_structure.Z ), 1 );
    
    % Loop through all measurement data sets
    for i = 1:length(measurement_structure.Z)

        if userinput.design_z_shift 

            % Resolution (pixelsize) of measurement data
            x_res = mean(diff(measurement_structure.x{i}));
            y_res = mean(diff(measurement_structure.y{i}));

            % Indizes of center positions
            x_meas_center_index = round(length(measurement_structure.x{i})/2);
            y_meas_center_index = round(length(measurement_structure.y{i})/2); 

            % Calculate height difference of design and measurement

            range_height_diff = 30e-6; % Size of the ROI (square) in the centre for offset calculation in m
            range_x = round(range_height_diff/x_res); % size of the square in pixels in x-direction
            range_y = round(range_height_diff/y_res); % size of the square in pixels in y-direction
            
            % if the structure is smaller than the ROI take structure
            % size
            if range_x > size(design.Z,2)
                range_x = size(design.Z,2);
            end
            if range_y > size(design.Z,1)
                range_y = size(design.Z,1);
            end

            % Calculate the average height of the measurement in the ROI
            height_meas = mean(measurement_structure.Z{i}(...
                y_meas_center_index - range_y : y_meas_center_index + range_y, ...
                x_meas_center_index - range_x : x_meas_center_index + range_x...
                ), 'all');
            % Calculate the average height of the design in the ROI
            height_design = mean(design.Z(...
                y_meas_center_index - range_y : y_meas_center_index + range_y, ...
                x_meas_center_index - range_x : x_meas_center_index + range_x...
                ), 'all');
            % Find the differnece between the two heights
            height_difference = height_meas - height_design; % Positive values -> measurement is higher than design

            % Offset the design, so that it fits better to the measurement
            design_difference = design.Z + height_difference;

        else 

            design_difference = design.Z;

        end

        % Calculate the difference between the design and the measurement
        % (pixelwise). The difference is minimized due to the design shift.
        difference{i} = measurement_structure.Z{i} - design_difference;


        % Update progress GUI
        %------------------------------------------------------------------
        progressGUI.structure = i;
        progressGUI.pb_local_percentage = i/length(measurement_structure.Z);
        progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
        progressGUI.update();
        %------------------------------------------------------------------

    end

end