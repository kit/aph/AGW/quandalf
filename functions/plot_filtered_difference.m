function plot_filtered_difference(difference_mean, difference_filtered, design, userinput, progressGUI)
%========================================================================== 
%                       <PLOT_FILTERED_DIFFERENCE>
%==========================================================================
% Plot the 2D filtered difference as well as two 1D cuts at locations close
% to the center
%--------------------------------------------------------------------------
% written for: QUANDALF V1.x
% last modified on: June 07, 2023
% last modified by: S. Kalt
%--------------------------------------------------------------------------
% INPUTS:
% - difference_mean:      [double_matrix]  initial difference data
% - difference_filtered:  [double-matrix]  filtered difference data
% - design:               [struct]         design data
% - userinput:            [object]         class object containing parameters 
%                                          about design, export etc
% - progressGUI:          [class object] containing everything GUI-related
%--------------------------------------------------------------------------
% OUTPUTS:
%==========================================================================

    if userinput.generalplots
        
        % update progress GUI
        %----------------------------------------------------------------------
        progressGUI.tool_main           = 'filtering';
        progressGUI.tool_sub            = 'plot_filtered';
        progressGUI.status              = 'plotting filtered difference data';
        progressGUI.structure           = 0;
        progressGUI.n_structures        = 0;
        progressGUI.pb_local_percentage = 0;
        pb_global_nfun_initial          = 23;
        progressGUI.pb_global_nfun      = pb_global_nfun_initial;
        progressGUI.update();
        %----------------------------------------------------------------------

        plot_offset = 2e-6;
        px_sz = userinput.designsizeX/numel(design.x);
        plot_offset_px = int64(plot_offset/px_sz);
    
        % Plot the filtered difference
        hfig1_2D_diff = figure('Color','w','Position',progressGUI.figuresize_small);
        ax = gca;
        imagesc(ax, design.x*1e3,design.y*1e3,difference_filtered*1e6);
        axis(ax, 'image');
        h = colorbar(ax);
        h.Label.String = '{\it z} / \mum';   
        xlabel(ax, '{\it x} / mm');
        ylabel(ax, '{\it y} / mm');
        title(ax, 'Filtered Difference');
        box(ax, 'on');
        
        % Plot 1D Cuts - Compare with unfiltered difference
        hfig_1D_x = figure('Color','w','Position',progressGUI.figuresize_small);
        ax = gca;
        plot(ax, design.x*1e3,1e6*difference_mean(plot_offset_px+round(size(difference_mean,1)/2),:),'LineWidth',1.5);
        hold (ax, 'on'); 
        plot(ax, design.x*1e3,1e6*difference_filtered(plot_offset_px+round(size(difference_mean,1)/2),:),'LineWidth',1.5);
        xlabel(ax, '{\it x} (mm)');
        ylabel(ax, 'Measured - Designed Height (\mum)');
        xlim(ax, [design.x(1)*1e3 design.x(end)*1e3]);
        legend(ax, 'initial', 'filtered'); 
        title(ax, 'X cut'); 
        hold(ax, 'off'); 
        box(ax, 'on');
        
        % Plot 1D Cuts - Compare with unfiltered difference
        hfig_1D_y = figure('Color','w','Position',progressGUI.figuresize_small);
        ax = gca;
        plot(ax, design.y*1e3,1e6*difference_mean(:,plot_offset_px+round(size(difference_mean,1)/2)),'LineWidth',1.5);
        hold (ax, 'on');
        plot(ax, design.y*1e3,1e6*difference_filtered(:,plot_offset_px+round(size(difference_mean,1)/2)),'LineWidth',1.5);
        xlabel(ax, '{\it y} (mm)');
        ylabel(ax, 'Measured - Designed Height (\mum)');
        xlim(ax, [design.y(1)*1e3 design.y(end)*1e3]);
        legend(ax, 'initial', 'filtered');   
        title(ax, 'Y cut'); 
        hold (ax, 'off');
        box(ax, 'on');
        
        if userinput.saveplots
            exportgraphics(hfig1_2D_diff,strcat(userinput.exportpath,userinput.analysis_filename{1},'_plot_difference_filtered.png'),'BackgroundColor','w');
            exportgraphics(hfig_1D_x,strcat(userinput.exportpath,userinput.analysis_filename{1},'_plot_1D_x_filtering.png'),'BackgroundColor','w');
            exportgraphics(hfig_1D_y,strcat(userinput.exportpath,userinput.analysis_filename{1},'_plot_1D_y_filtering.png'),'BackgroundColor','w');
        end   

        % Update progress GUI
        %----------------------------------------------------------------------
        progressGUI.pb_local_percentage = 1;
        progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
        progressGUI.update();
        %----------------------------------------------------------------------

    else        

        % update progress GUI
        %------------------------------------------------------------------
        progressGUI.tool_main           = 'filtering';
        progressGUI.tool_sub            = 'plot_filtered';
        progressGUI.status              = 'skipping filtered difference plot';
        progressGUI.structure           = 0;
        progressGUI.n_structures        = 0;
        progressGUI.pb_local_percentage = 0;
        pb_global_nfun_initial          = 23;
        progressGUI.pb_global_nfun      = pb_global_nfun_initial;
        progressGUI.update();
        %------------------------------------------------------------------
        
        fprintf(1,'Skipping difference plot due to user decision.\n')

        % Update progress GUI
        %----------------------------------------------------------------------
        progressGUI.pb_local_percentage = 1;
        progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
        progressGUI.update();
        %----------------------------------------------------------------------
    end

end