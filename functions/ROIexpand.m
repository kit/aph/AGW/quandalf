function ROIexpand = ROIexpand(ROIcoarseposition, expandsize)
%========================================================================== 
%                          ROI EXPANSION
%==========================================================================
% Expands ROI by number of px.
%--------------------------------------------------------------------------
% written for: QUANDALF V1.2
% last modified on: February 22, 2023
% last modified by: Pascal Kiefer
%--------------------------------------------------------------------------
% INPUTS:
% - ROI.coarseposition:   [cell of double matrices] input ROI
% - expandsize:           [double] expand size in px
%--------------------------------------------------------------------------
% OUTPUTS:
% - ROIexpand:              [cell of double matrices] output ROI
%==========================================================================
   
    ROIexpand = cellfun(@(x) x + [-expandsize -expandsize 2*expandsize 2*expandsize], ROIcoarseposition, 'UniformOutput',false);

end