function [x_shift_cross, y_shift_cross] = rotandxcorr_dft_outputs(design_Z_resized, measurement_Z, xcorrsize_design, xcorrsize_measurement, angle, gpu_enable)
%========================================================================== 
%                       ROT AND XCORR OUTPUTS
%==========================================================================
% Rotates the measurement about the given angle and calculates the
% cross-correlation with the design using dft-registratiom. The output is 
% the the shift of the two images. 
%--------------------------------------------------------------------------
% written for: QUANDALF V1.1
% last modified on: February 17, 2023
% last modified by: Jannis Weinacker
%--------------------------------------------------------------------------
% INPUTS:
% - design_Z_resized:            [single-matrix]  design data
% - measurement_Z:               [single-matrix]  measurement data
% - xcorrsize_design:            [double]  number of pixels to cross
%                                        correlate in design
% - xcorrsize_measurement:       [double]  number of pixels to cross
%                                        correlate in measurement
% - angle:                       [double]  rotation angle
% - userinput:                   [object] class object containing information
%                                        about design, exports etc.
%-------------------------------------------------------------------------
% OUTPUTS:
% - x_shift_cross:               [int]  x-coordinate of the shift of the 
%                                           two images
% - y_shift_cross:               [int]  y-coordinate of the shift of the 
%                                           two images
%==========================================================================
    
    % Create a region of interest for the measurement data (rectangle)
    crosscorr_ROI_exp =...
        [round(size(design_Z_resized,2)/2)-round(xcorrsize_measurement/2)... x-coordinate of the upper left corner
        round(size(design_Z_resized,1)/2)-round(xcorrsize_measurement/2)...  y-coordinate of the upper left corner
        xcorrsize_measurement...                                             size of the rectangle (pixels)
        xcorrsize_measurement];                                            % size of the rectangle (pixels)
    
    % Create a region of interest for the design data (rectangle)
    crosscorr_ROI_theo =...
        [round(size(design_Z_resized,2)/2)-round(xcorrsize_design/2)... x-coordinate of the upper left corner
        round(size(design_Z_resized,1)/2)-round(xcorrsize_design/2)...  y-coordinate of the upper left corner
        xcorrsize_design...                                             size of the rectangle (pixels)
        xcorrsize_design];                                            % size of the rectangle (pixels)
    
    % Rotate and crop measurement to ROI (add tolerance?)
    measurement_Z_crosscrop = imcrop(imrotate(measurement_Z, angle, 'crop'), crosscorr_ROI_exp);
    % Crop design to ROI
    design_Z_resized_crosscrop = imcrop(design_Z_resized, crosscorr_ROI_theo);
    
    % Normalize to maximum value of 1
    measurement_Z_crosscrop = measurement_Z_crosscrop./max(measurement_Z_crosscrop(:));
    design_Z_resized_crosscrop = design_Z_resized_crosscrop./max(design_Z_resized_crosscrop(:));

    % Copy data to GPU if set by user
    if gpu_enable % Copy arrays to GPU if wanted
        measurement_Z_crosscrop = gpuArray(measurement_Z_crosscrop); 
        design_Z_resized_crosscrop = gpuArray(design_Z_resized_crosscrop);
    end
    
    % First try to perform FFT on GPU. If this does not work, do it on CPU
    try 
        % Perform FFT
        measurement_Z_crosscrop_FT = fft2(measurement_Z_crosscrop);
        design_Z_resized_crosscrop_FT = fft2(design_Z_resized_crosscrop);
    catch % Try without gpuArray again if memory is a problem
        measurement_Z_crosscrop = gather(measurement_Z_crosscrop);
        design_Z_resized_crosscrop = gather(design_Z_resized_crosscrop);
        
        % Perform FFT
        measurement_Z_crosscrop_FT = fft2(measurement_Z_crosscrop);
        design_Z_resized_crosscrop_FT = fft2(design_Z_resized_crosscrop);
        
        % Copy data to GPU if set by user
        if gpu_enable % Copy arrays to GPU if wanted
            measurement_Z_crosscrop_FT = gpuArray(measurement_Z_crosscrop_FT); 
            design_Z_resized_crosscrop_FT = gpuArray(design_Z_resized_crosscrop_FT);
        end
    end

    % perform xcorr via dftregistration
    usfac = 50; % Upscaling factor = precision of the registration in pixels
    [output, ~] = dftregistration(measurement_Z_crosscrop_FT,design_Z_resized_crosscrop_FT,usfac);

    output = gather(output);
    x_shift_cross = round(output(4));
    y_shift_cross = round(output(3));

end