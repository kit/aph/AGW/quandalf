function Figure_Difference = plotdifference_core(i, measurement_structure, difference, userinput,progressGUI)
%========================================================================== 
%                          PLOT DIFFERENCE (CORE)
%==========================================================================
% Plots the calculated difference and saves the plot if wanted. Only for
% analytic tasks, not essential for the program flow.
%--------------------------------------------------------------------------
% written for: QUANDALF V1.2
% last modified on: December 19, 2023
% last modified by: Sebastian Kalt
%--------------------------------------------------------------------------
% INPUTS:
% - i:                      [integer]       Determines which data to plot
% - measurement_structure:  [cell of single-matrices] confocal measurement data
% - difference:             [cell of single-matrices] calculated difference data to
%                                           the design
% - userinput               [object] class object containing information
%                                    about design, exports etc.
% - progressGUI             [object] containing everything GUI-related
%-------------------------------------------------------------------------
% OUTPUTS:
% - none
%==========================================================================

    % for now the best solution is to just hardcode the difference.
%     plot_threshold = findplotthreshold(difference{i}); % height in meters to detect a sructure
    plot_threshold = 0;

    Figure_Difference = figure('Color','w','WindowStyle','normal','Name',userinput.analysis_filename{i},'Position',progressGUI.figuresize_small);

    imagesc(measurement_structure.x{i}*1e3,measurement_structure.y{i}*1e3,difference{i}*1e6);
    axis 'image';
    h = colorbar;
    h.FontSize = 12;
    ylabel(h,'{\it z} in µm');
    xlabel('{\it x} in mm');
    ylabel('{\it y} in mm');

    caxis([(plot_threshold-1e-6)*1e6,(plot_threshold+1e-6)*1e6]);
    title(strcat('Difference data:',{' '},userinput.analysis_filename{i}(end-7:end)),'Interpreter','none')

end