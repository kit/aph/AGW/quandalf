function threshold = findplotthreshold(measurementheights)
%========================================================================== 
%                       FIND PLOT-THRESHOLD
%==========================================================================
% The optimal plot threshold is found by analysing the height
% histogram. The minimum between -2 µm and 2 µm heights in the histogram is
% considered as the threshold.
%--------------------------------------------------------------------------
% written for: QUANDALF V1.1
% last modified on: February 22, 2023
% last modified by: Pascal Kiefer
%--------------------------------------------------------------------------
% INPUTS:
% - measurementheights:     [single-matrix]  measured height matrix
%-------------------------------------------------------------------------
% OUTPUTS:
% - threshold:              [single]  height with lowest frequency between
%                                       0 µm and 1 µm
%==========================================================================
    
    [H.Values, H.BinEdges] = histcounts( measurementheights(:) ); % Generate histogram object
    
    % There can be cases where the ROI has true values outside of the array
    % bounds. Define these boundaries to circumvent that.
    lowerboundary = max(min(H.BinEdges),-1e-6);
    upperboundary = min(max(H.BinEdges),+1e-6);
    
    ROI = (H.BinEdges > lowerboundary) & (H.BinEdges < upperboundary); % define the region of interest between 0 µm and 1 µm

    [~, index] = max(H.Values(ROI)); % find the minimum in the region of interest
    heightvalues = H.BinEdges(ROI);  % array with all the height values between 0 µm and 1 µm
    threshold = heightvalues(index); % height value, where the minimum is, is the threshold

end