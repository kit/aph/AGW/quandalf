function plotdifference(measurement_structure, difference, userinput, progressGUI)
%========================================================================== 
%                          PLOT DIFFERENCE
%==========================================================================
% Plots the calculated difference and saves the plot if wanted. Only for
% analytic tasks, not essential for the program flow.
%--------------------------------------------------------------------------
% written for: QUANDALF V1.2
% last modified on: February 23, 2023
% last modified by: Pascal Kiefer
%--------------------------------------------------------------------------
% INPUTS:
% - measurement_structure:  [cell of single-matrices] confocal measurement data
% - difference:             [cell of single-matrices] calculated difference data to
%                                           the design
% - userinput               [object] class object containing information
%                                    about design, exports etc.
% - progressGUI:            [class object] containing everything GUI-related
%-------------------------------------------------------------------------
% OUTPUTS:
% - none
%==========================================================================


    % update progress GUI
    %----------------------------------------------------------------------
    progressGUI.tool_main           = 'difference';
    progressGUI.tool_sub            = 'calc_diff';
    progressGUI.status              = 'plotting difference data';
    progressGUI.structure           = 0;
    progressGUI.n_structures        = length(difference);
    progressGUI.pb_local_percentage = 0;
    pb_global_nfun_initial          = 14;
    progressGUI.pb_global_nfun      = pb_global_nfun_initial;
    progressGUI.update();
    %----------------------------------------------------------------------

    if userinput.generalplots
            
        for i = 1:length(difference)

            Figure_Difference = plotdifference_core(i, measurement_structure, difference, userinput,progressGUI);

            if userinput.saveplots

                exportgraphics(Figure_Difference,strcat(userinput.exportpath,userinput.analysis_filename{i},'_plot_difference.png'),'BackgroundColor','w');

            end

            % Update progress GUI
            %------------------------------------------------------------------
            progressGUI.structure = i;
            progressGUI.pb_local_percentage = i/length(difference);
            progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
            progressGUI.update();
            %------------------------------------------------------------------


        end
    
    else
        
        fprintf(1,'Skipping difference plot due to user decision.\n')

        % Update progress GUI
        %------------------------------------------------------------------
        progressGUI.status = 'skipping difference plot (disabled by user)';
        progressGUI.pb_local_percentage = 1;
        progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
        progressGUI.update();
        %------------------------------------------------------------------
        
    end
    
end