function [x_shift_cross, y_shift_cross] = findshift(design_Z_resized, measurement_Z, xcorrsize_px, correct_angle, maximumshift, userinput)
%========================================================================== 
%                          FIND SHIFT
%==========================================================================
% Here the shift between the measurement and the design is determined. This
% happens by a cross correlation. The coordinates where the
% corss-correlation function has its maximum give the information about the
% resulting shift between the two images.
%--------------------------------------------------------------------------
% written for: QUANDALF V1.1
% last modified on: March 8, 2023
% last modified by: Jannis Weinacker
%--------------------------------------------------------------------------
% INPUTS:
% - design_Z_resized:            [single-matrix]  design data
% - measurement_Z:               [single-matrix]  measurement data
% - xcorrsize_px:                [double]  number of pixels to cross-
%                                        correlate 
% - correct_angle:               [double] already found optimal rotation
%                                        angle
% - maximumshift:                [int] 2x size of the cross correlation
%                                        function and therefore the maximum 
%                                        displacement that is considered
% - userinput:                   [object] class object containing information
%                                        about design, exports etc.
%-------------------------------------------------------------------------
% OUTPUTS:
% - correct_angle:               [double]  determined optimal rotation
%                                           angle
%==========================================================================

    if userinput.xcorr_mode == 0
        % Determine the shift between design and measurement
        try 
            [x_shift_cross, y_shift_cross] = rotandxcorr_outputs(design_Z_resized, measurement_Z,...
                xcorrsize_px, xcorrsize_px, correct_angle, maximumshift, userinput.gpu_enable);
        catch ME % Try without gpuArray again if memory is a problem
            if (strcmp(ME.identifier, 'parallel:gpu:array:OOM'))
                fprintf('Non enough memory on GPU, calculating on CPU \n') 
                [x_shift_cross, y_shift_cross] = rotandxcorr_outputs(design_Z_resized, measurement_Z,...
                    xcorrsize_px, xcorrsize_px, correct_angle, maximumshift, 0);
                fprintf('X-corr worked on CPU \n')
            else
                fprintf(ME.identifier) % check what was the problem
                error('Cross correlation did not work, problem was not memory on GPU')
            end
        end

    elseif userinput.xcorr_mode == 1
        % Perform  fminsearch via dftregistration function
        % For documentation see: Manuel Guizar-Sicairos, Samuel T. Thurman, and James R. Fienup, "Efficient subpixel image registration algorithms," Opt. Lett. 33, 156-158 (2008)
        % "https://de.mathworks.com/matlabcentral/fileexchange/18401-efficient-subpixel-image-registration-by-cross-correlation"

        % This mode is dependent on an equal size of both design and
        % measurement, meaning kernel and moving image
        
        % Determine the shift between design and measurement
        try 
            [x_shift_cross, y_shift_cross] = rotandxcorr_dft_outputs(design_Z_resized, measurement_Z,...
                xcorrsize_px, xcorrsize_px, correct_angle, userinput.gpu_enable);
        catch ME % Try without gpuArray again if memory is a problem
            if (strcmp(ME.identifier, 'parallel:gpu:array:OOM'))
                fprintf('Non enough memory on GPU, calculating on CPU \n') 
                [x_shift_cross, y_shift_cross] = rotandxcorr_dft_outputs(design_Z_resized, measurement_Z,...
                    xcorrsize_px, xcorrsize_px, correct_angle, 0);
                fprintf('X-corr worked on CPU \n')
            else
                fprintf(strcat(ME.identifier, '\n')) % check what was the problem
                error('Cross correlation did not work, problem was not memory on GPU')
            end
        end
    else
        error('No valid corss-correlation method given')
    end

end