classdef Cuserinput
    % Manages different user inputs necessary during runtime of QUANDALF
    
    properties (SetAccess = private)
       
        design_filename
        design_path
        design_file
        
        measurement_filename
        measurement_path
        measurement_file
        
        exportpath
        analysis_filename
        
        designsizeX
        designsizeY
        designsizeZ
        
        xcorrsize
        xcorr_mode

        design_z_shift

        gpu_enable

        enable_marker_removal

        shrinkageanalysis

        downscalingfactor

        interactive_filtering 
        
        generalplots          = false;
        shrinkageplot         = false;
        saveplots             = false;       

        informationfileID
        generaloutputfileID
        
        findangle
        findscaling

        tilingX
        tilingY

        GUIsize

       
        
    end
    
    methods
        function obj = Cuserinput()
            % Constructor that asks for all necessary intputs
            
            % Ask for characterization method, design, and measurement files
            obj = getinputfiles(obj);
            
            % Ask for numerical parameters
            obj = getnumericalparameters(obj);
            
            % Save used input data to txt file
            obj = savegeneraldatatotxt(obj);

        end

        function obj = getinputfiles(obj)

            set(0,'units','pixels')  
            screen_size_px = get(0,'screensize');
            
            obj.GUIsize = [(screen_size_px(3)-980)/2 (screen_size_px(4)-500)/2 980 500];

            input_file_ui    = uifigure('Name','Quandalf','Position',obj.GUIsize ,'WindowStyle','modal');
            input_file_ui.Icon = 'images\Q_logo.png';
            im = uiimage(input_file_ui,"ImageSource","images\quandalf_gui_header.png","Position",[20 397 940 83],"ScaleMethod","fit");

            continue_button = uibutton(input_file_ui,"state","Position",[250 20 220 50],"Text","Continue","FontWeight","bold","BackgroundColor",[167/255 199/255 231/255],"FontSize",13,"Value",false,"Enable","off");
            cancel_button   = uibutton(input_file_ui,"Position",[510 20 220 50],"Text","Cancel","FontWeight","bold","BackgroundColor",[0.9 0.9 0.9],"FontSize",13,"ButtonPushedFcn", @(cancel_button,event) CancelButtonPushed(input_file_ui));

            % GUI Panel: Quandalf User Guide
            %--------------------------------------------------------------
            guide_header = uipanel(input_file_ui,"Position",[20 347 300 30],"BackgroundColor",[167/255 199/255 231/255]);
            guide_title  = uilabel(guide_header,"Position",[0,0,300,30],"Text","Quandalf User Guide","VerticalAlignment",'center',"HorizontalAlignment",'center',"FontWeight",'bold',"FontSize",15);
            guide_panel  = uipanel(input_file_ui,"Position",[20 90 300 250],"BackgroundColor",[0.9,0.9,0.9]);
            
            guide_text_01 = "The structure design should be a 2D height profile. Therefore, a single PNG grayscale image or a two-dimensional .MAT-file are allowed as design inputs. The dimensions of the design will be requested as a user input in the next step.";
            guide_text_02 = "The measurement data are also expected to be 2D height profiles and can be imported in the .SUR, two-dimensional .MAT, or .TXT file format. It is possible to import multiple measurement files, e.g. multiple prints of the same design.";
            guide_text_03 = "Not yet. 3D data sets have to be converted to 2D heigth profiles before they can be processed by the QUANDALF routine.";

            guide_label_01 = uilabel(guide_panel,"Position",[10,230,265,15],"Text","How is the structure design processed?","FontWeight",'bold',"VerticalAlignment",'top',"HorizontalAlignment",'left',"WordWrap","on");
            guide_label_02 = uilabel(guide_panel,"Position",[10,150,265,80],"Text",guide_text_01,"VerticalAlignment",'top',"HorizontalAlignment",'left',"WordWrap","on");

            guide_label_03 = uilabel(guide_panel,"Position",[10,140,265,15],"Text","How are the measurement data processed?","FontWeight",'bold',"VerticalAlignment",'top',"HorizontalAlignment",'left',"WordWrap","on");
            guide_label_04 = uilabel(guide_panel,"Position",[10,70,265,72],"Text",guide_text_02,"VerticalAlignment",'top',"HorizontalAlignment",'left',"WordWrap","on");
            
            guide_label_05 = uilabel(guide_panel,"Position",[10,50,265,15],"Text","Can 3D (fluorescence) data be processed?","FontWeight",'bold',"VerticalAlignment",'top',"HorizontalAlignment",'left',"WordWrap","on");
            guide_label_06 = uilabel(guide_panel,"Position",[10,10,265,42],"Text",guide_text_03,"VerticalAlignment",'top',"HorizontalAlignment",'left',"WordWrap","on");

            % GUI Panel: Select Design File
            %--------------------------------------------------------------
            design_header = uipanel(input_file_ui,"Position",[340 347 300 30],"BackgroundColor",[167/255 199/255 231/255]);
            design_title  = uilabel(design_header,"Position",[0,0,300,30],"Text","Select Design File","VerticalAlignment",'center',"HorizontalAlignment",'center',"FontWeight",'bold',"FontSize",15);
            design_panel  = uipanel(input_file_ui,"Position",[340 90 300 250],"BackgroundColor",[0.9,0.9,0.9]);
            
            ui_return = uicheckbox(design_panel,"Value",0,"Visible","off");

            design_number_lb_1 = uilabel(design_panel,"Position",[18 210 280 30],"Text",'Number of selected design files:','FontWeight','bold',Visible='off');
            design_number_lb_2 = uilabel(design_panel,"Position",[18 190 264 30],"Text",'0','FontWeight','bold','FontSize',25,'HorizontalAlignment','center',Visible='off');

            design_name_lb_1   = uilabel(design_panel,"Position",[18 210 280 30],"Text",'Name of the selected design file:','FontWeight','bold');
            design_name_lb_2   = uilabel(design_panel,"Position",[18 130 264 85],"Text",'no file selected','WordWrap','on','FontWeight','bold','FontSize',15,'HorizontalAlignment','center','VerticalAlignment','top');

            design_path_lb     = uilabel(design_panel,"Position",[18,90,280,30],"Text","Path to design file:","FontWeight",'bold',"VerticalAlignment",'center',"HorizontalAlignment",'left');
            design_path_file   = uieditfield(design_panel,"Text","Position",[15 65 270 25],"Value","");
            design_path_button = uibutton(design_panel,"Position",[50 20 200 30],"Text","Browse","FontWeight",'bold',"BackgroundColor",[0.9 0.9 0.9],"ButtonPushedFcn", @(design_path_button,event) BrowseButtonPushed(design_path_file,'',input_file_ui,'file','design',{'*.png';'*.mat'},design_number_lb_2,design_name_lb_2,ui_return,continue_button));

            % GUI Panel: Select Measurement File
            %--------------------------------------------------------------
            meas_header = uipanel(input_file_ui,"Position",[660 347 300 30],"BackgroundColor",[167/255 199/255 231/255]);
            meas_title  = uilabel(meas_header,"Position",[0,0,300,30],"Text","Select Measurement File(s)","VerticalAlignment",'center',"HorizontalAlignment",'center',"FontWeight",'bold',"FontSize",15);
            meas_panel  = uipanel(input_file_ui,"Position",[660 90 300 250],"BackgroundColor",[0.9,0.9,0.9]);

            meas_number_lb_1 = uilabel(meas_panel,"Position",[18 145 280 40],"Text",'Number of selected measurement files:','FontWeight','bold');
            meas_number_lb_2 = uilabel(meas_panel,"Position",[18 125 264 40],"Text",'0','FontWeight','bold','FontSize',25,'HorizontalAlignment','center');

            meas_name_lb_1 = uilabel(meas_panel,"Position",[18 210 280 30],"Text",'Name of the first selected measurement file:','FontWeight','bold');
            meas_name_lb_2 = uilabel(meas_panel,"Position",[18 190 264 30],"Text",'no file selected','FontWeight','bold','FontSize',15,'HorizontalAlignment','center');

            meas_path_lb     = uilabel(meas_panel,"Position",[18,90,280,30],"Text","Path to measurement file(s):","FontWeight",'bold',"VerticalAlignment",'center',"HorizontalAlignment",'left');
            meas_path_file   = uieditfield(meas_panel,"Position",[15 65 270 25],"Value",'');       
            meas_path_help   = uicontrol(meas_panel,"Style",'listbox',"Position",[0 0 1 1],"Visible","off");
            meas_path_button = uibutton(meas_panel,"Position",[50 20 200 30],"Text","Browse","FontWeight",'bold',"BackgroundColor",[0.9 0.9 0.9],"ButtonPushedFcn", @(meas_path_button,event) BrowseButtonPushed(meas_path_file,meas_path_help,input_file_ui,'file','measurement',{'*.sur';'*.mat';'*.txt'},meas_number_lb_2,meas_name_lb_2,ui_return,continue_button));

            drawnow
            
            waitfor(continue_button,'Value')
            

            % use ui returns to set design-related object properties:
            %--------------------------------------------------------------
            designfile = design_path_file.Value;
            match = wildcardPattern + "\";
            designfilename = erase(designfile,match);
            designpath = erase(designfile,designfilename);

            obj.design_filename = designfilename ;
            obj.design_path     = designpath     ;
            obj.design_file     = designfile     ;


            % use ui returns to set measurement-related object properties:
            %--------------------------------------------------------------
            measurementfiles = meas_path_help.String;
            measurementfilenames = erase(measurementfiles,match);
            measurementpaths = erase(measurementfiles,measurementfilenames);
            
            obj.measurement_filename = measurementfilenames;
            obj.measurement_path     = measurementpaths;
            obj.measurement_file     = measurementfiles;
            obj.analysis_filename    = cellfun(@(x) x(1:end-4),measurementfilenames,'UniformOutput',false);
            
            close(input_file_ui)
            drawnow
           
        end
        
        
        function obj = getnumericalparameters(obj)
            % Asks for all relevant numerical parameters
            
            % initial definitions:
            formatOut = 'yyyy-mm-dd';
            datename = datestr(now,formatOut);
            foldername = strcat(datename, '_ShrinkageAnalysis');
            folderpath = strcat(obj.measurement_path{1}, foldername);
            exportpath_default = strcat(folderpath,'\');
            

            %=============================================================%
            %                        user input gui                       %
            %=============================================================%

            gui_inputs = uifigure('Name','Quandalf','Position',obj.GUIsize ,'WindowStyle','modal');
            gui_inputs.Icon = 'images\Q_logo.png';
            im = uiimage(gui_inputs,"ImageSource","images\quandalf_gui_header.png","Position",[20 397 940 83],"ScaleMethod","fit");

            %-------------------------------------------------------------%
            %                       design properties                     %
            %-------------------------------------------------------------%

            % panel, header, and title
            input_design_header     = uipanel(gui_inputs,"Position",[20 347 300 30],"BackgroundColor",[167/255 199/255 231/255]);
            input_design_title      = uilabel(input_design_header,"Position",[0,0,300,30],"Text","Design Properties","VerticalAlignment",'center',"HorizontalAlignment",'center',"FontWeight",'bold',"FontSize",15);
            input_design_panel      = uipanel(gui_inputs,"Position",[20 20 300 320]);
            
            % set designsize
            input_design_lb_xsize   = uilabel(input_design_panel,"Position",[18,285,280,30],"Text","Design size in x-direction (in um):","VerticalAlignment",'center',"HorizontalAlignment",'left');
            input_design_ef_xsize   = uieditfield(input_design_panel,"Text","Position",[15 265 270 25],"Value","0");
            input_design_lb_ysize   = uilabel(input_design_panel,"Position",[18,235,280,30],"Text","Design size in y-direction (in um):","VerticalAlignment",'center',"HorizontalAlignment",'left');
            input_design_ef_ysize   = uieditfield(input_design_panel,"Text","Position",[15 215 270 25],"Value","0");
            input_design_lb_zsize   = uilabel(input_design_panel,"Position",[18,185,280,30],"Text","Design height (in um):","VerticalAlignment",'center',"HorizontalAlignment",'left');
            input_design_ef_zsize   = uieditfield(input_design_panel,"Text","Position",[15 165 270 25],"Value","0");
            
            % set tiling
            input_design_lb_tiling  = uilabel(input_design_panel,"Position",[18,115,265,43],"Text","If the built-in tiling option is used in Grayscribe X to generate the print job, use the same tiling values here:","VerticalAlignment",'center',"HorizontalAlignment",'left',"WordWrap","on");
            input_design_lb_xtiling = uilabel(input_design_panel,"Position",[18,85,280,30],"Text","Tiling x:","VerticalAlignment",'center',"HorizontalAlignment",'left');
            input_design_ef_xtiling = uieditfield(input_design_panel,"Text","Position",[15 65 270 25],"Value","1");
            input_design_lb_ytiling = uilabel(input_design_panel,"Position",[18,35,280,30],"Text","Tiling y:","VerticalAlignment",'center',"HorizontalAlignment",'left');
            input_design_ef_ytiling = uieditfield(input_design_panel,"Text","Position",[15 15 270 25],"Value","1");      
               

            %-------------------------------------------------------------%
            %                         export settings                     %
            %-------------------------------------------------------------%
            
            % panel, header, and title
            input_export_header       = uipanel(gui_inputs,"Position",[340 165 300 30],"BackgroundColor",[167/255 199/255 231/255]);
            input_export_title        = uilabel(input_export_header,"Position",[0,0,300,30],"Text","Figure and Export Settings","VerticalAlignment",'center',"HorizontalAlignment",'center',"FontWeight",'bold',"FontSize",15);
            input_export_panel        = uipanel(gui_inputs,"Position",[340 20 300 138]);
            
            % set export path
            input_export_lb_path      = uilabel(input_export_panel,"Position",[18,35,280,30],"Text","Figure export path:","VerticalAlignment",'center',"HorizontalAlignment",'left');
            input_export_path         = uieditfield(input_export_panel,"Text","Position",[15 15 160 25],"Value",exportpath_default,"Enable","off");
            input_export_path_browser = uibutton(input_export_panel,"Position",[180 15 110 25],"Text","Browse","BackgroundColor",[0.9 0.9 0.9],"Enable","off","ButtonPushedFcn", @(input_export_path_browser,event) BrowseButtonPushed(input_export_path,'',gui_inputs,'path','export','','',''));

            % export settings
            input_export_cb_show_plots          = uicheckbox(input_export_panel,"Position",[18 105 280 20],"Text","Show generated plots and figures");
            input_export_cb_show_shrinkage_plot = uicheckbox(input_export_panel,"Position",[18 85 280 20],"Text","Show shrinkage plot","Enable","off");
            input_export_cb_export_figures      = uicheckbox(input_export_panel,"Position",[18 65 280 20],"Text","Export plots and figures",'ValueChangedFcn',@(input_export_cb_export_figures,event) cBoxChanged(input_export_cb_export_figures,input_export_path,input_export_path_browser));

            
            %-------------------------------------------------------------%
            %                     computational settings                  %
            %-------------------------------------------------------------%
            
            % panel, header, and title
            input_comp_header            = uipanel(gui_inputs,"Position",[660 347 300 30],"BackgroundColor",[167/255 199/255 231/255]);
            input_comp_title             = uilabel(input_comp_header,"Position",[0,0,300,30],"Text","Computational Settings","VerticalAlignment",'center',"HorizontalAlignment",'center',"FontWeight",'bold',"FontSize",15);
            input_comp_panel             = uipanel(gui_inputs,"Position",[660 70 300 270]);
            
            % ask for cross-correlation algorithm
            input_comp_lb_cc_algorithm   = uilabel(input_comp_panel,"Position",[18,235,280,30],"Text","Select cross-correlation algorithm:","VerticalAlignment",'center',"HorizontalAlignment",'left');
            input_comp_dd_cc_algorithm   = uidropdown(input_comp_panel,"Position",[18 220 265 22],"Items",["dftregistration (default)","xcorr2 (internal matlab function, slow)"],"BackgroundColor",[0.9 0.9 0.9]);
            
            % ask for using GPU or CPU arrays
            input_comp_lb_gpu_arrays     = uilabel(input_comp_panel,"Position",[18,190,280,30],"Text","Select where arrays are stored:","VerticalAlignment",'center',"HorizontalAlignment",'left');
            input_comp_dd_gpu_arrays     = uidropdown(input_comp_panel,"Position",[18 175 265 22],"Items",["GPU memory (fast, requires decent GPU memory)","CPU memory (slow)"],"BackgroundColor",[0.9 0.9 0.9]);
            
            % ask for difference data gauging
            input_comp_lb_reference      = uilabel(input_comp_panel,"Position",[18,145,280,30],"Text","Reference difference data to...","VerticalAlignment",'center',"HorizontalAlignment",'left');
            input_comp_dd_reference      = uidropdown(input_comp_panel,"Position",[18 130 265 22],"Items",["central region of the structure","substrate"],"BackgroundColor",[0.9 0.9 0.9]);
            
            % ask for cross-correlation kernel size
            input_comp_lb_cc_kernel_size = uilabel(input_comp_panel,"Position",[18,85,280,30],"Text","Kernel size for cross-correlation calculations (in %):","VerticalAlignment",'center',"HorizontalAlignment",'left');
            input_comp_ef_cc_kernel_size = uieditfield(input_comp_panel,"Text","Position",[15 65 270 25],"Value","100");
            
            % ask for rotation scaling factor
            input_comp_lb_scaling_factor = uilabel(input_comp_panel,"Position",[18,35,280,30],"Text","Downscaling factor for automated sample rotation:","VerticalAlignment",'center',"HorizontalAlignment",'left');
            input_comp_ef_scaling_factor = uieditfield(input_comp_panel,"Text","Position",[15 15 270 25],"Value","1.0","Enable","off");
        

            %-------------------------------------------------------------%
            %                           tool settings                     %
            %-------------------------------------------------------------%
            
            % panel, header, and title
            input_tools_header = uipanel(gui_inputs,"Position",[340 347 300 30],"BackgroundColor",[167/255 199/255 231/255]);
            input_tools_title  = uilabel(input_tools_header,"Position",[0,0,300,30],"Text","Tool Settings","VerticalAlignment",'center',"HorizontalAlignment",'center',"FontWeight",'bold',"FontSize",15);
            input_tools_panel  = uipanel(gui_inputs,"Position",[340 205 300 135]);
            
            % tool setting checkboxes
            input_tools_label                 = uilabel(input_tools_panel,"Position",[18,105,280,30],"Text","Enable/Disable optional tools:","VerticalAlignment",'center',"HorizontalAlignment",'left',"FontWeight",'bold');
            input_tools_cb_alignment_marks    = uicheckbox(input_tools_panel,"Position",[18 90 280 20],"Text","Alignment mark removal routine");
            input_tools_cb_sample_rotation    = uicheckbox(input_tools_panel,"Position",[18 70 280 20],"Text","Automatic sample rotation",'ValueChangedFcn',@(input_tools_cb_sample_rotation,event) cBoxChanged(input_tools_cb_sample_rotation,input_comp_ef_scaling_factor));
            input_tools_cb_sample_scaling     = uicheckbox(input_tools_panel,"Position",[18 50 280 20],"Text","Automatic sample scaling");
            input_tools_cb_shrinkage_analysis = uicheckbox(input_tools_panel,"Position",[18 30 280 20],"Text","Shrinkage analysis via scattering plot routine",'ValueChangedFcn',@(input_tools_cb_shrinkage_analysis,event) cBoxChanged(input_tools_cb_shrinkage_analysis,input_export_cb_show_shrinkage_plot));
            input_tools_cb_filtering_app      = uicheckbox(input_tools_panel,"Position",[18 10 280 20],"Text","Interactive filtering application");
            
            
            %-------------------------------------------------------------%
            %                             buttons                         %
            %-------------------------------------------------------------%
            input_start_button  = uibutton(gui_inputs,"state","Position",[660 20 140 30],"Text","Start Computation","FontWeight","bold","BackgroundColor",[167/255 199/255 231/255],"Value",false);
            input_cancel_button = uibutton(gui_inputs,"Position",[820 20 140 30],"Text","Cancel","FontWeight","bold","BackgroundColor",[0.9 0.9 0.9],"ButtonPushedFcn", @(input_cancel_button,event) CancelButtonPushed(gui_inputs));
            

            %-------------------------------------------------------------%
            %                      Differentiate cases                    %
            %-------------------------------------------------------------%

            if strcmp(obj.design_filename(end-2:end), 'mat')
                input_design_ef_zsize.Enable = 'off';
                input_design_ef_zsize.Value = "set in input file";
            end
                
            drawnow

            waitfor(input_start_button,'Value')
            
            

            %-------------------------------------------------------------%
            %                      Save user inputs                       %
            %-------------------------------------------------------------%

            % save user inputs: Design parameters
            %--------------------------------------------------------------
            obj.designsizeX           = str2double(input_design_ef_xsize.Value) * 1e-6;
            obj.designsizeY           = str2double(input_design_ef_ysize.Value) * 1e-6;
            obj.tilingX               = str2double(input_design_ef_xtiling.Value);
            obj.tilingY               = str2double(input_design_ef_xtiling.Value);   

            if strcmp(obj.design_filename(end-2:end), 'png')
                obj.designsizeZ           = str2double(input_design_ef_zsize.Value) * 1e-6;
            else
                obj.designsizeZ           = NaN;
            end
            

            % save user inputs: tool selection (checkboxes)
            %--------------------------------------------------------------
            obj.enable_marker_removal = input_tools_cb_alignment_marks.Value;
            obj.findangle             = input_tools_cb_sample_rotation.Value;
            obj.findscaling           = input_tools_cb_sample_scaling.Value;
            obj.shrinkageanalysis     = input_tools_cb_shrinkage_analysis.Value;
            obj.interactive_filtering = input_tools_cb_filtering_app.Value;


            % computational settings
            %--------------------------------------------------------------
            obj.xcorrsize             = str2double(input_comp_ef_cc_kernel_size.Value)/100;
            obj.downscalingfactor     = str2double(input_comp_ef_scaling_factor.Value);
            if strcmp(input_comp_dd_cc_algorithm.Value,'dftregistration (default)')
                obj.xcorr_mode = 1;
            elseif strcmp(input_comp_dd_cc_algorithm.Value,'xcorr2 (internal matlab function, slow)') 
                obj.xcorr_mode = 0;
            else
                error('make sure that the dropdown options match the variable save conditions')            
            end

            if strcmp(input_comp_dd_reference.Value,'central region of the structure')
                obj.design_z_shift = 1;
            elseif strcmp(input_comp_dd_reference.Value,'substrate')
                obj.design_z_shift = 0;
            else
                error('make sure that the dropdown options match the variable save conditions')            
            end

            if strcmp(input_comp_dd_gpu_arrays.Value,'GPU memory (fast, requires decent GPU memory)')
                obj.gpu_enable = 1;
            elseif strcmp(input_comp_dd_gpu_arrays.Value,'CPU memory (slow)')
                obj.gpu_enable = 0;
            else
                error('make sure that the dropdown options match the variable save conditions')            
            end
                       
            
            % save user inputs: figure and export settings
            %--------------------------------------------------------------
            obj.exportpath    = input_export_path.Value;
            obj.generalplots  = input_export_cb_show_plots.Value;
            obj.shrinkageplot = input_export_cb_show_shrinkage_plot.Value;
            obj.saveplots     = input_export_cb_export_figures.Value;

            if obj.saveplots == 1
                mkdir(obj.exportpath);
            end
      
            close(gui_inputs)

        end
        

        function obj = savegeneraldatatotxt(obj)
           if obj.saveplots
        
                if iscell(obj.analysis_filename)
                    obj.informationfileID = cellfun(@(x) fopen(strcat(obj.exportpath, x, '_analysis_information.txt'), 'w'), obj.analysis_filename);
                    
                    for i = 1:length(obj.analysis_filename)
                        fprintf(obj.informationfileID(i),'== ANALYSIS OF STRUCTRE %s ==\n\n', obj.analysis_filename{i});
                        fprintf(obj.informationfileID(i),'Used design data for difference calculation: %s\n', obj.design_file);
                        fprintf(obj.informationfileID(i),'Design size  : %1.4f mm x %1.4f mm\n', obj.designsizeX * 1e3, obj.designsizeY * 1e3);
                        fprintf(obj.informationfileID(i),'Design height: %1.3f um\n\n', obj.designsizeZ * 1e6);
                        fprintf(obj.informationfileID(i),'Fraction of design for cross correlation: %1.2f%%\n', obj.xcorrsize*100);
                        %fprintf(obj.informationfileID,'Measurement kernel size for xcorr: %d px x %d px\n', obj.xcorrsize_measurement, obj.xcorrsize_measurement);
                    end
                else
                    obj.informationfileID = fopen(strcat(obj.exportpath, obj.analysis_filename, '_analysis_information.txt'), 'w');
                    
                    fprintf(obj.informationfileID,'== ANALYSIS OF STRUCTRE %s ==\n\n', obj.analysis_filename);
                    fprintf(obj.informationfileID,'Used design data for difference calculation: %s\n', obj.design_file);
                    fprintf(obj.informationfileID,'Design size  : %1.4f mm x %1.4f mm\n', obj.designsizeX * 1e3, obj.designsizeY * 1e3);
                    fprintf(obj.informationfileID,'Design height: %1.3f um\n\n', obj.designsizeZ * 1e6);
                    fprintf(obj.informationfileID,'Fraction of design for cross correlation: %1.2f%%\n', obj.xcorrsize*100);
                    %fprintf(obj.informationfileID,'Measurement kernel size for xcorr: %d px x %d px\n', obj.xcorrsize_measurement, obj.xcorrsize_measurement);
                end
                obj.generaloutputfileID = fopen(strcat(obj.exportpath, 'GeneralOutputData.txt'), 'w');
                fprintf(obj.generaloutputfileID, 'Evaluated quality figure of merits \n');

            else

                obj.informationfileID = 1;
                obj.generaloutputfileID = 1;

            end 
        end
    
        
    end
end

