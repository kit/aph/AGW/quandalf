function [new_design] = calc_save_new_design(difference_filtered, previous_design, design, userinput, progressGUI)
%========================================================================== 
%                       <CALC_SAVE_NEW_DESIGN>
%==========================================================================
% Calculates the new design as the difference of the previous design and
% the filtered difference. Saves the new design as .mat and .png file
%--------------------------------------------------------------------------
% written for: QUANDALF V1.x
% last modified on: March 31, 2023
% last modified by: Pascal Rietz
%--------------------------------------------------------------------------
% INPUTS:
% - difference_filtered: [double-matrix]  filtered difference
% - previous_design:     [double-matrix]  previous design 
% - design:              [struct]  targeted design data
% - userinput:           [object]  class object containing parameters about
%                                  design, export etc
% - progressGUI:         [class object] containing everything GUI-related
%--------------------------------------------------------------------------
% OUTPUTS:
% - new_design:          [double_matrix]  new_design
%==========================================================================
    
    % update progress GUI
    %----------------------------------------------------------------------
    progressGUI.tool_main           = 'filtering';
    progressGUI.tool_sub            = 'save_new';
    progressGUI.status              = 'calculating new design';
    progressGUI.structure           = 0;
    progressGUI.n_structures        = 0;
    progressGUI.pb_local_percentage = 0;
    pb_global_nfun_initial          = 26;
    progressGUI.pb_global_nfun      = pb_global_nfun_initial;
    progressGUI.update();
    %----------------------------------------------------------------------

    fprintf(1, 'Calculating new design...\n');
    
    % catch cell array input
    if ~isnumeric(previous_design)
        if iscell(previous_design)
            previous_design = previous_design{1};
        else
            warning('Data type of previous design might not match required data type');
        end
    end
    
    % if a resize step was missing during program evaluation
    if size(previous_design) ~= size(difference_filtered)
        warning('Sizes of previous design and new difference do not match, previous design will be resized');
        previous_design = imresize(previous_design, size(difference_filtered));
    end
    
    % new design = design (old) - difference (new measurement) 
    new_design = previous_design - difference_filtered;
    
    % Plot the new design
    figure('Color','w','Position',progressGUI.figuresize_small)
    ax = gca;
    imagesc(ax, design.x*1e3,design.y*1e3,new_design*1e6);
    axis(ax, 'image');
    h = colorbar(ax);
    h.Label.String = ['{\it z} / \mum'];   
    xlabel(ax, '{\it x} / mm');
    ylabel(ax, '{\it y} / mm');
    title(ax, 'New design');
    box(ax, 'on');
    
    % Save new design as .mat-file as well as grayscale .png files

    % Update progress GUI
    %----------------------------------------------------------------------
    progressGUI.status = 'saving new design';
    progressGUI.pb_local_percentage = 0.5;
    progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
    progressGUI.update();
    %----------------------------------------------------------------------

    fprintf(1, 'Saving new design...\n');

    % Save as .mat file
    array_to_save_des = new_design;           
    if iscell(userinput.analysis_filename)                
        save(strcat(userinput.exportpath,userinput.analysis_filename{1},'_new_design.mat'), 'array_to_save_des');                
    else            
        save(strcat(userinput.exportpath,userinput.analysis_filename,'_new_design.mat'), 'array_to_save_des');                
    end
    
    fprintf(1, '...mat-file saved.\n');
    
    % Save as .png file
    my_export = new_design;
    my_d16 = uint16(my_export/max(max(my_export))*2^16);
    export_name = ['_new_des_height_' num2str(max(max(my_export))*1e6,5)];          
    if iscell(userinput.analysis_filename)              
        imwrite(my_d16, strcat(userinput.exportpath, userinput.analysis_filename{1}, export_name, '.png'));                    
    else                
        imwrite(my_d16, strcat(userinput.exportpath, userinput.analysis_filename, export_name, '.png'));                    
    end
    
    fprintf(1, '...png-file saved.\n');

    % update progress GUI
    %----------------------------------------------------------------------
    progressGUI.tool_main           = 'filtering';
    progressGUI.tool_sub            = 'done';
    progressGUI.status              = 'process completed';
    progressGUI.status_message.FontColor = [0 168/255 0];
    progressGUI.structure           = 0;
    progressGUI.n_structures        = 0;
    progressGUI.pb_local_percentage = 1;
    pb_global_nfun_initial          = 27;
    progressGUI.pb_global_nfun      = pb_global_nfun_initial;
    progressGUI.update();
    %----------------------------------------------------------------------

end