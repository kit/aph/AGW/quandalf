function difference_filtered = run_filtering_script(difference_mean, design, userinput, progressGUI)
%========================================================================== 
%                       RUN FILTERING (SCRIPT)
%==========================================================================
% Performs multistep filtering of difference data excluding high
% gradient values. For further information regarding the different
% steps, see the comments below
%--------------------------------------------------------------------------
% written for: QUANDALF V1.x
% last modified on: March 30, 2023
% last modified by: Pascal Rietz
%--------------------------------------------------------------------------
% INPUTS:
% - difference_mean: [double-matrix]  initial difference data
% - design:          [struct]  design data
% - userinput:       [object]  class object containing parameters about
%                              design, export etc
% - progressGUI:     [object] containing everything GUI-related
%--------------------------------------------------------------------------
% OUTPUTS:
% - difference_filtered:  [double_matrix]  filtered difference
%==========================================================================
    
    % update progress GUI
    %----------------------------------------------------------------------
    progressGUI.tool_main           = 'filtering';
    progressGUI.tool_sub            = 'auto_filtering';
    progressGUI.status              = 'running automated filtering';
    progressGUI.structure           = 0;
    progressGUI.n_structures        = 0;
    progressGUI.pb_local_percentage = 0;
    pb_global_nfun_initial          = 22;
    progressGUI.pb_global_nfun      = pb_global_nfun_initial;
    progressGUI.update();
    %----------------------------------------------------------------------

    % Start taking runtime
    tic;

    % Set default parameters for filtering
    dif.gauss_sig       =  2e-6;    % [double]  default gaussian filtering kernel standard deviation for filtering neglecting NaNs
    dif.nan_expand      =  2e-6;    % [double]  default value for expanding excluded (NaN) regions
    dif.grad_thres      =  3e-7;    % [double]  default gradient threshold for excluding values        
    dif.border_area     =  5e-6;    % [double]  default border area treated seperately
    dif.high_dif_cut    =  2e-6;    % [double]  default cut of values of high difference
    dif.px_sz = userinput.designsizeX/numel(design.x);

    % Set temporary variable during the filtering process
    dif.temp = difference_mean;

    % General filtering process: Set values of high gradient
    % (according to the grad_thres variable) and high difference values
    % to NaN (= Not a number). Such values are espected to stem mostly from
    % statistical errors of the printing or the measurement
    % process. Therefore, these should not be taken into account
    % for the calculation of a new design, which compensates for
    % systematic errors 

    % 1. Compute gradient image. Define a mask where the gradient image is
    % above the defined threshold. Also add pixels to the mask which
    % correspont to a height value above the hight cut off.
    dif.dif_gradient = imgradient(dif.temp);
    dif.mask = (dif.dif_gradient > dif.grad_thres) | (abs(dif.temp) > dif.high_dif_cut);         
    
    % 2. Expand the regions in the mask about the defined NaN
    % expansion value. Set corresponding pixels to NaN
    r = round(dif.nan_expand / dif.px_sz); %Convert NaN expansion width to pixels
    dif.mask = imdilate(dif.mask, strel("disk", r));
    dif.temp(dif.mask) = NaN;

    % Coarse estimation of computational time depending on the
    % number of NaN values. The factor is emperically found, based
    % on several runs on a Intel Xeon Platinum 8260 CPU
    num_nan = sum(isnan(dif.temp(:)));
    fprintf(1,'Estimated computational time is %.2f seconds\n', num_nan*9.3e-6);
    
    % 3. The borders of the structure can be treated seperately. As
    % there can be steep edges, which are prone to measurement
    % errors, these areas are often set to NaN and therefore
    % excluded. However, the interpolation can fail in this region.
    % Therefore, the border areas are treated seperately by
    % expanding the nearest non-NaN value to the border. In general,
    % is is recommended to print a sufficiently large area, such
    % that edge effects are negligible.
    border = int64(dif.border_area / dif.px_sz); % Define border area in px

    % 4. Interpolation of NaN values (except for border areas)
    % using the inpaint_nans function (John D'Errico (2023).
    % inpaint_nans (See https://www.mathworks.com/matlabcentral/fileexchange/4551-inpaint_nans),
    % MATLAB Central File Exchange. Retrieved March 30, 2023. 
    % for license and implementation information ).
    % The function is fast and reliable especially for 2D data.
    dif.temp(border:end-border, border:end-border) = inpaint_nans(dif.temp(border:end-border, border:end-border), 0);

    % 5. As discussed in 3., the border values are excluded
    % seperately and filled with the nearest non-NaN value using
    % the 'fillmissing' function
    dif.temp(1:border,:)       = NaN;
    dif.temp(end-border:end,:) = NaN;
    dif.temp(:,1:border)       = NaN;
    dif.temp(:,end-border:end) = NaN;
    
    dif.temp(border:end-border,:) = fillmissing(dif.temp(border:end-border,:), 'linear', 2, 'EndValues', 'nearest');
    dif.temp(:,border:end-border) = fillmissing(dif.temp(:,border:end-border), 'linear', 1, 'EndValues', 'nearest');
    dif.temp = fillmissing(dif.temp, 'linear', 2, 'EndValues', 'nearest');
    
    % Check if all NaNs are filled
    no_nans = all(~isnan(dif.temp(:)));
    if ~no_nans
        warning('For some reason there are NaNs left. Check the interpolation');
    end
    
    % 6. Main filtering. Gaussian filtering  
    dif.temp = imgaussfilt(dif.temp, dif.gauss_sig/dif.px_sz);

    % Set filter flag
    dif.filter_flag = true;

    % Set output variable
    difference_filtered = dif.temp;

    % End taking runtime
    toc;

    % Update progress GUI
    %----------------------------------------------------------------------
    progressGUI.pb_local_percentage = 1;
    progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
    progressGUI.update();
    %----------------------------------------------------------------------

end