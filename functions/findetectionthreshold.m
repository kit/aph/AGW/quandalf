function threshold = findetectionthreshold(measurementheights)
%========================================================================== 
%                       FIND DETECTION-THRESHOLD
%==========================================================================
% The optimal detection threshold is found by analysing the height
% histogram. The minimum between 0 µm and 1 µm heights in the histogram is
% considered as the threshold.
%--------------------------------------------------------------------------
% written for: QUANDALF V1.1
% last modified on: February 09, 2023
% last modified by: Jannis Weinacker
%--------------------------------------------------------------------------
% INPUTS:
% - measurementheights:     [single-matrix]  measured height matrix
%-------------------------------------------------------------------------
% OUTPUTS:
% - threshold:              [single]  height with lowest frequency between
%                                       0 µm and 1 µm
%==========================================================================
    f = figure();
    H = histogram( measurementheights(:) ); % Generate histogram object

    ROI = (H.BinEdges > 0) & (H.BinEdges < 1e-6); % define the region of interest between 0 µm and 1 µm

    [~, index] = min(H.Values(ROI)); % find the minimum in the region of interest
    heightvalues = H.BinEdges(ROI);  % array with all the height values between 0 µm and 1 µm
    threshold = heightvalues(index); % height value, where the minimum is, is the threshold
    close(f);

end