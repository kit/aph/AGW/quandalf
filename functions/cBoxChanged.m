function cBoxChanged(cb,ui_element_1,ui_element_2)
    % callback function for checkboxes to manipulate up to two ui input elements
    val = cb.Value;
    if val
        ui_element_1.Enable = 'on';
        if nargin > 2
            ui_element_2.Enable = 'on';
        end
    else
        ui_element_1.Enable = 'off';
        if nargin > 2
            ui_element_2.Enable = 'off';
        end
    end
end