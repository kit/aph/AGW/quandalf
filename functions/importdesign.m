function design_data = importdesign(userinput,progressGUI)
%========================================================================== 
%                          IMPORT DESIGN
%==========================================================================
% function to import the design data from a .mat or 16-bit .png file
%--------------------------------------------------------------------------
% written for: QUANDALF V1.0
% last modified on: June 03, 2023
% last modified by: S. Kalt
%--------------------------------------------------------------------------
% INPUTS:
% - userinput       [object] class object containing information
%                                    about design, exports etc.
% - progressGUI     [object] class object containing everything GUI-related
%-------------------------------------------------------------------------
% OUTPUTS:
% - design_data:    [single matrix] design data in meter 
%==========================================================================
    
    % Update progress GUI
    %----------------------------------------------------------------------
    progressGUI.tool_main           = 'import';
    progressGUI.tool_sub            = 'load_design';
    progressGUI.status              = 'importing design data';
    progressGUI.structure           = 0;
    progressGUI.n_structures        = 0;
    progressGUI.pb_global_nfun      = 1;
    progressGUI.pb_local_percentage = 0;
    progressGUI.update();
    %----------------------------------------------------------------------

    % Separation between several data types
    if strcmp(userinput.design_filename(end-2:end), 'mat')
        design       = load(userinput.design_file);
        design_names = fieldnames(design);
        design_data  = design.(design_names{1}); 
        
    elseif strcmp(userinput.design_filename(end-2:end), 'png')
        design_data = imread(userinput.design_file);
        height = userinput.designsizeZ;
        design_data = double(design_data)./(2^(16)) .* height;
        
    else % here something went wrong
        errordlg('Wrong data type in design import function');
        
    end
    
    % convert to single-precision floating point
    design_data = single(design_data);

    % Update progress GUI
    %----------------------------------------------------------------------
    progressGUI.pb_local_percentage = 1;
    progressGUI.update();
    %----------------------------------------------------------------------

    
end

