function [progress_panel,overview_panel,ax,ax2,th,th2,ph,ph2]...
                = initialize_progress_gui(userinput)

    progress_gui = uifigure('Name','Quandalf','Position',userinput.progressGUIsize ,'WindowStyle','alwaysontop');
    progress_gui.Icon = 'images\Q_logo.png';
    im = uiimage(progress_gui,"ImageSource","images\quandalf_gui_header_tight.png","Position",[20 500 560 83],"ScaleMethod","fit");
    
    progress_panel = uipanel(progress_gui,"Position",[20 220 560 260],"BackgroundColor",[0.9 0.9 0.9]);
    overview_panel = uipanel(progress_gui,"Position",[20 20 560 180],"BackgroundColor",[0.9 0.9 0.9]);
    
    gif = uiimage(progress_panel,"Position",[20 100 280 140],"ImageSource",'images\quandalf_running.gif','ScaleMethod','fit');
    
    label_current_fun1 = uilabel(progress_panel,"Text",'Current status:',"Position",[315 220 210 20],"VerticalAlignment",'top','HorizontalAlignment','left','FontWeight','bold','FontSize',13);
    label_current_fun2 = uilabel(progress_panel,"Text",'initializing...',"Position",[315 190 210 20],"VerticalAlignment",'center','HorizontalAlignment','center','FontWeight','bold','FontSize',15);   

    label_current_fun1 = uilabel(progress_panel,"Text",'Processed measurement files:','WordWrap','on',"Position",[315 120 210 40],"VerticalAlignment",'top','HorizontalAlignment','left','FontWeight','bold','FontSize',13);
%     label_current_fun2 = uilabel(progress_panel,"Text",'0/10',"Position",[315 110 210 20],"VerticalAlignment",'center','HorizontalAlignment','center','FontWeight','bold','FontSize',17);   
    
    ax = axes(progress_panel,'Position',[20/560 50/220 520/560 20/220],'box','on','xtick',[],'ytick',[],...
    'color',[0.9 0.9 0.9],'xlim',[0,1],'ylim',[0,1]);
    ax.Toolbar.Visible = 'off';
    disableDefaultInteractivity(ax)

    ph = patch(ax,[0 0 0 0],[0 0 1 1],[167/255 199/255 231/255],'EdgeColor','none');
    
    th = text(ax,0.5,0.15,'0%','FontSize',10,'FontWeight','bold','VerticalAlignment','bottom','HorizontalAlignment','right'); 
    
    ax2 = axes(progress_panel,'Position',[20/560 20/220 520/560 20/220],'box','on','xtick',[],'ytick',[],...
    'color',[0.9 0.9 0.9],'xlim',[0,1],'ylim',[0,1]);
    ax2.Toolbar.Visible = 'off';
    disableDefaultInteractivity(ax2)

    ph2 = patch(ax2,[0 0 0 0],[0 0 1 1],[167/255 199/255 231/255],'EdgeColor','none');
    
    th2 = text(ax2,0.5,0.15,'0%','FontSize',10,'FontWeight','bold','VerticalAlignment','bottom','HorizontalAlignment','right'); 

%     cat_box = uipanel(overview_panel,"Position",[0 90 560 45],"BackgroundColor",[1 1 1]);
%     cat_box2= uipanel(overview_panel,"Position",[280 0 280 180],"BackgroundColor",[1 1 1]);
% 
%     cat_label_01 = uilabel(overview_panel,"Text","Import data files","Position",[20 135 260 45],"VerticalAlignment",'center',"HorizontalAlignment",'left',"FontSize",12,"FontWeight",'bold');
%     cat_label_02 = uilabel(overview_panel,"Text","Alignment of measurement and design","Position",[20 90 260 45],"VerticalAlignment",'center',"HorizontalAlignment",'left',"FontSize",12,"FontWeight",'bold');
%     cat_label_03 = uilabel(overview_panel,"Text","Evaluation of difference data","Position",[20 45 260 45],"VerticalAlignment",'center',"HorizontalAlignment",'left',"FontSize",12,"FontWeight",'bold');
%     cat_label_04 = uilabel(overview_panel,"Text","Calculation of the new design","Position",[20 0 260 45],"VerticalAlignment",'center',"HorizontalAlignment",'left',"FontSize",12,"FontWeight",'bold');
    

% 
%     % Create processing loop that updates the progress bar
%     n = 100; 
%     for i = 1:n
%         % update patch size and percentage text
%         ph.XData = [0 i/n i/n 0]; 
%         th.String = sprintf('%.0f%%',round(i/n*100)); 
%         drawnow %update graphics
%        
%     end
% 
%     
%     
%     % Create processing loop that updates the progress bar
%     n = 100; 
%     for i = 1:n
%         % update patch size and percentage text
%         ph2.XData = [0 i/n i/n 0]; 
%         th2.String = sprintf('%.0f%%',round(i/n*100)); 
%         drawnow %update graphics
%        
%     end


    drawnow

end