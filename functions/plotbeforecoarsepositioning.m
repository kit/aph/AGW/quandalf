function plotbeforecoarsepositioning(design, measurement_raw, ROI, userinput,progressGUI)
    
    % updating progress GUI
    %---------------------------------------------------------------------%
    progressGUI.tool_main           = 'alignment';
    progressGUI.tool_sub            = 'coarse_pos';
    progressGUI.status              = 'plotting heigth profiles before coarse positioning';
    progressGUI.structure           = 0;
    progressGUI.n_structures        = length(measurement_raw.Z);
    progressGUI.pb_local_percentage = 0;
    pb_global_nfun_initial          = 7;
    progressGUI.pb_global_nfun      = pb_global_nfun_initial;
    progressGUI.update();
    %---------------------------------------------------------------------%

    if userinput.generalplots

        for i = 1:length(measurement_raw.Z)

            ROIbCoarse = figure('Color','w','WindowStyle','normal','Position',progressGUI.figuresize_wide,'Name','Measurement data before coarse positioning');

            subplot(1,2,1)
            imagesc(design.x*1e3,design.y*1e3,design.Z*1e6);
            axis 'image';
            h = colorbar;
            h.FontSize = 12;
            ylabel(h,'{\it z} in µm');
            xlabel('{\it x} in mm');
            ylabel('{\it y} in mm');
            caxis([0,max(design.Z(:))*1e6]);
            title('Design data')

            ROI_SI = [measurement_raw.x{i}(ROI{i}(1)) measurement_raw.y{i}(ROI{i}(2)) mean(diff(measurement_raw.x{i}))*ROI{i}(3) mean(diff(measurement_raw.y{i}))*ROI{i}(4)];

            subplot(1,2,2)
            imagesc(measurement_raw.x{i}*1e3,measurement_raw.y{i}*1e3,measurement_raw.Z{i}*1e6);
            axis 'image';
            h = colorbar;
            h.FontSize = 12;
            ylabel(h,'{\it z} in µm');
            xlabel('{\it x} in mm');
            ylabel('{\it y} in mm');
            hr = rectangle('Position',ROI_SI*1e3);
            hr.EdgeColor = 'r';
            hr.LineWidth = 2;
            caxis([0,max(measurement_raw.Z{i}(:))*1e6]);
            title('Measurement data before coarse positioning')

            if userinput.saveplots

                exportgraphics(ROIbCoarse,strcat(userinput.exportpath,userinput.analysis_filename{i},'_overviewplot_before_coarsepos.png'),'BackgroundColor','w');

            end

            % Update progress GUI
            %--------------------------------------------------------------
            progressGUI.structure = i;
            progressGUI.pb_local_percentage = i/length(measurement_raw.Z);
            progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
            progressGUI.update();
            %--------------------------------------------------------------

        end
    
    else
        
        fprintf(1,'Skipping plot before coarse positioning due to user decision.\n')
        
    end
    
end