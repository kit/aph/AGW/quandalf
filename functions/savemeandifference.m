function savemeandifference(measurement_structure, difference_mean, mean_difference, std_difference, userinput, progressGUI)

    if userinput.generalplots
        
        % update progress GUI
        %------------------------------------------------------------------
        progressGUI.tool_main           = 'difference';
        progressGUI.tool_sub            = 'save_diff';
        progressGUI.status              = 'saving mean difference plot';
        progressGUI.structure           = 0;
        progressGUI.n_structures        = 0;
        progressGUI.pb_local_percentage = 0;
        pb_global_nfun_initial          = 19;
        progressGUI.pb_global_nfun      = pb_global_nfun_initial;
        progressGUI.update();
        %------------------------------------------------------------------    
       
        Figure_MeanDifference = figure('Color','w','WindowStyle','normal','Name','mean difference','Position',progressGUI.figuresize_small);

        imagesc(measurement_structure.x{1}*1e3,measurement_structure.y{1}*1e3,difference_mean*1e6);
        axis 'image';
        h = colorbar;
        h.FontSize = 12;
        ylabel(h,'difference in μm');
        xlabel('{\it x} in mm');
        ylabel('{\it y} in mm');

        caxis([(0-1e-6)*1e6,(0+1e-6)*1e6]);
        title('mean difference','Interpreter','none')
        
        if userinput.saveplots

            exportgraphics(Figure_MeanDifference,strcat(userinput.exportpath,userinput.analysis_filename{1},'_plot_mean_difference_std_',num2str(std_difference*1e9,6),'nm_mean_',num2str(mean_difference*1e9,6),'nm.png'),'BackgroundColor','w');

        end
    
    else

        % update progress GUI
        %------------------------------------------------------------------
        progressGUI.tool_main           = 'difference';
        progressGUI.tool_sub            = 'save_diff';
        progressGUI.status              = 'skipping mean difference plot export';
        progressGUI.structure           = 0;
        progressGUI.n_structures        = 0;
        progressGUI.pb_local_percentage = 0;
        pb_global_nfun_initial          = 19;
        progressGUI.pb_global_nfun      = pb_global_nfun_initial;
        progressGUI.update();
        %------------------------------------------------------------------  
        
        fprintf(1,'Skipping difference plot due to user decision.\n')
        
    end

    if userinput.saveplots
        fprintf(userinput.generaloutputfileID, 'Mean deviation of structure: %1.3f nm\n', mean_difference*1e9);
        fprintf(userinput.generaloutputfileID, 'Standard deviation of structure difference: %1.3f nm\n\n', std_difference*1e9);
    end

end