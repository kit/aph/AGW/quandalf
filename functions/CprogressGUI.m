classdef CprogressGUI
    % Manages the progress gui that provides constant updates on the current
    % status and progress of the computation

    properties (SetAccess = private)
        
        % general gui variables
        guiwindow
        guisize
        guiheader
        guigif
        guiicon

        % gui panels
        progress_panel
        overview_panel
        
        % current status display
        status_label
        iteration_label
        iteration_message
        
        % global progress bar
        pb_global_ax
        pb_global_text
        pb_global_patch    
        pb_global_percentage
        
        % local progress bar (inside of functions)
        pb_local_ax
        pb_local_text
        pb_local_patch
        
        % functions overview (main category, labels)
        catbox_main
        catbox_main_label_01
        catbox_main_label_02
        catbox_main_label_03
        catbox_main_label_04

        % functions overview (main category, icons)
        catbox_main_icon_01
        catbox_main_icon_02
        catbox_main_icon_03
        catbox_main_icon_04

        % functions overview (sub category, labels)
        catbox_sub
        catbox_sub_label_01
        catbox_sub_label_02
        catbox_sub_label_03
        catbox_sub_label_04
        catbox_sub_label_05

        % functions overview (sub category, icons)
        catbox_sub_icon_01
        catbox_sub_icon_02
        catbox_sub_icon_03
        catbox_sub_icon_04        
        catbox_sub_icon_05

        screen_size_px
        figuresize_small
        figuresize_wide
        figuresize_large

        nfun = 27
        
    end

    properties (SetAccess = public)
        
        % tool handles
        tool_main
        tool_sub

        % status message
        status
        status_message

        % progress bar variables
        pb_global_nfun
        pb_local_percentage
        structure
        n_structures
        update_enabled

    end

    methods
        
        function obj = CprogressGUI()
            %constructor
            
            obj = initialize_progress_gui(obj);

        end


        function obj = initialize_progress_gui(obj)
            
            % get screen resolution and define gui size
            %--------------------------------------------------------------
            set(0,'units','pixels')  
            obj.screen_size_px = get(0,'screensize');
            obj.guisize = [25 obj.screen_size_px(4)-650 600 603];
            
            % define figure sizes
            %--------------------------------------------------------------
            xPos         = obj.guisize(1)+obj.guisize(3)+5;
            yPos         = obj.guisize(2);
            yPos_large   = 100;
            height       = obj.guisize(4)-55;
            height_large = obj.guisize(2)+obj.guisize(4)-55-100;
            width_narrow = obj.guisize(3);
            width_wide   = obj.screen_size_px(3)-xPos-obj.guisize(1);
            
            obj.figuresize_small = [xPos yPos width_narrow height];
            obj.figuresize_wide  = [xPos yPos width_wide height];
            obj.figuresize_large = [xPos yPos_large width_wide height_large];

            % initialize gui window incl. icon and header
            %--------------------------------------------------------------
            progress_gui = uifigure('Name','Quandalf','Position',obj.guisize ,'WindowStyle','modal');
            obj.guiicon = 'images\Q_logo.png';
            progress_gui.Icon = obj.guiicon;
            obj.guiwindow = progress_gui;
            obj.guiheader = uiimage(progress_gui,"ImageSource","images\quandalf_gui_header_tight.png","Position",[20 500 560 83],"ScaleMethod","fit");
            
            obj.progress_panel = uipanel(progress_gui,"Position",[20 220 560 260],"BackgroundColor",[0.9 0.9 0.9]);
            obj.overview_panel = uipanel(progress_gui,"Position",[20 20 560 180],"BackgroundColor",[0.9 0.9 0.9]);
            

            % initialize progress panel
            %--------------------------------------------------------------
            obj.guigif = uiimage(obj.progress_panel,"Position",[20 100 280 140],"ImageSource",'images\quandalf_running.gif','ScaleMethod','fit');
            
            obj.status         = 'initializing...';
            obj.status_label   = uilabel(obj.progress_panel,"Text",'Current status:',"Position",[315 220 210 20],"VerticalAlignment",'top','HorizontalAlignment','left','FontWeight','bold','FontSize',13);
            obj.status_message = uilabel(obj.progress_panel,"Text",obj.status,"Position",[315 170 210 40],"VerticalAlignment",'top','HorizontalAlignment','center','FontWeight','bold','FontSize',15,'WordWrap','on');   
        
            obj.iteration_label = uilabel(obj.progress_panel,"Text",'Processed measurement files:','WordWrap','on',"Position",[315 120 210 40],"VerticalAlignment",'top','HorizontalAlignment','left','FontWeight','bold','FontSize',13);
            obj.iteration_message = uilabel(obj.progress_panel,"Text",'0/0',"Position",[315 110 210 20],"VerticalAlignment",'center','HorizontalAlignment','center','FontWeight','bold','FontSize',17);   
            
            % progress bar (local)
            obj.pb_local_ax = axes(obj.progress_panel,'Position',[20/560 50/220 520/560 20/220],'box','on','xtick',[],'ytick',[],'color',[0.9 0.9 0.9],'xlim',[0,1],'ylim',[0,1]);
            obj.pb_local_ax.Toolbar.Visible = 'off';
            disableDefaultInteractivity(obj.pb_local_ax)
            obj.pb_local_patch = patch(obj.pb_local_ax,[0 0 0 0],[0 0 1 1],[167/255 199/255 231/255],'EdgeColor','none');
            obj.pb_local_text = text(obj.pb_local_ax,0.5,0.15,'0%','FontSize',10,'FontWeight','bold','VerticalAlignment','bottom','HorizontalAlignment','right'); 
            
            % progress bar (global)
            obj.pb_global_ax = axes(obj.progress_panel,'Position',[20/560 20/220 520/560 20/220],'box','on','xtick',[],'ytick',[],'color',[0.9 0.9 0.9],'xlim',[0,1],'ylim',[0,1]);
            obj.pb_global_ax.Toolbar.Visible = 'off';
            disableDefaultInteractivity(obj.pb_global_ax)
            obj.pb_global_patch= patch(obj.pb_global_ax,[0 0 0 0],[0 0 1 1],[167/255 199/255 231/255],'EdgeColor','none');
            obj.pb_global_text = text(obj.pb_global_ax,0.5,0.15,'0%','FontSize',10,'FontWeight','bold','VerticalAlignment','bottom','HorizontalAlignment','right'); 
        

            % initialize overview panel
            %--------------------------------------------------------------
            obj.catbox_main = uipanel(obj.overview_panel,"Position",[0 135 560 45],"BackgroundColor",[1 1 1]);
            obj.catbox_sub  = uipanel(obj.overview_panel,"Position",[280 0 280 180],"BackgroundColor",[1 1 1]);
       
            obj.catbox_main_label_01 = uilabel(obj.overview_panel,"Text","Import data files","Position",[40 135 260 45],"VerticalAlignment",'center',"HorizontalAlignment",'left',"FontSize",12,"FontWeight",'bold');
            obj.catbox_main_label_02 = uilabel(obj.overview_panel,"Text","Alignment of measurement and design","Position",[40 90 260 45],"VerticalAlignment",'center',"HorizontalAlignment",'left',"FontSize",12,"FontWeight",'bold');
            obj.catbox_main_label_03 = uilabel(obj.overview_panel,"Text","Evaluation of difference data","Position",[40 45 260 45],"VerticalAlignment",'center',"HorizontalAlignment",'left',"FontSize",12,"FontWeight",'bold');
            obj.catbox_main_label_04 = uilabel(obj.overview_panel,"Text","Calculation of the new design","Position",[40 0 260 45],"VerticalAlignment",'center',"HorizontalAlignment",'left',"FontSize",12,"FontWeight",'bold');
            
            obj.catbox_main_icon_01 = uiimage(obj.overview_panel,"Position",[15 150 15 15],"ImageSource",'images\icon_inprogress.png');
            obj.catbox_main_icon_02 = uiimage(obj.overview_panel,"Position",[15 105 15 15],"ImageSource",'images\icon_pending.png');
            obj.catbox_main_icon_03 = uiimage(obj.overview_panel,"Position",[15 60 15 15],"ImageSource",'images\icon_pending.png');
            obj.catbox_main_icon_04 = uiimage(obj.overview_panel,"Position",[15 15 15 15],"ImageSource",'images\icon_pending.png');
            
            obj.catbox_sub_label_01 = uilabel(obj.catbox_sub,"Text","import design data","Position",[40 144 260 36],"VerticalAlignment",'center',"HorizontalAlignment",'left',"FontSize",12,"FontWeight",'bold');
            obj.catbox_sub_label_02 = uilabel(obj.catbox_sub,"Text","calculate xy-coordinates","Position",[40 108 260 36],"VerticalAlignment",'center',"HorizontalAlignment",'left',"FontSize",12,"FontWeight",'bold');
            obj.catbox_sub_label_03 = uilabel(obj.catbox_sub,"Text","import measurement data","Position",[40 72 260 36],"VerticalAlignment",'center',"HorizontalAlignment",'left',"FontSize",12,"FontWeight",'bold');
            obj.catbox_sub_label_04 = uilabel(obj.catbox_sub,"Text","","Position",[40 36 260 36],"VerticalAlignment",'center',"HorizontalAlignment",'left',"FontSize",12,"FontWeight",'bold');
            obj.catbox_sub_label_05 = uilabel(obj.catbox_sub,"Text","","Position",[40 0 260 36],"VerticalAlignment",'center',"HorizontalAlignment",'left',"FontSize",12,"FontWeight",'bold');

            obj.catbox_sub_icon_01 = uiimage(obj.catbox_sub,"Position",[15 155 15 15],"ImageSource",'images\icon_inprogress.png');
            obj.catbox_sub_icon_02 = uiimage(obj.catbox_sub,"Position",[15 119 15 15],"ImageSource",'images\icon_pending.png');
            obj.catbox_sub_icon_03 = uiimage(obj.catbox_sub,"Position",[15 83 15 15],"ImageSource",'images\icon_pending.png');
            obj.catbox_sub_icon_04 = uiimage(obj.catbox_sub,"Position",[15 47 15 15],"ImageSource",'images\icon_pending.png',"Visible","off");
            obj.catbox_sub_icon_05 = uiimage(obj.catbox_sub,"Position",[15 11 15 15],"ImageSource",'images\icon_pending.png',"Visible","off");

            drawnow
        end


        function obj = update(obj)
              
            % update progress panel
            %--------------------------------------------------------------
            obj.status_message.Text = obj.status;
             
            % iteration status
            obj.iteration_message.Text = [num2str(obj.structure),'/',num2str(obj.n_structures)];

            % global progress bar
            obj.pb_global_percentage = obj.pb_global_nfun/obj.nfun;
            obj.pb_global_patch.XData = [0 obj.pb_global_percentage obj.pb_global_percentage 0];
            obj.pb_global_patch.YData = [0 0 1 1];
            obj.pb_global_text.String = [num2str(round(obj.pb_global_percentage.*100)),'%'];

            % local progress bar
            obj.pb_local_patch.XData = [0 obj.pb_local_percentage obj.pb_local_percentage 0];
            obj.pb_local_patch.YData = [0 0 1 1];
            obj.pb_local_text.String = [num2str(round(obj.pb_local_percentage.*100)),'%'];
                
            % update overview panel
            %--------------------------------------------------------------        
           
            % import section
            if strcmp(obj.tool_main,'import') 
                pos_catbox_main = [0 135 560 45];
                obj.catbox_main_icon_01.ImageSource = 'images\icon_inprogress.png';
                obj.catbox_main_icon_02.ImageSource = 'images\icon_pending.png';
                obj.catbox_main_icon_03.ImageSource = 'images\icon_pending.png';
                obj.catbox_main_icon_04.ImageSource = 'images\icon_pending.png';
                
                if strcmp(obj.tool_sub,'load_design')
                    % already set in constructor
                elseif strcmp(obj.tool_sub,'calc_xy')
                    obj.catbox_sub_icon_01.ImageSource = 'images\icon_success.png';
                    obj.catbox_sub_icon_02.ImageSource = 'images\icon_inprogress.png';
                elseif strcmp(obj.tool_sub,'load_meas')
                    obj.catbox_sub_icon_02.ImageSource = 'images\icon_success.png';
                    obj.catbox_sub_icon_03.ImageSource = 'images\icon_inprogress.png';
                else
                    error('if the property >tool_main< is set to >import<, >tool_sub< has to be set to >load_design<, >calc_xy<, or >load_meas<.')
                end

            % alignment section    
            elseif strcmp(obj.tool_main,'alignment')
                
                pos_catbox_main = [0 90 560 45];

                if strcmp(obj.tool_sub,'offset_tilt')
                    
                    obj.catbox_main_icon_01.ImageSource = 'images\icon_success.png';
                    obj.catbox_main_icon_02.ImageSource = 'images\icon_inprogress.png';
                    obj.catbox_main_icon_03.ImageSource = 'images\icon_pending.png';
                    obj.catbox_main_icon_04.ImageSource = 'images\icon_pending.png';
                    
                    obj.catbox_sub_label_01.Text = 'offset and tilt correction';
                    obj.catbox_sub_label_02.Text = 'removal of alignment marks';
                    obj.catbox_sub_label_03.Text = 'coarse positioning';
                    obj.catbox_sub_label_04.Text = 'fine positioning via cross-correlation';
                    obj.catbox_sub_label_05.Text = 'cropping, rotating, and scaling';
                    
                    obj.catbox_sub_icon_04.Visible = 'on';
                    obj.catbox_sub_icon_05.Visible = 'on';

                    obj.catbox_sub_icon_01.ImageSource = 'images\icon_inprogress.png';
                    obj.catbox_sub_icon_02.ImageSource = 'images\icon_pending.png';
                    obj.catbox_sub_icon_03.ImageSource = 'images\icon_pending.png';
                    obj.catbox_sub_icon_04.ImageSource = 'images\icon_pending.png';
                    obj.catbox_sub_icon_05.ImageSource = 'images\icon_pending.png';

                elseif strcmp(obj.tool_sub,'alignment_mark')
                    obj.catbox_sub_icon_01.ImageSource = 'images\icon_success.png';
                    obj.catbox_sub_icon_02.ImageSource = 'images\icon_inprogress.png';
                    
                elseif strcmp(obj.tool_sub,'coarse_pos')
                    obj.catbox_sub_icon_02.ImageSource = 'images\icon_success.png';
                    obj.catbox_sub_icon_03.ImageSource = 'images\icon_inprogress.png';
                
                elseif strcmp(obj.tool_sub,'fine_pos')
                    obj.catbox_sub_icon_03.ImageSource = 'images\icon_success.png';
                    obj.catbox_sub_icon_04.ImageSource = 'images\icon_inprogress.png';

                elseif strcmp(obj.tool_sub,'crop_rot_scale')
                    obj.catbox_sub_icon_04.ImageSource = 'images\icon_success.png';
                    obj.catbox_sub_icon_05.ImageSource = 'images\icon_inprogress.png';

                else
                    error('if the property >tool_main< is set to >alignment<, >tool_sub< has to be set to >offset_tilt<, >alignment_mark<, >coarse_pos<, >fine_pos<, or >crop_rot_scale<.')
                end

            % difference section
            elseif strcmp(obj.tool_main,'difference')
                
                pos_catbox_main = [0 45 560 45];
                
                if strcmp(obj.tool_sub,'shrinkage')
                    
                    obj.catbox_main_icon_01.ImageSource = 'images\icon_success.png';
                    obj.catbox_main_icon_02.ImageSource = 'images\icon_success.png';
                    obj.catbox_main_icon_03.ImageSource = 'images\icon_inprogress.png';
                    obj.catbox_main_icon_04.ImageSource = 'images\icon_pending.png';
                    
                    obj.catbox_sub_label_01.Text = 'axial shrinkage analysis';
                    obj.catbox_sub_label_02.Text = 'calculation of difference data';
                    obj.catbox_sub_label_03.Text = 'cut plot analysis';
                    obj.catbox_sub_label_04.Text = 'selection & evaluation of difference data';
                    obj.catbox_sub_label_05.Text = 'save results and process parameters';

                    obj.catbox_sub_icon_01.ImageSource = 'images\icon_inprogress.png';
                    obj.catbox_sub_icon_02.ImageSource = 'images\icon_pending.png';
                    obj.catbox_sub_icon_03.ImageSource = 'images\icon_pending.png';
                    obj.catbox_sub_icon_04.ImageSource = 'images\icon_pending.png';
                    obj.catbox_sub_icon_05.ImageSource = 'images\icon_pending.png';

                elseif strcmp(obj.tool_sub,'calc_diff')
                    obj.catbox_sub_icon_01.ImageSource = 'images\icon_success.png';
                    obj.catbox_sub_icon_02.ImageSource = 'images\icon_inprogress.png';

                elseif strcmp(obj.tool_sub,'cut_plots')
                    obj.catbox_sub_icon_02.ImageSource = 'images\icon_success.png';
                    obj.catbox_sub_icon_03.ImageSource = 'images\icon_inprogress.png';
                
                elseif strcmp(obj.tool_sub,'sel_eval_diff')
                    obj.catbox_sub_icon_03.ImageSource = 'images\icon_success.png';
                    obj.catbox_sub_icon_04.ImageSource = 'images\icon_inprogress.png';

                elseif strcmp(obj.tool_sub,'save_diff')
                    obj.catbox_sub_icon_04.ImageSource = 'images\icon_success.png';
                    obj.catbox_sub_icon_05.ImageSource = 'images\icon_inprogress.png';

                else
                    error('if the property >tool_main< is set to >difference<, >tool_sub< has to be set to >shrinkage<, >calc_diff<, >cut_plots<, >sel_eval_diff<, or >save_diff<.')
                end

            % new design section
            elseif strcmp(obj.tool_main,'filtering')
                
                pos_catbox_main = [0 0 560 45];

                if strcmp(obj.tool_sub,'auto_filtering')

                    obj.catbox_main_icon_01.ImageSource = 'images\icon_success.png';
                    obj.catbox_main_icon_02.ImageSource = 'images\icon_success.png';
                    obj.catbox_main_icon_03.ImageSource = 'images\icon_success.png';
                    obj.catbox_main_icon_04.ImageSource = 'images\icon_inprogress.png';
                
                    obj.catbox_sub_label_01.Text = 'automated filtering';
                    obj.catbox_sub_label_02.Text = 'filtered difference plot';
                    obj.catbox_sub_label_03.Text = 'import previous design file';
                    obj.catbox_sub_label_04.Text = 'save filtered difference data';
                    obj.catbox_sub_label_05.Text = 'calculation of the new design';

                    obj.catbox_sub_icon_01.ImageSource = 'images\icon_inprogress.png';
                    obj.catbox_sub_icon_02.ImageSource = 'images\icon_pending.png';
                    obj.catbox_sub_icon_03.ImageSource = 'images\icon_pending.png';
                    obj.catbox_sub_icon_04.ImageSource = 'images\icon_pending.png';
                    obj.catbox_sub_icon_05.ImageSource = 'images\icon_pending.png';

                elseif strcmp(obj.tool_sub,'plot_filtered')
                    obj.catbox_sub_icon_01.ImageSource = 'images\icon_success.png';
                    obj.catbox_sub_icon_02.ImageSource = 'images\icon_inprogress.png';    
            
                elseif strcmp(obj.tool_sub,'load_previous')
                    obj.catbox_sub_icon_02.ImageSource = 'images\icon_success.png';
                    obj.catbox_sub_icon_03.ImageSource = 'images\icon_inprogress.png';
                
                elseif strcmp(obj.tool_sub,'save_filtered')
                    obj.catbox_sub_icon_03.ImageSource = 'images\icon_success.png';
                    obj.catbox_sub_icon_04.ImageSource = 'images\icon_inprogress.png';

                elseif strcmp(obj.tool_sub,'save_new')
                    obj.catbox_sub_icon_04.ImageSource = 'images\icon_success.png';
                    obj.catbox_sub_icon_05.ImageSource = 'images\icon_inprogress.png'; 
                
                elseif strcmp(obj.tool_sub,'done')
                    obj.catbox_main_icon_04.ImageSource = 'images\icon_success.png';
                    obj.catbox_sub_icon_05.ImageSource = 'images\icon_success.png'; 
                    obj.guigif.ImageSource = 'images\you_shall_not_shrink.gif';
                    obj.guigif.ScaleMethod = 'fill';

                else
                    error('if the property >tool_main< is set to >filtering<, >tool_sub< has to be set to >auto_filtering<, >plot_filtered<, >load_previous<, >save_filtered<, or >save_new<.')
                end

            elseif strcmp(obj.tool_main,'filteringApp')
                pos_catbox_main = [0 0 560 45];
                
                if strcmp(obj.tool_sub,'initial')
                    obj.catbox_main_icon_01.ImageSource = 'images\icon_success.png';
                    obj.catbox_main_icon_02.ImageSource = 'images\icon_success.png';
                    obj.catbox_main_icon_03.ImageSource = 'images\icon_success.png';
                    obj.catbox_main_icon_04.ImageSource = 'images\icon_inprogress.png';
                
                    obj.catbox_sub_label_01.Text = 'manual filtering via app';
                    obj.catbox_sub_label_02.Visible = 'off';
                    obj.catbox_sub_label_03.Visible = 'off';
                    obj.catbox_sub_label_04.Visible = 'off';
                    obj.catbox_sub_label_05.Visible = 'off';

                    obj.catbox_sub_icon_01.ImageSource = 'images\icon_inprogress.png';
                    obj.catbox_sub_icon_02.Visible = 'off';
                    obj.catbox_sub_icon_03.Visible = 'off';
                    obj.catbox_sub_icon_04.Visible = 'off';
                    obj.catbox_sub_icon_05.Visible = 'off';

                elseif strcmp(obj.tool_sub,'done')
                    obj.catbox_sub_icon_01.ImageSource = 'images\icon_success.png';
                    obj.guigif.ImageSource = 'images\you_shall_not_shrink.gif';
                    obj.guigif.ScaleMethod = 'fill';
                end

            else
                error('progressGUI.tool_main has to be on of the following handles >import<, >alignment<, >difference<, or >filtering<')
            end

            obj.catbox_main.Position = pos_catbox_main;
            drawnow
            

           
        end

        % set functions
        %------------------------------------------------------------------
        
        function obj = set.tool_main(obj,val)
            obj.tool_main = val;
        end

        function obj = set.tool_sub(obj,val)
            obj.tool_sub = val;
        end

        function obj = set.status(obj,val)
            obj.status = val;
        end

    end

end