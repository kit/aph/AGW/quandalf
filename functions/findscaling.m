function [x_scaling, y_scaling] = findscaling(design_Z_resized, measurement_Z, xcorrsize_px, correct_angle, maximumshift, x_scale_coarse, y_scale_coarse, userinput)
%========================================================================== 
%                       FIND SCALING
%==========================================================================
% In this function the scaling factors in x- and y-direction are
% determined. Therefore, the maximum value of the cross-correlation is
% optimized by variing the scaling factors in x- and y-direction. The
% result is given back to the finpositioning function.
%--------------------------------------------------------------------------
% written for: QUANDALF V1.4
% last modified on: March 8, 2023
% last modified by: Jannis Weinacker
%--------------------------------------------------------------------------
% INPUTS:
% - design_Z_resized:            [single-matrix]  design data
% - measurement_Z:               [single-matrix]  measurement data
% - xcorrsize_px:                [double]  number of pixels to cross-
%                                        correlate 
% - correct_angle:               [double] already found optimal rotation
%                                        angle
% - maximumshift:                [int] 2x size of the cross correlation
%                                        function and therefore the maximum 
%                                        displacement that is considered
% - x_scale_coarse:              [double] scaling factor between the measurement and
%                           the design in x-direction from coarse
%                           positioning
% - y_scale_coarse:              [double] scaling factor between the measurement and
%                           the design in y-direction from coarse
%                           positioning
% - userinput:                   [object] class object containing information
%                                        about design, exports etc.
%-------------------------------------------------------------------------
% OUTPUTS:
% - x_scaling:          [cell of double] scaling factor between the
%                                   measurement and the design in x-direction
% - y_scaling:          [cell of double] scaling factor between the
%                                   measurement and the design in y-direction
%==========================================================================
    
    % define parameters for fminsearch
    optns = optimset(...
    'Display','iter',...
    'MaxFunEvals', 100,...
    'TolFun', 1e-6,...
    'TolX', 1e-6,...
    'PlotFcns',@optimplotfval);

    InitialCoeffs = [double(x_scale_coarse), double(y_scale_coarse)]; 

    if userinput.xcorr_mode == 0
        % Perform fminsearch via xcorr2 built-in function

        tic
        % Determine the rotation angle by maximizing the xcorr value
        [scales,~]...
            = fminsearch(@(x)scaleandxcorr(...
            imresize(design_Z_resized,userinput.downscalingfactor), ... scale down design by downscaling factor
            imresize(measurement_Z,userinput.downscalingfactor),... scale down measurement by downscaling factor
            xcorrsize_px * userinput.downscalingfactor, ... Reduce xcorrsize
            xcorrsize_px * userinput.downscalingfactor, ... Reduce xcorrsize
            correct_angle, maximumshift, x(1), x(2), userinput), InitialCoeffs, optns);
        toc

    elseif userinput.xcorr_mode == 1
        % Perform  fminsearch via dftregistration function
        % For documentation see: Manuel Guizar-Sicairos, Samuel T. Thurman, and James R. Fienup, "Efficient subpixel image registration algorithms," Opt. Lett. 33, 156-158 (2008)
        % "https://de.mathworks.com/matlabcentral/fileexchange/18401-efficient-subpixel-image-registration-by-cross-correlation"

        % This mode is dependent on an equal size of both design and
        % measurement, meaning kernel and moving image

        tic
        % Determine the rotation angle by maximizing the xcorr value
        [scales,~]...
            = fminsearch(@(x)scaleandxcorr_dft(...
            imresize(design_Z_resized,userinput.downscalingfactor), ... scale down design by downscaling factor
            imresize(measurement_Z,userinput.downscalingfactor),... scale down measurement by downscaling factor
            xcorrsize_px * userinput.downscalingfactor, ... Reduce xcorrsize
            xcorrsize_px * userinput.downscalingfactor, ... Reduce xcorrsize
            correct_angle, x(1), x(2), userinput), InitialCoeffs, optns);
        toc

    else
        error('No valid corss-correlation method given')
    end

    x_scaling = scales(1);
    y_scaling = scales(2);

end