function plotafterxcorrpositioning(design, measurement_structure, userinput, progressGUI)

    % update progress GUI
    %---------------------------------------------------------------------%
    progressGUI.tool_main           = 'alignment';
    progressGUI.tool_sub            = 'crop_rot_scale';
    progressGUI.status              = 'plotting results after fine positioning';
    progressGUI.structure           = 0;
    progressGUI.n_structures        = length(measurement_structure.Z);
    progressGUI.pb_local_percentage = 0;
    pb_global_nfun_initial          = 11;
    progressGUI.pb_global_nfun      = pb_global_nfun_initial;
    progressGUI.update();
    %---------------------------------------------------------------------%

    if userinput.generalplots

        for i = 1:length(measurement_structure.Z)

            ROIaxcorr = figure('Color','w','WindowStyle','normal','Position',progressGUI.figuresize_wide,'Name','Measurement data after xcorr positioning');

            subplot(1,2,1)
            imagesc(design.x*1e3,design.y*1e3,design.Z*1e6);
            axis 'image';
            h = colorbar;
            h.FontSize = 12;
            ylabel(h,'{\it z} in µm');
            xlabel('{\it x} in mm');
            ylabel('{\it y} in mm');
            caxis([0,max(design.Z(:))*1e6]);
            title('Design data')

            subplot(1,2,2)
            imagesc(measurement_structure.x{i}*1e3,measurement_structure.y{i}*1e3,measurement_structure.Z{i}*1e6);
            axis 'image';
            h = colorbar;
            h.FontSize = 12;
            ylabel(h,'{\it z} in µm');
            xlabel('{\it x} in mm');
            ylabel('{\it y} in mm');
            caxis([0,max(measurement_structure.Z{i}(:))*1e6]);
            title('Measurement data after xcorr positioning')

            if userinput.saveplots

                exportgraphics(ROIaxcorr,strcat(userinput.exportpath,userinput.analysis_filename{i},'_overviewplot_after_xcorrpos.png'),'BackgroundColor','w');
    
            end

            % Update progress GUI
            %--------------------------------------------------------------
            progressGUI.structure = i;
            progressGUI.pb_local_percentage = i/length(measurement_structure.Z);
            progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
            progressGUI.update();
            %--------------------------------------------------------------

        end
    
    else
        
        fprintf(1,'Skipping plot after xcorr positioning due to user decision.\n')
        
    end
    
end