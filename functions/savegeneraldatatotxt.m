function informationfileID = savegeneraldatatotxt(userinput)

    if userinput.saveplots
        
        informationfileID = fopen(strcat(userinput.exportpath,userinput.analysis_filename,'_analysis_information.txt'),'w');
        
        fprintf(informationfileID,'== ANALYSIS OF STRUCTRE %s ==\n\n',userinput.analysis_filename);
        fprintf(informationfileID,'Used design data for difference calculation: %s\n',userinput.design_file);
        fprintf(informationfileID,'Design size  : %1.4f mm x %1.4f mm\n',userinput.designsizeX*1e3,userinput.designsizeY*1e3);
        fprintf(informationfileID,'Design height: %1.3f um\n\n',userinput.designsizeZ*1e6);
        fprintf(informationfileID,'Design      kernel size for xcorr: %d px x %d px\n',userinput.xcorrsize_design,userinput.xcorrsize_design);
        fprintf(informationfileID,'Measurement kernel size for xcorr: %d px x %d px\n',userinput.xcorrsize_measurement,userinput.xcorrsize_measurement);
        
    else
        
        informationfileID = 1;
        
    end
        
end