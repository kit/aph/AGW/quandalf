function filtered_data_Z = remove_alignment_marks(measurement_raw,design,userinput,progressGUI)
%========================================================================== 
%                         REMOVE ALIGNMENT MARKS
%==========================================================================
% function to filter out alignment marks or other objects (dust particles
% etc.) on the substrate before the measurement data set is further
% processed. The aligment marks or other unwanted objects are automatically
% detected, removed, and interpolated. This filtering is only applied to
% objects on the substrate and not to objects on top the structures. It is
% possible to load in both a data set with multiple structures as well as 
% multiple data sets. This feature is meant to improve the performance of
% the subsequent data processing steps and is especially helpful if the
% structure(s) to be analyzed do(es) not fill the corresponding rectangular
% bounding box entirely. 
% Please note that the structures should be larger and higher than the
% alignment marks or dirt particles on the substrate in other to get a
% properly working object detection.
%--------------------------------------------------------------------------
% written for: QUANDALF V1.4
% last modified on: June 11, 2024
% last modified by: Jannis Weinacker
%--------------------------------------------------------------------------
% INPUTS:
% - measurement_raw:   [cell of single-matrices] offset- and tilt-corrected
%                                                confocal data
% - design             [single-matrix] height data of the design in meters
% - userinput          [object] class object containing information about
%                               design, exports etc.
% - progressGUI:       [class object] containing everything GUI-related
%--------------------------------------------------------------------------
% CALLED FUNCTIONS:
% - findetectionthreshold
% - remove_alignment_marks_plotting
%--------------------------------------------------------------------------
% OUTPUTS:
% - filtered_data_Z:   [cell of single-matrices] confocal data without 
%                                                alignment marks
%--------------------------------------------------------------------------
% FIGURE OUTPUTS:
% - filtering process control figure
%==========================================================================
    
    % update progress GUI
    %---------------------------------------------------------------------%
    progressGUI.tool_main           = 'alignment';
    progressGUI.tool_sub            = 'alignment_mark';
    progressGUI.status              = 'removing alignment marks and dust particles';
    progressGUI.structure           = 0;
    progressGUI.n_structures        = length(measurement_raw.Z);
    progressGUI.pb_local_percentage = 0;
    pb_global_nfun_initial          = 5;
    progressGUI.pb_global_nfun      = pb_global_nfun_initial;
    progressGUI.update();
    %---------------------------------------------------------------------%

    if userinput.enable_marker_removal
        disp('-----------------------------------------------------------------------')
        disp('ALIGNMENT MARK REMOVAL ROUTINE')
        disp('removing alignment marks and other unwanted objects on the substrate...')
        
        % Determine the number of structures in the design file
        %----------------------------------------------------------------------
        
        % convert the design file to a black-and-white image
        design_Z_temp = design.Z;
        design_Z_temp(design_Z_temp~=0) = 1;
        design_Z_bw = logical(design_Z_temp);
    
        % determine the number of logical clusters in the black-and-white image
        design_region_props = regionprops(design_Z_bw,'Area');
        design_region_areas = [design_region_props.Area];
        N_objects_design = size(design_region_areas,2);
    
    
        % Remove unwanted objects and generate the filtered height profile
        %----------------------------------------------------------------------
    
        filtered_data_Z = cell(length(measurement_raw.Z),1);
    
        % process multiple data sets (if required)
        for i = 1:length(measurement_raw.Z)
            
            measurement_data_Z = measurement_raw.Z{i};
            
    
            % generate a mask that covers the intended structures
            %------------------------------------------------------------------
            
            % determine a suitable height threshhold
            detection_threshold_coarse = findetectionthreshold(measurement_data_Z); % height in meters to detect a sructure
            
            % initialize the structure mask
            mask_structures = ones(size(measurement_data_Z));
            
            % detect all structures heigher than the determined threshold
            mask_structures(measurement_data_Z<detection_threshold_coarse) = 0;
            
            % convert the mask containing the detected objects into a bw image
            mask_structures_labeled = bwlabel(mask_structures);
            mask_structures_bw = logical(mask_structures_labeled);
            
            % determine the area of the detected objects
            mask_structures_bw_region_props = regionprops(mask_structures_bw,'Area'); 
            mask_structures_bw_areas = [mask_structures_bw_region_props.Area];    
            
            % identify the N largest objects as the intended structures
            largest_areas = maxk(mask_structures_bw_areas,N_objects_design);
            area_threshold = round(0.9*min(largest_areas));
            
            % remove all other objects
            mask_structures_final = bwareaopen(mask_structures_bw,area_threshold);
            
            % increase the structure masks by a few pixels
            se90 = strel('line',3,90);
            se0 = strel('line',3,0);
            mask_structures_final = imdilate(mask_structures_final,[se90,se0]);
    
    
            % generate mask that covers the unwanted structures
            %------------------------------------------------------------------
            
            % use a smaller height detection threshold
            detection_threshold_fine = detection_threshold_coarse/4;
            
            % initialize the object mask
            mask_alignment_marks = ones(size(measurement_data_Z));
    
            % detect all objects higher than the threshold
            mask_alignment_marks(measurement_data_Z<detection_threshold_fine) = 0;
            
            % convert the mask to a black-and-white image
            mask_alignment_marks_bw = logical(mask_alignment_marks);
            
            % remove the intended structures from the object mask
            mask_alignment_marks_bw = mask_alignment_marks_bw - mask_structures_final;
    
            % increase the object masks by a few pixels
            mask_alignment_marks_bw_final = imdilate(mask_alignment_marks_bw,[se90,se0]); 
           
    
            % Remove the unwanted objects
            %------------------------------------------------------------------
            
            % generate a mask for the clean substrate (no structures, no objects)
            mask_substrate = (~mask_structures_final).*(~mask_alignment_marks_bw_final);
            
            % extract the height data of the substrate (later used for fitting)
            substrate_raw_Z = measurement_data_Z.*mask_substrate;
            [col_grid, row_grid] = meshgrid(1:size(substrate_raw_Z,2),1:size(substrate_raw_Z,1));
            
            % determine and format known data to create a substrate fit model
            [row,col,z_values] = find(substrate_raw_Z);
    	    fit_input_row = double(row);
            fit_input_col = double(col);
            fit_input_z = double(z_values);
        
            % find a fit plane to model the substrate's height profile
            sf = fit([fit_input_row, fit_input_col], fit_input_z, 'poly11');
            
            % fill the empty spots with the determined plane fit model
            substrate_interpolated = substrate_raw_Z + (~mask_substrate).*sf(row_grid,col_grid);
            
            % apply a 2D Gauss filter on the interpolated substrate plane
            substrate_filtered = imgaussfilt(substrate_interpolated,1);
            
            % Generate the final height profile by combining the structure data
            % and the interpolated and filtered substrate data
            Z_data_filtered = substrate_filtered.*(~mask_structures_final)...
                + measurement_data_Z.*mask_structures_final;
            
            % return the filtered height profile
            filtered_data_Z{i} = Z_data_filtered;
    
    
            % Generate a control plot
            %--------------------------------------------------------------
    
            remove_alignment_marks_plotting(i,measurement_raw,Z_data_filtered,...
                mask_structures_final,mask_alignment_marks_bw_final,userinput,progressGUI)
        
            % Update progress GUI
            %--------------------------------------------------------------
            progressGUI.structure = i;
            progressGUI.pb_local_percentage = i/length(measurement_raw.Z);
            progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
            progressGUI.update();
            %--------------------------------------------------------------    
    
        end

        disp('alignment mark removal routine completed!')
        disp('-----------------------------------------------------------------------')

    else
        disp('alignment mark removal routine disabled!')
        filtered_data_Z = measurement_raw.Z;
    end
   

end