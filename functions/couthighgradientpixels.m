function num_high_gradient_pixels = couthighgradientpixels(img, grad_threshold)
%==========================================================================
%                       COUNT HIGH GRADIENT PIXELS
%==========================================================================
% Counts the number of pixels in a grayscale image that have a gradient
% magnitude greater than a given threshold.
%
% written for: QUANDALF V1.4
% last modified on: March 22, 2023
% last modified by: ChatGPT (OpenAI)
%
%--------------------------------------------------------------------------
% INPUTS:
% - img:             [MxN single/double]  Grayscale image.
%                     Input image.
%
% - grad_threshold:  [single/double]     Gradient magnitude threshold.
%                     Threshold value for the gradient magnitude.
%
%--------------------------------------------------------------------------
% OUTPUTS:
% - num_high_gradient_pixels:
%                    [single/double]     Number of pixels with gradient
%                     magnitude greater than the threshold.
%
%==========================================================================

    % Calculate the gradient magnitude using the imgradient function
    [grad_mag, ~] = imgradient(img);
    
    % Count the number of pixels with a gradient magnitude greater than the threshold
    num_high_gradient_pixels = sum(grad_mag(:) > grad_threshold);

end
