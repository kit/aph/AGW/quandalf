function FOM = rotandxcorr(design_Z_resized, measurement_Z, xcorrsize_design, xcorrsize_measurement, maximumshift, angle, userinput)
%========================================================================== 
%                       ROT AND XCORR
%==========================================================================
% Rotates the measurement about the given angle and calculates the
% cross-correlation with the design. The output is the maximum value of
% the cross-correlation (calculated with xcorr2 algorithm) function. 
% The function can be used to find the best fitting rotation angle.
%--------------------------------------------------------------------------
% written for: QUANDALF V1.1
% last modified on: March 8, 2023
% last modified by: Jannis Weinacker
%--------------------------------------------------------------------------
% INPUTS:
% - design_Z_resized:            [single-matrix]  design data
% - measurement_Z:               [single-matrix]  measurement data
% - xcorrsize_design:            [double]  number of pixels to cross
%                                        correlate in design
% - xcorrsize_measurement:       [double]  number of pixels to cross
%                                        correlate in measurement
% - maximumshift:                [int] 2x size of the cross correlation
%                                        function and therefore the maximum 
%                                        displacement that is considered
% - angle:                       [double]  rotation angle
% - userinput:                   [object] class object containing information
%                                        about design, exports etc.
%-------------------------------------------------------------------------
% OUTPUTS:
% - FOM:                         [double]  Maximum value of the cross-
%                                        correlation function
%==========================================================================
    
    % Create a region of interest for the measurement data (rectangle)
    crosscorr_ROI_exp =...
        [round(size(design_Z_resized,2)/2)-round(xcorrsize_measurement/2)... x-coordinate of the upper left corner
        round(size(design_Z_resized,1)/2)-round(xcorrsize_measurement/2)...  y-coordinate of the upper left corner
        xcorrsize_measurement...                                             size of the rectangle (pixels)
        xcorrsize_measurement];                                            % size of the rectangle (pixels)
    
    % Create a region of interest for the design data (rectangle)
    crosscorr_ROI_theo =...
        [round(size(design_Z_resized,2)/2)-round(xcorrsize_design/2)... x-coordinate of the upper left corner
        round(size(design_Z_resized,1)/2)-round(xcorrsize_design/2)...  y-coordinate of the upper left corner
        xcorrsize_design...                                             size of the rectangle (pixels)
        xcorrsize_design];                                            % size of the rectangle (pixels)
    
    % Rotate the measurement data about the angle and crop it to the ROI
    measurement_Z_crosscrop = imcrop(imrotate(measurement_Z,angle,'crop'),crosscorr_ROI_exp);      
    % Crop the design data to the ROI
    design_Z_resized_crosscrop = imcrop(design_Z_resized,crosscorr_ROI_theo);    
    
    % Copy data to GPU if set by user
    if userinput.gpu_enable
        measurement_Z_crosscrop = gpuArray(measurement_Z_crosscrop); 
        design_Z_resized_crosscrop = gpuArray(design_Z_resized_crosscrop);
    end

    % Normalize design and measurement
    measurement_Z_crosscrop    = measurement_Z_crosscrop    ./ max(measurement_Z_crosscrop(:));
    design_Z_resized_crosscrop = design_Z_resized_crosscrop ./ max(design_Z_resized_crosscrop(:));
    
    % Pad array with zero to control the size of the calculated cross
    % correlation
    measurement_Z_crosscrop = padarray(measurement_Z_crosscrop, [maximumshift maximumshift]);

    % perform xcorr
    XCorr = xcorr2_small(measurement_Z_crosscrop, design_Z_resized_crosscrop) / sum(sum(design_Z_resized_crosscrop.^2));
    
    % Extract the maximum value
    [max_cc, ~] = max(abs(XCorr(:)));
    
    % Flip sign, so that a minimum is searched
    FOM = gather(-max_cc);
    
end