function welcome_to_quandalf()
    
    version = 'V2.1';
    version_date = 'January 2024';

    set(0,'units','pixels')  
    screen_size_px = get(0,'screensize');
           
    GUIsize = [(screen_size_px(3)-980)/2 (screen_size_px(4)-500)/2 980 500];

    welcome_ui    = uifigure('Name','Welcome to Quandalf','Position',GUIsize ,'WindowStyle','modal');
    welcome_ui.Icon = 'images\Q_logo.png';
    im = uiimage(welcome_ui,"ImageSource","images\quandalf_welcome_gui.png","Position",[0 0 982 500],"ScaleMethod","fill");
    start = uibutton(welcome_ui,"state","Position",[360 30 260 60],"Text","Start Quandalf","FontSize",20,"FontWeight","bold","BackgroundColor",[167/255 199/255 231/255],"FontColor",[1 1 1],"Value",false);
    version_label = uilabel(welcome_ui,"Position",[500 10 470 20],"Text",[version,', ',version_date],"FontName","Arial","FontWeight",'bold',"FontSize",17,"FontColor",[1 1 1],'HorizontalAlignment','right','VerticalAlignment','bottom');

    drawnow

    waitfor(start,'Value')
    close(welcome_ui)

end