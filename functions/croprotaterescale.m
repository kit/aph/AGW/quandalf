function measurement_structure = croprotaterescale(ROI_fine, rotationangle, x_scaling, y_scaling, measurement_raw, design, progressGUI)
%========================================================================== 
%                       CROP ROTATE SCALE
%==========================================================================
% This function rotates the measurement about the already determined angle
% and crops it to the size of the design. Afterwards it is scaled to the
% number of pixels of the design so that both files have the exact same
% size and resolution. This is a condition to calculate the difference
% afterwards.
%--------------------------------------------------------------------------
% written for: QUANDALF V1.4
% last modified on: June 3, 2023
% last modified by: S. Kalt
%--------------------------------------------------------------------------
% INPUTS:
% - ROI_fine:               [cell of 1x4 matrices] region of interest found in the
%                                       fine positioning
% - rotationangle:          [cell of doubles] optimal rotation angle found in the
%                                       fine positioning
% - x_scaling:              [cell of double] scaling factor between the measurement and
%                                       the design in x-direction 
% - y_scaling:              [cell of double] scaling factor between the measurement and
%                                       the design in y-direction 
% - measurement_raw:        [cell of single-matrices] confocal data
% - design:                 [single-matrix] design data
% - progressGUI:            [class object] containing everything GUI-related
%-------------------------------------------------------------------------
% OUTPUTS:
% - measurement_structure:  [cell of single-matrices] cropped, rotated and scaled
%                                       confocal data
%==========================================================================
    
    % update progress GUI
    %----------------------------------------------------------------------
    progressGUI.tool_main           = 'alignment';
    progressGUI.tool_sub            = 'crop_rot_scale';
    progressGUI.status              = 'cropping, rotating, and scaling measurement data';
    progressGUI.structure           = 0;
    progressGUI.n_structures        = length(measurement_raw.Z);
    progressGUI.pb_local_percentage = 0;
    pb_global_nfun_initial          = 10;
    progressGUI.pb_global_nfun      = pb_global_nfun_initial;
    progressGUI.update();
    %----------------------------------------------------------------------


    % Determine the size of one pixel in meters in x and y direction
    pixelsize_design_x = mean(diff(design.x));
    pixelsize_design_y = mean(diff(design.y));

    % Get the number of pixels of the design image in x and y direction
    [Ny, Nx] = size(design.Z);
    
    % Preallocate cell arrays
    measurement_structure.x = cell(length(measurement_raw.Z),1);
    measurement_structure.y = cell(length(measurement_raw.Z),1);
    measurement_structure.Z = cell(length(measurement_raw.Z),1);

    for i = 1:length(measurement_raw.Z)
    
        measurement_structure.Z{i} = imrotate(measurement_raw.Z{i}, rotationangle{i}, 'crop');
        measurement_structure.Z{i} = imresize(measurement_structure.Z{i},...
            size(measurement_structure.Z{i}) ./ [y_scaling{i}, x_scaling{i}]);
        measurement_structure.Z{i} = imcrop(measurement_structure.Z{i}, ROI_fine{i});
        measurement_structure.Z{i} = imresize(measurement_structure.Z{i}, [Ny, Nx]);

        % Perform also the rescaling of the lateral measurement arrays
        % Those two should now be exactly the same as in design
        measurement_structure.x{i} = ((1:Nx) - (Nx+1)/2) * pixelsize_design_x;
        measurement_structure.y{i} = ((1:Ny) - (Ny+1)/2) * pixelsize_design_y;
        
        
        % Update progress GUI
        %------------------------------------------------------------------
        progressGUI.structure = i;
        progressGUI.pb_local_percentage = i/length(measurement_raw.Z);
        progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
        progressGUI.update();
        %------------------------------------------------------------------

    end
    
end

