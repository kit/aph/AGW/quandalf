function FOM = scaleandxcorr_dft(design_Z_resized, measurement_Z, xcorrsize_design, xcorrsize_measurement, angle, x_scaling, y_scaling, userinput)
%========================================================================== 
%                       SALCE AND XCORR DFT
%==========================================================================
% Scales the measurement about the given scaling factors and calculates the
% cross-correlation with the design. The output is the maximum value of
% the cross-correlation (calculated with dft algorithm) function. 
% The function can be used to find the best fitting rotation angle.
%--------------------------------------------------------------------------
% written for: QUANDALF V1.4
% last modified on: March 2, 2023
% last modified by: Jannis Weinacker
%--------------------------------------------------------------------------
% INPUTS:
% - design_Z_resized:            [single-matrix]  design data
% - measurement_Z:               [single-matrix]  measurement data
% - xcorrsize_design:            [double]  number of pixels to cross
%                                        correlate in design
% - xcorrsize_measurement:       [double]  number of pixels to cross
%                                        correlate in measurement
% - angle:                       [double]  rotation angle
% - x_scaling:                   [double] scaling factor between the
%                                   measurement and the design in x-direction
% - y_scaling:                   [double] scaling factor between the
%                                   measurement and the design in y-direction
% - userinput:                   [object] class object containing information
%                                        about design, exports etc.
%-------------------------------------------------------------------------
% OUTPUTS:
% - FOM:                         [double]  Maximum value of the cross-
%                                        correlation function
%==========================================================================
    
    % Create a region of interest for the measurement data (rectangle)
    crosscorr_ROI_exp =...
        [round(size(design_Z_resized,2)/2)-round(xcorrsize_measurement/2)... x-coordinate of the upper left corner
        round(size(design_Z_resized,1)/2)-round(xcorrsize_measurement/2)...  y-coordinate of the upper left corner
        xcorrsize_measurement...                                             size of the rectangle (pixels)
        xcorrsize_measurement];                                            % size of the rectangle (pixels)
    
    % Create a region of interest for the design data (rectangle)
    crosscorr_ROI_theo =...
        [round(size(design_Z_resized,2)/2)-round(xcorrsize_design/2)... x-coordinate of the upper left corner
        round(size(design_Z_resized,1)/2)-round(xcorrsize_design/2)...  y-coordinate of the upper left corner
        xcorrsize_design...                                             size of the rectangle (pixels)
        xcorrsize_design];                                            % size of the rectangle (pixels)
    
    % Rotate the measurement data about the angle
    measurement_Z_crosscrop = imrotate(measurement_Z, angle, 'crop');  
    % Scale the measurement data about the scaling factors
    measurement_Z_crosscrop = imresize(measurement_Z_crosscrop, size(measurement_Z_crosscrop) ./ [y_scaling, x_scaling]);
    % Crop measurement to the coarse ROI
    measurement_Z_crosscrop = imcrop(measurement_Z_crosscrop, crosscorr_ROI_exp ./ [x_scaling, y_scaling, 1, 1]);
    
    % Crop the design data to the ROI
    design_Z_resized_crosscrop = imcrop(design_Z_resized, crosscorr_ROI_theo);

    % Determine the new minimum sizes and set ROIs to it
    sizeX = min(size(design_Z_resized_crosscrop, 2), size(measurement_Z_crosscrop, 2));
    sizeY = min(size(design_Z_resized_crosscrop, 1), size(measurement_Z_crosscrop, 1));

    % Crop the design and measurement to smalles common sizes
    rect_design = [floor(flip(size(design_Z_resized_crosscrop)-[sizeY-2 sizeX-2])/2) sizeX sizeY] - [0 0 1 1];
    rect_measurement = [floor(flip(size(measurement_Z_crosscrop)-[sizeY-2 sizeX-2])/2) sizeX sizeY] - [0 0 1 1];
    design_Z_resized_crosscrop = imcrop(design_Z_resized_crosscrop, rect_design);
    measurement_Z_crosscrop = imcrop(measurement_Z_crosscrop, rect_measurement);

    % Normalize design and measurement
    measurement_Z_crosscrop    = measurement_Z_crosscrop    ./ max(measurement_Z_crosscrop(:));
    design_Z_resized_crosscrop = design_Z_resized_crosscrop ./ max(design_Z_resized_crosscrop(:));
    
    % Copy data to GPU if set by user
    if userinput.gpu_enable % Copy arrays to GPU if wanted
        measurement_Z_crosscrop = gpuArray(measurement_Z_crosscrop); 
        design_Z_resized_crosscrop = gpuArray(design_Z_resized_crosscrop);
    end
    
    % First try to perform FFT on GPU. If this does not work, do it on CPU
    try 
        % Perform FFT
        measurement_Z_crosscrop_FT = fft2(measurement_Z_crosscrop);
        design_Z_resized_crosscrop_FT = fft2(design_Z_resized_crosscrop);
    catch % Try without gpuArray again if memory is a problem
        measurement_Z_crosscrop = gather(measurement_Z_crosscrop);
        design_Z_resized_crosscrop = gather(design_Z_resized_crosscrop);
        
        % Perform FFT
        measurement_Z_crosscrop_FT = fft2(measurement_Z_crosscrop);
        design_Z_resized_crosscrop_FT = fft2(design_Z_resized_crosscrop);
        
        % Copy data to GPU if set by user
        if userinput.gpu_enable % Copy arrays to GPU if wanted
            measurement_Z_crosscrop_FT = gpuArray(measurement_Z_crosscrop_FT); 
            design_Z_resized_crosscrop_FT = gpuArray(design_Z_resized_crosscrop_FT);
        end
    end

    % Free GPU memory from untransformed arrays
    clear measurement_Z_crosscrop design_Z_resized_crosscrop
    
    % perform xcorr via dftregistration
    usfac = 0; % upscaling factor for DFT registration. See dftregistration documentation for more info
    [output, Greg] = dftregistration(measurement_Z_crosscrop_FT, design_Z_resized_crosscrop_FT, usfac);
    
    FOM = gather(output(1));
    
end