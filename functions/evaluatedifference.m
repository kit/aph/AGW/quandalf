function [mean_difference, std_difference] = evaluatedifference(difference_mean, measurement_structure, difference_choice, userinput, progressGUI, identifier)
%========================================================================== 
%                       EVALUATE DIFFERENCE  
%==========================================================================
% In this function the differences between the measurements and the design
% is analyzed and some figure of merits are calculated. In this case the
% standard deviation and the average absolute value of the difference image
% are returned. The calculation of those FOMs happens only in the regions
% where the structure is detected via the height.
%--------------------------------------------------------------------------
% written for: QUANDALF V1.3
% last modified on: January 09, 2024
% last modified by: S. Kalt
%--------------------------------------------------------------------------
% INPUTS:
% - difference_mean:          [single array]  average difference from all
%                                       selected measurement data sets
% - measurement_structure:    [cell (single arrays)]  measurement data
% - difference_choice:        [logical array] chosen difference data by
%                                       user
% - userinput:                [object] class object containing information
%                                       about design, exports etc.
% - progressGUI:              [class object] containing everything GUI-related
% - identifier:               [var] identifier for the title of the plot
%--------------------------------------------------------------------------
% OUTPUTS:
% - mean_difference:          [double] average absolute value of the
%                                       difference data in the ROI
% - std_difference:           [double] standard deviation of the average
%                                       difference data in the ROI
%==========================================================================
    
    % update progress GUI
    %----------------------------------------------------------------------
    if progressGUI.update_enabled
        progressGUI.tool_main           = 'difference';
        progressGUI.tool_sub            = 'sel_eval_diff';
        progressGUI.status              = 'analyzing difference data';
        progressGUI.structure           = 0;
        progressGUI.n_structures        = length(measurement_structure.Z);
        progressGUI.pb_local_percentage = 0;
        pb_global_nfun_initial          = 18;
        progressGUI.pb_global_nfun      = pb_global_nfun_initial;
        progressGUI.update();
    end
    %----------------------------------------------------------------------

    % preallocate logical array for structure recognition
    detectionstructuremask = ones(size(measurement_structure.Z{1}));

    for i=1:length(measurement_structure.Z)
        
        % Determine detection threshold from the histogram data
        detection_thresholds = findetectionthreshold(measurement_structure.Z{i});

        % Create logical field of all pixels heigher than the threshold
        structure = measurement_structure.Z{i} > detection_thresholds;
        
        % AND connect all the structure logical fields to get the smallest
        % suitable mask for all structures
        if difference_choice(i)
            detectionstructuremask = and(detectionstructuremask, structure);
        end

        % Update progress GUI
        %------------------------------------------------------------------
        if progressGUI.update_enabled
            progressGUI.structure = i;
            progressGUI.pb_local_percentage = i/(length(measurement_structure.Z)+1);
            progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
            progressGUI.update();
        end
        %------------------------------------------------------------------
        
    end
    
    % Consider only pixels that correspont to the structure
    difference_mean_selected = difference_mean(detectionstructuremask);

    % Shift difference so that mean value is 0
    difference_mean_selected = difference_mean_selected - mean(difference_mean_selected(:));
    
    % Calculate different figure of merits
    mean_difference = mean(abs(difference_mean_selected));
    std_difference  = std(difference_mean_selected);

    % Print results to console
    fprintf(1, 'Mean deviation of structure: %1.3f nm\n', mean_difference * 1e9);
    fprintf(1, 'Standard deviation of structure difference: %1.3f nm\n', std_difference * 1e9);

    % Plot histogram of difference pixels
    if userinput.generalplots
        pos_local = progressGUI.figuresize_small;
        pos_local(1) = progressGUI.figuresize_small(1)+progressGUI.figuresize_small(3)+5; 
        F = figure('Color','w','Position',pos_local);
        histogram( difference_mean_selected(:) * 1e6 ); % Generate histogram object
        xlabel('Difference value in µm')
        ylabel('Frequency')
        xlim([-0.5 0.5])

        % Determin title for the plot (and filename) depending on the input
        if isnumeric(identifier)
            if (floor(identifier) == identifier) && (identifier > 0) && (identifier <= length(userinput.analysis_filename))
                plottitle = userinput.analysis_filename{identifier};
            else
                plottitle = strcat('Identifier_', num2str(identifier));
            end
        elseif isstring(identifier)
            plottitle = identifier;
        elseif ischar(identifier)
            plottitle = identifier;
        else
            plottitle = 'Difference_Histogram';
        end

        title(plottitle, 'Interpreter', 'none');
        
        % Save plot as png file
        if userinput.saveplots
            exportgraphics(F, strcat(userinput.exportpath, plottitle, '_plot_difference_histogram.png'), 'BackgroundColor', 'w');
        end
    end

    % Update progress GUI
    %----------------------------------------------------------------------
    if progressGUI.update_enabled
        progressGUI.pb_local_percentage = 1;
        progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
        progressGUI.update();
    end
    %----------------------------------------------------------------------
    
end