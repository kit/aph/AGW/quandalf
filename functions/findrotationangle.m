function correct_angle = findrotationangle(design_Z_resized, measurement_Z, xcorrsize_px, maximumshift, userinput)
%========================================================================== 
%                       FIND ROTATION ANGLE
%==========================================================================
% Here the angle between the measurement and the design is determined. This
% happens by a cross correlation. The angle is a variable and the maximum
% value of the cross-correlation function will be maximized as a function
% of the rotation angle. The resulting angle is returned.
%--------------------------------------------------------------------------
% written for: QUANDALF V1.1
% last modified on: March 8, 2023
% last modified by: Jannis Weinacker
%--------------------------------------------------------------------------
% INPUTS:
% - design_Z_resized:            [single-matrix]  design data
% - measurement_Z:               [single-matrix]  measurement data
% - xcorrsize_px:                [double]  number of pixels to cross-
%                                        correlate 
% - maximumshift:                [int] 2x size of the cross correlation
%                                        function and therefore the maximum 
%                                        displacement that is considered
% - userinput:                   [object] class object containing information
%                                        about design, exports etc.
%-------------------------------------------------------------------------
% OUTPUTS:
% - correct_angle:               [double]  determined optimal rotation
%                                           angle
%==========================================================================

    % define parameters for fminsearch
    optns = optimset(...
    'Display','iter',...
    'MaxFunEvals', 100,...
    'TolFun', 1e-6,...
    'TolX', 1e-6,...
    'PlotFcns',@optimplotfval);

    InitialCoeffs = 5;
    
    if userinput.xcorr_mode == 0
        % Perform fminsearch via xcorr2 built-in function

        tic
        % Determine the rotation angle by maximizing the xcorr value
        [correct_angle,~]...
            = fminsearch(@(x)rotandxcorr(...
            imresize(design_Z_resized,userinput.downscalingfactor), ... scale down design by downscaling factor
            imresize(measurement_Z,userinput.downscalingfactor),... scale down measurement by downscaling factor
            xcorrsize_px * userinput.downscalingfactor, ... Reduce xcorrsize
            xcorrsize_px * userinput.downscalingfactor, ... Reduce xcorrsize
            maximumshift, x, userinput), InitialCoeffs, optns);
        toc

    elseif userinput.xcorr_mode == 1
        % Perform  fminsearch via dftregistration function
        % For documentation see: Manuel Guizar-Sicairos, Samuel T. Thurman, and James R. Fienup, "Efficient subpixel image registration algorithms," Opt. Lett. 33, 156-158 (2008)
        % "https://de.mathworks.com/matlabcentral/fileexchange/18401-efficient-subpixel-image-registration-by-cross-correlation"

        % This mode is dependent on an equal size of both design and
        % measurement, meaning kernel and moving image

        tic
        % Determine the rotation angle by maximizing the xcorr value
        [correct_angle,~]...
            = fminsearch(@(x)rotandxcorr_dft(...
            imresize(design_Z_resized,userinput.downscalingfactor), ... scale down design by downscaling factor
            imresize(measurement_Z,userinput.downscalingfactor),... scale down measurement by downscaling factor
            xcorrsize_px * userinput.downscalingfactor, ... Reduce xcorrsize
            xcorrsize_px * userinput.downscalingfactor, ... Reduce xcorrsize
            x, userinput), InitialCoeffs, optns);
        toc

    else
        error('No valid corss-correlation method given')
    end

end