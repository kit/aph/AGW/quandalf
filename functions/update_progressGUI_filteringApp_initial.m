function [] = update_progressGUI_filteringApp_initial(progressGUI)

    % update progress GUI
    %----------------------------------------------------------------------
    progressGUI.tool_main           = 'filteringApp';
    progressGUI.tool_sub            = 'initial';
    progressGUI.status              = 'running filtering app';
    progressGUI.structure           = 0;
    progressGUI.n_structures        = 0;
    progressGUI.pb_local_percentage = 0;
    pb_global_nfun_initial          = 22;
    progressGUI.pb_global_nfun      = pb_global_nfun_initial;
    progressGUI.update();
    %----------------------------------------------------------------------

end