function [] = update_progressGUI_filteringApp_final(progressGUI)

    % update progress GUI
    %----------------------------------------------------------------------
    progressGUI.tool_main           = 'filteringApp';
    progressGUI.tool_sub            = 'done';
    progressGUI.status              = 'process done';
    progressGUI.status_message.FontColor = [0 168/255 0];
    progressGUI.structure           = 0;
    progressGUI.n_structures        = 0;
    progressGUI.pb_local_percentage = 1;
    pb_global_nfun_initial          = 27;
    progressGUI.pb_global_nfun      = pb_global_nfun_initial;
    progressGUI.update();
    %----------------------------------------------------------------------

end