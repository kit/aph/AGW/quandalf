function BrowseButtonPushed(ui_element,ui_element_2,gui_figure,type,buzzword,file_formats,label_num,label_name,ui_return,ui_continue)
%========================================================================== 
%                          BROWSE BUTTON PUSHED
%==========================================================================
% callback function to define what happens if a "Browse" button in a
% quandalf ui is pushed to load either files or directories. 
%--------------------------------------------------------------------------
% written for: QUANDALF V1.5
% last modified on: May 31, 2023
% last modified by: Sebastian Kalt
%--------------------------------------------------------------------------
% INPUTS:
% - ui_element      [uieditfield] editfield to show the selected file or
%                                 directory path
% - ui_element_2    [uicontrol]   has to be a (hidden) listbox to return a
%                                 cell of multiple filenames if multiselect
%                                 is enabled
% - gui_figure      [uifigure]    the current uifigure
% - type            [string]      has to be set to either 'path' or 'file'
% - buzzword        [string]      has to be set to 'export', 'design', or
%                                 'measurment'
% - file_formats    [Nx1 cell]    contains optional file type limitations 
%                                 for importing data files
% - label_num       [uilabel]     shows the number of selected files
% - label_name      [uilabel]     shows the name of the first selected file
% - ui_return       [uicheckbox]  invisible checkbox to transfer infos
% - ui_continue     [uibutton]    ui continue button for dual request guis
%--------------------------------------------------------------------------
% OUTPUTS/PROPERTIES SET:
% - ui_element.Value
% - ui_element_2.String
% - label_num.Text
% - label_name.Text
% - gui_figure.WindowStyle
% - ui_return.Value
% - ui_continue.Enable
%==========================================================================      

    % Callback for browsing directory paths
    %----------------------------------------------------------------------
    if strcmp(type,'path')
        
        figure_title = ['Select ',buzzword, ' path'];
        selpath = uigetdir("Title",figure_title);
        ui_element.Value = [selpath,'\'];

        % put gui_figure to the foreground
        gui_figure.WindowStyle = 'modal';

    % Callback for browsing file paths
    %----------------------------------------------------------------------
    elseif strcmp(type,'file')
        
        figure_title = ['Select ',buzzword, ' file'];
        
        % callback for getting a design file (multiselect not enabled)
        if strcmp(buzzword,'design') 

            [filename, path, ~] = uigetfile(file_formats,"Title",figure_title,"MultiSelect",'off');
            file = [path, filename];
            ui_element.Value = file;
            
            label_num.Text = '1';
            label_name.Text = filename;
           
            % Enable ui_continue if design and measurement are loaded
            if ui_return.Value == 0
                ui_return.Value = 1;
            else
                ui_continue.Enable = 'on';
            end

            % put gui_figure to the foreground
            gui_figure.WindowStyle = 'modal';

        elseif strcmp(buzzword,'previous design')
            
            [filename, path, ~] = uigetfile(file_formats,"Title",figure_title,"MultiSelect",'off');
            file = [path, filename];
            ui_element.Value = file;

            ui_continue.Enable = 'on';

            gui_figure.WindowStyle = 'modal';
        % callback for getting measurement files (multiselect enabled)    
        elseif strcmp(buzzword,'measurement')
            
            [filename, path, ~] = uigetfile(file_formats, 'MultiSelect', 'on', 'Please select measurement data');

            if iscell(filename)
                file = cellfun(@(x) [path, x], filename, 'UniformOutput', false);
                ui_element.Value = file{1};

                label_num.Text = num2str(size(file,2));
                label_name.Text = filename{1};
            else
                file = [path, filename];
                ui_element.Value = file;

                label_num.Text = '1';
                label_name.Text = filename;
            end
            
            % uieditfield can only handle single string elements. Therefore
            % a hidden listbox is used to return a cell array containing
            % all selected measurement files

            if iscell(file)
                ui_element_2.String = file;
            else
                ui_element_2.String = {file};
            end
            
            % Enable ui_continue if design and measurement are loaded
            if ui_return.Value == 0
                ui_return.Value = 1;
            else
                ui_continue.Enable = 'on';
            end

            % put gui_figure to the foreground
            gui_figure.WindowStyle = 'modal';



        else
            error('choose from the buzzwords >design<, >measurement<, or >export<.')
        end
        
      
    else
        error('the input variable >type< should be >path< or >file<')
    end

end




