function [ROI_fine, correct_angle, x_scaling, y_scaling] = xcorr_finepositioning(design, measurement_raw, ROI_coarse, x_scale_coarse, y_scale_coarse, userinput, progressGUI)
%========================================================================== 
%                       xCORR FINEPOSITIONING
%==========================================================================
% This function provides the fine positioning of the confocal measurement
% and the design. The lateral displacement between both matricies is
% determined using cross-correlation. The corss-correlation function
% thereby is called multiple times with different angles of rotation of the
% images. The right rotation angle is considered to be the one with the
% highes maximum in the cross-correlation function, since it is assumed to
% have the best overlap. To find this optimal angle the MatLab function 
% fminsearch is used.
%--------------------------------------------------------------------------
% written for: QUANDALF V1.4
% last modified on: June 3, 2023
% last modified by: S. Kalt
%--------------------------------------------------------------------------
% INPUTS:
% - design:             [struct of single-matricies] design data
% - measurement_raw:    [cell of single-matrices] confocal measurement data
% - ROI_coarse:         [cell of 1x4 matrices] region of interest detemined on coarse
%                                   positioning
% - x_scale_coarse:     [cell of double] scaling factor between the measurement and
%                           the design in x-direction from coarse
%                           positioning
% - y_scale_coarse:     [cell of double] scaling factor between the measurement and
%                           the design in y-direction from coarse
%                           positioning
% - userinput:          [object] class object containing information
%                                    about design, exports etc.
% - progressGUI:       [class object] containing everything GUI-related
%-------------------------------------------------------------------------
% OUTPUTS:
% - ROI_fine:           [cell of 1x4 matrices] region of interest with corners and 
%                                       lenghts 
% - correct_angle:      [cell of double] optimal angle of rotation between design
%                                   and measurement
% - x_scaling:          [cell of double] scaling factor between the
%                                   measurement and the design in x-direction
% - y_scaling:          [cell of double] scaling factor between the
%                                   measurement and the design in y-direction
%==========================================================================

    % update progress GUI
    %---------------------------------------------------------------------%
    progressGUI.tool_main           = 'alignment';
    progressGUI.tool_sub            = 'fine_pos';
    progressGUI.status              = 'performing fine positioning';
    progressGUI.structure           = 0;
    progressGUI.n_structures        = length(measurement_raw.Z);
    progressGUI.pb_local_percentage = 0;
    pb_global_nfun_initial          = 9;
    progressGUI.pb_global_nfun      = pb_global_nfun_initial;
    progressGUI.update();
    %---------------------------------------------------------------------%

    pixelsize_measurement = mean(diff(measurement_raw.x{1}));
    maximumshift = double(round(10e-6/pixelsize_measurement));

    ROI_fine = cell(length(ROI_coarse),1);
    correct_angle = cell(length(ROI_coarse),1);
    x_scaling = cell(length(ROI_coarse),1);
    y_scaling = cell(length(ROI_coarse),1);

    for i = 1:length(measurement_raw.Z)

        % Cut measurement to region found by coarse positioning
        measurement_Z_coarsecrop = imcrop(measurement_raw.Z{i}, ROI_coarse{i} - [0 0 1 1]);

        % Rescale design to measurement resolution
        nx_design = ROI_coarse{i}(3) - 200; % substract 200 since ROI is 200 px too large
        ny_design = ROI_coarse{i}(4) - 200; 
        design_Z_resized = imresize(design.Z, [ny_design, nx_design]);
        design_Z_resized = padarray(design_Z_resized, [100 100]);

        % number of pixels to apply xcorr
        xcorrsize_px = userinput.xcorrsize * max(ROI_coarse{i}(3), ROI_coarse{i}(4)) ;

        % Find the rotation angle between measurement and design
        if userinput.findangle
            fprintf(1,'===== %s (%i/%i) =====\n', userinput.analysis_filename{i}, i, length(measurement_raw.Z));
            correct_angle{i} = findrotationangle(design_Z_resized, measurement_Z_coarsecrop, xcorrsize_px, maximumshift, userinput);
            
            % Update progress GUI
            %--------------------------------------------------------------
            if userinput.findscaling
                progressGUI.pb_local_percentage = (3*(i-1)/3+1/3)/length(measurement_raw.Z);
            else
                progressGUI.pb_local_percentage = (2*(i-1)/2+1/2)/length(measurement_raw.Z);
            end
            progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
            progressGUI.update();
            %--------------------------------------------------------------
        else
            correct_angle{i} = 0;
        end

        % Find correct latheral scale between measurement and design
        if userinput.findscaling
            % Find the scaling factors that lead to an optimal
            % cross-correlation value
            [x_scaling{i}, y_scaling{i}] = findscaling(design_Z_resized, measurement_Z_coarsecrop, xcorrsize_px, correct_angle{i}, maximumshift, x_scale_coarse{i}, y_scale_coarse{i}, userinput);

            % Scale and crop the measurement data
            measurement_raw.Z{i} = imresize(measurement_raw.Z{i},...
                size(measurement_raw.Z{i}) ./ [y_scaling{i}, x_scaling{i}]);
            measurement_Z_coarsecrop = imcrop(measurement_raw.Z{i}, (ROI_coarse{i} - [0 0 1 1]) ./ [x_scaling{i}, y_scaling{i}, 1, 1]);
            

            % Update progress GUI
            %--------------------------------------------------------------
            if userinput.findangle
                progressGUI.pb_local_percentage = (3*(i-1)/3+2/3)/length(measurement_raw.Z);
            else
                progressGUI.pb_local_percentage = (2*(i-1)/2+1/2)/length(measurement_raw.Z);
            end
            progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
            progressGUI.update();
            %--------------------------------------------------------------

        else
            x_scaling{i} = 1;
            y_scaling{i} = 1;
        end

        % Find the shift between measurement and design
        [x_shift_cross, y_shift_cross] = findshift(design_Z_resized, measurement_Z_coarsecrop, xcorrsize_px, correct_angle{i}, maximumshift, userinput);

        % Adjust the ROI to the new shifting position
        ROI_fine{i} = ROI_coarse{i} ./ [x_scaling{i}, y_scaling{i}, 1, 1] + [x_shift_cross y_shift_cross 0 0]; 
        

        % Update progress GUI
        %------------------------------------------------------------------
        progressGUI.structure = i;
        progressGUI.pb_local_percentage = i/length(measurement_raw.Z);
        progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
        progressGUI.update();
        %------------------------------------------------------------------
    
    end
    
end