function CancelButtonPushed(gui_figure)
    close(gui_figure)
    drawnow
    error('Quandalf terminated by user')
end