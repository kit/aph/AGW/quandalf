function FOM = rotandxcorr_dft(design_Z_resized, measurement_Z, xcorrsize_design, xcorrsize_measurement, angle, userinput)
%========================================================================== 
%                       ROT AND XCORR
%==========================================================================
% Rotates the measurement about the given angle and calculates the
% cross-correlation with the design. The output is the maximum value of
% the cross-correlation (calculated with dft-registration algorithm) 
% function. 
% The function can be used to find the best fitting rotation angle.
%--------------------------------------------------------------------------
% written for: QUANDALF V1.1
% last modified on: February 17, 2023
% last modified by: Jannis Weinacker
%--------------------------------------------------------------------------
% INPUTS:
% - design_Z_resized:            [single-matrix]  design data
% - measurement_Z:               [single-matrix]  measurement data
% - xcorrsize_design:            [double]  number of pixels to cross
%                                        correlate in design
% - xcorrsize_measurement:       [double]  number of pixels to cross
%                                        correlate in measurement
% - angle:                       [double]  rotation angle
% - userinput:                   [object] class object containing information
%                                        about design, exports etc.
%-------------------------------------------------------------------------
% OUTPUTS:
% - FOM:                         [double]  Maximum value of the cross-
%                                        correlation function
%==========================================================================

    % Create a region of interest for the measurement data (rectangle)
    crosscorr_ROI_exp =...
        [round(size(design_Z_resized,2)/2)-round(xcorrsize_measurement/2)... x-coordinate of the upper left corner
        round(size(design_Z_resized,1)/2)-round(xcorrsize_measurement/2)...  y-coordinate of the upper left corner
        xcorrsize_measurement...                                             size of the rectangle (pixels)
        xcorrsize_measurement];                                            % size of the rectangle (pixels)
    
    % Create a region of interest for the design data (rectangle)
    crosscorr_ROI_theo =...
        [round(size(design_Z_resized,2)/2)-round(xcorrsize_design/2)... x-coordinate of the upper left corner
        round(size(design_Z_resized,1)/2)-round(xcorrsize_design/2)...  y-coordinate of the upper left corner
        xcorrsize_design...                                             size of the rectangle (pixels)
        xcorrsize_design];                                            % size of the rectangle (pixels)

    % Rotate and crop measurement to ROI (add tolerance?)
    measurement_Z_crosscrop = imcrop(imrotate(measurement_Z, angle, 'crop'), crosscorr_ROI_exp);
    % Crop design to ROI
    design_Z_resized_crosscrop = imcrop(design_Z_resized, crosscorr_ROI_theo);
    
    % Normalize to maximum value of 1
    measurement_Z_crosscrop    = measurement_Z_crosscrop ./ max(measurement_Z_crosscrop(:));
    design_Z_resized_crosscrop = design_Z_resized_crosscrop ./ max(design_Z_resized_crosscrop(:));
    
    % Copy data to GPU if set by user
    if userinput.gpu_enable % Copy arrays to GPU if wanted
        measurement_Z_crosscrop = gpuArray(measurement_Z_crosscrop); 
        design_Z_resized_crosscrop = gpuArray(design_Z_resized_crosscrop);
    end
    
    % First try to perform FFT on GPU. If this does not work, do it on CPU
    try 
        % Perform FFT
        measurement_Z_crosscrop_FT = fft2(measurement_Z_crosscrop);
        design_Z_resized_crosscrop_FT = fft2(design_Z_resized_crosscrop);
    catch % Try without gpuArray again if memory is a problem
        measurement_Z_crosscrop = gather(measurement_Z_crosscrop);
        design_Z_resized_crosscrop = gather(design_Z_resized_crosscrop);
        
        % Perform FFT
        measurement_Z_crosscrop_FT = fft2(measurement_Z_crosscrop);
        design_Z_resized_crosscrop_FT = fft2(design_Z_resized_crosscrop);
        
        % Copy data to GPU if set by user
        if userinput.gpu_enable % Copy arrays to GPU if wanted
            measurement_Z_crosscrop_FT = gpuArray(measurement_Z_crosscrop_FT); 
            design_Z_resized_crosscrop_FT = gpuArray(design_Z_resized_crosscrop_FT);
        end
    end

    % Free GPU memory from untransformed arrays
    clear measurement_Z_crosscrop design_Z_resized_crosscrop
    
    % perform xcorr via dftregistration
    usfac = 0; % upscaling factor for DFT registration. See dftregistration documentation for more info
    [output, Greg] = dftregistration(measurement_Z_crosscrop_FT, design_Z_resized_crosscrop_FT, usfac);
    
    FOM = gather(output(1));
        
end