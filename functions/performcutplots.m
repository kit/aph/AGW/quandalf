function [] = performcutplots(design, measurement_structure, userinput,progressGUI)
%========================================================================== 
%                            PERFORM CUT PLOTS
%==========================================================================
% This function generates cut plots in x- and y-direction through the
% center of the structures.
%--------------------------------------------------------------------------
% written for: QUANDALF V1.4
% last modified on: January 09, 2024
% last modified by: S. Kalt
%--------------------------------------------------------------------------
% INPUTS:
% - design                     [struct, single-arrays]  design data
%   - design.x                 
%   - design.y                 
%   - design.Z                 
% - measurement_structure      [struct, single-arrays]  measurement data
%   - measurement_structure.x  
%   - measurement_structure.y
%   - measurement_structure.Z
% - userinput                  [object] class object containing information
%                                    about design, exports etc.
% - progressGUI:               [class object] containing everything GUI-related
%-------------------------------------------------------------------------
% OUTPUTS:
% - none
%==========================================================================


    % update progress GUI
    %----------------------------------------------------------------------
    progressGUI.tool_main           = 'difference';
    progressGUI.tool_sub            = 'cut_plots';
    progressGUI.status              = 'generating cut plots';
    progressGUI.structure           = 0;
    progressGUI.n_structures        = length(measurement_structure.Z);
    progressGUI.pb_local_percentage = 0;
    pb_global_nfun_initial          = 15;
    progressGUI.pb_global_nfun      = pb_global_nfun_initial;
    progressGUI.update();
    %----------------------------------------------------------------------

    disp('Plotting 1D cuts...');
        
    for i = 1:length(measurement_structure.Z)

        % Check whether measurement is rescaled and cropped to design
        if size(design.Z) ~= size(measurement_structure.Z{i})
            warndlg('Measurement data is not of the same size as design data. Please consider rescaling first...');
        end

        % Distance away from x and y center position, e.g. due to stitching seams
        off_center_x = 0e-6; % 30e-6;
        off_center_y = 0e-6; % 30e-6;

        % Resolution (pixelsize) of measurement data
        x_res = mean(diff(measurement_structure.x{i}));
        y_res = mean(diff(measurement_structure.y{i}));

        % Indizes of center positions
        x_meas_center_index = round(length(measurement_structure.x{i})/2);
        y_meas_center_index = round(length(measurement_structure.y{i})/2);

        % Indizes for plotting 1D cuts (may be shifted off center due to
        % stitching seams)
        x_meas_cut_index = round(length(measurement_structure.x{i})/2) + round(off_center_x/x_res);
        y_meas_cut_index = round(length(measurement_structure.y{i})/2) + round(off_center_y/y_res);

        % Calculate height difference of design and measurement
        range_height_diff = 30e-6; %What is this? Why is it hardcoded? (JW)
        height_meas = mean(measurement_structure.Z{i}(y_meas_center_index-round(range_height_diff/y_res):y_meas_center_index+round(range_height_diff/y_res), ...
            x_meas_center_index-round(range_height_diff/x_res):x_meas_center_index+round(range_height_diff/x_res)), 'all');
        height_design = mean(design.Z(y_meas_center_index-round(range_height_diff/y_res):y_meas_center_index+round(range_height_diff/y_res), ...
            x_meas_center_index-round(range_height_diff/x_res):x_meas_center_index+round(range_height_diff/x_res)), 'all');
        height_difference = height_meas - height_design; % Positive values -> measurement is higher than design

        design_plot = design.Z + height_difference;

        % Limits for plotting
        xy_plot_lim = 100e-6;
        z_plot_lim = 2e-6;



        OneDCuts = figure('Color','w','WindowStyle','normal','Position',progressGUI.figuresize_large,'Name','1D Cuts');
        
        %-----------------------------------------------------------------%
        %                           plot x cut                            %
        %-----------------------------------------------------------------%
        subplot(2,2,1)
        area(design.x*1e3,design_plot(y_meas_cut_index,:)*1e6,'EdgeColor','k','FaceColor','k','FaceAlpha',0.1,'LineWidth',2);
        hold on
        plot(measurement_structure.x{i}*1e3,measurement_structure.Z{i}(y_meas_cut_index,:)*1e6,'-','LineWidth',2,'Color','r');
        hold off

        grid on
        xlim(1e3*[min(design.x)-xy_plot_lim, max(design.x)+xy_plot_lim]);
        ylim(1e6*[0, max(measurement_structure.Z{i}(y_meas_cut_index,:))+z_plot_lim]);

        xlabel('{\it x} in mm');
        ylabel('{\it z} in µm');
        legend({'Design Data', 'Measured Data'},'Location','northeast');
        title('x cut')




        %-----------------------------------------------------------------%
        %              plot experimental data with x cut line             %
        %-----------------------------------------------------------------%
        subplot(2,2,2)
        imagesc(measurement_structure.x{i}*1e3,measurement_structure.y{i}*1e3,measurement_structure.Z{i}*1e6);axis 'image';
        h = colorbar;
        h.FontSize = 12;
    %     xlim(1e3*[min(design.x)-xy_plot_lim, max(design.x)+xy_plot_lim]);
    %     ylim(1e3*[min(design.y)-xy_plot_lim, max(design.y)+xy_plot_lim]);
        ylabel(h,'{\it z} in µm');
        xlabel('{\it x} in mm');
        ylabel('{\it y} in mm');
        yline(off_center_x*1e3,'LineWidth',2,'Color','r');
        caxis(1e6*[0, max(measurement_structure.Z{i}(y_meas_cut_index,:))+z_plot_lim]);
        title('Experimental data: x cut')


        %-----------------------------------------------------------------%
        %                           plot y cut                            %
        %-----------------------------------------------------------------%

        subplot(2,2,3)
        area(design.y*1e3,design_plot(:,x_meas_cut_index)*1e6,'EdgeColor','k','FaceColor','k','FaceAlpha',0.1,'LineWidth',2);
        hold on
        plot(measurement_structure.y{i}*1e3,measurement_structure.Z{i}(:,x_meas_cut_index)*1e6,'-','LineWidth',2,'Color','r');
        hold off

        grid on
        xlim(1e3*[min(design.y)-xy_plot_lim, max(design.y)+xy_plot_lim]);
        ylim(1e6*[0, max(measurement_structure.Z{i}(:,x_meas_cut_index))+z_plot_lim]);

        xlabel('{\it y} in mm');
        ylabel('{\it z} in µm');
        legend({'Design Data', 'Measured Data'},'Location','northeast');
        title('y cut')


        %-----------------------------------------------------------------%
        %              plot experimental data with y cut line             %
        %-----------------------------------------------------------------%
       
        subplot(2,2,4)
        imagesc(measurement_structure.x{i}*1e3,measurement_structure.y{i}*1e3,measurement_structure.Z{i}*1e6);axis 'image';
        h = colorbar;
        h.FontSize = 12;
    %     xlim(1e3*[min(design.x)-xy_plot_lim, max(design.x)+xy_plot_lim]);
    %     ylim(1e3*[min(design.y)-xy_plot_lim, max(design.y)+xy_plot_lim]);
        ylabel(h,'{\it z} in µm');
        xlabel('{\it x} in mm');
        ylabel('{\it y} in mm');
        xline(off_center_y*1e3,'LineWidth',2,'Color','r');
        caxis(1e6*[0, max(measurement_structure.Z{i}(:,x_meas_cut_index))+z_plot_lim]);
        title('Experimental data: y cut')

        % export cut plots (if enabled)
        if userinput.saveplots
            exportgraphics(OneDCuts,strcat(userinput.exportpath,userinput.analysis_filename{i},'_1DCuts.png'),'BackgroundColor','w');
        end
        

        % Update progress GUI
        %------------------------------------------------------------------
        progressGUI.structure = i;
        progressGUI.pb_local_percentage = i/length(measurement_structure.Z);
        progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
        progressGUI.update();
        %------------------------------------------------------------------

    end

    disp('... Finished plotting 1D cuts');

end

