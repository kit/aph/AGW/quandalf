function [ROI, x_scale_coarse, y_scale_coarse] = coarsepositioning(measurement_raw, userinput,progressGUI)
%========================================================================== 
%                          COARSE POSITIONING
%==========================================================================
% A first step in the positioning of the design over the measurement
% The structure is identified by a height threshold deterined in the user
% input class
%--------------------------------------------------------------------------
% written for: QUANDALF V1.4
% last modified on: March 01, 2023
% last modified by: Jannis Weinacker
%--------------------------------------------------------------------------
% INPUTS:
% - design:             [single matrix] design data in meter
% - measurement_raw:    [cell of double matrices] measurement data in meter
% - userinput               [object] class object containing information
%                                    about design, exports etc.
% - progressGUI:        [class object] containing everything GUI-related
%--------------------------------------------------------------------------
% OUTPUTS:
% - ROI:                [cell of 4x1 double matrices] region of interest (corner + sides) 
% - x_scale_coarse:     [cell of double] scaling factor between the measurement and
%                           the design in x-direction
% - y_scale_coarse:     [cell of double] scaling factor between the measurement and
%                           the design in y-direction
%==========================================================================

    % updating progress GUI
    %---------------------------------------------------------------------%
    progressGUI.tool_main           = 'alignment';
    progressGUI.tool_sub            = 'coarse_pos';
    progressGUI.status              = 'performing coarse positioning';
    progressGUI.structure           = 0;
    progressGUI.n_structures        = length(measurement_raw.Z);
    progressGUI.pb_local_percentage = 0;
    pb_global_nfun_initial          = 6;
    progressGUI.pb_global_nfun      = pb_global_nfun_initial;
    progressGUI.update();
    %---------------------------------------------------------------------%

    ROI = cell(length(measurement_raw.Z),1);
    x_scale_coarse = cell(length(measurement_raw.Z),1);
    y_scale_coarse = cell(length(measurement_raw.Z),1);

    for i = 1:length(measurement_raw.Z)

        detection_threshold = findetectionthreshold(measurement_raw.Z{i}); % height in meters to detect a sructure

        % define pixel number arrays in x and y direction
        x = 1:size(measurement_raw.Z{i},2);
        y = 1:size(measurement_raw.Z{i},1);

        % Make meshgrid in pixel coordinates
        [X, Y] = meshgrid(x,y);

        % Make all coordinates to 0 that correspond to a smaller value than the threshold
        X = X .* (measurement_raw.Z{i} > detection_threshold);
        Y = Y .* (measurement_raw.Z{i} > detection_threshold);

        % Calculate the mean coordinates of the recognized structure in x and y
        % direction; this is the centre of the structure
        x_cog = round(sum(sum(X)) / length(find(X)));
        y_cog = round(sum(sum(Y)) / length(find(Y)));

        % calculate target size of ROI in meters
        design_size_x = userinput.designsizeX;
        design_size_y = userinput.designsizeY;

        % Calculate the size of one pixel in x and y direction in meters
        measurement_x_res = mean(diff(measurement_raw.x{i}));  % m per px
        measurement_y_res = mean(diff(measurement_raw.y{i}));  % m per px

        % Calculate the lateral sizes of the ROI in pixels
        ROI_targetsize_x_px = design_size_x / measurement_x_res;  % in px
        ROI_targetsize_y_px = design_size_y / measurement_y_res;  % in px

        % Definition of the ROI based on the measures calculated before
        corner_x = x_cog - round(ROI_targetsize_x_px/2); % x-coordinate of the upper left corner
        corner_y = y_cog - round(ROI_targetsize_y_px/2); % y-coordinate of the upper left corner

        ROI{i} = round([corner_x, corner_y, ROI_targetsize_x_px, ROI_targetsize_y_px]);

        %% Define parameter for size determination in the centre of the stucture
        % Here a first guess for the latheral scaling is determined for
        % later optimization
        detectionrange = 60; % Number of lines - 1 to determine size
        size_x_collect = zeros(detectionrange + 1, 1);
        size_y_collect = zeros(detectionrange + 1, 1);

        % Determination of the width and height around the centre position
        for j = 0:detectionrange
            detectedpixels_x    = x(measurement_raw.Z{i}(y_cog - detectionrange/2 + j, :) > detection_threshold);
            detectedpixels_y    = y(measurement_raw.Z{i}(:, x_cog - detectionrange/2 + j) > detection_threshold);
            size_x_collect(j+1) = max( detectedpixels_x ) - min( detectedpixels_x );
            size_y_collect(j+1) = max( detectedpixels_y ) - min( detectedpixels_y );
        end

        % average over all widths and heights
        size_x_px = round( mean( size_x_collect ) );
        size_y_px = round( mean( size_y_collect ) );

        % Converting pixel occrdinates to meters
        size_x_SI = size_x_px * measurement_x_res;
        size_y_SI = size_y_px * measurement_y_res;

        % Calculate the scaling factors
        x_scale_coarse{i} = size_x_SI / design_size_x;
        y_scale_coarse{i} = size_y_SI / design_size_y;

        fprintf(1,'=== %s ===\n', userinput.analysis_filename{i});
        fprintf(1,'Calculated structure size = %1.1fµm x %1.1fµm.\n', size_x_SI * 1e6, size_y_SI * 1e6);
        fprintf(1,'Total mismatch to design size = %1.1fµm x %1.1fµm.\n', (design_size_x-size_x_SI) * 1e6, (design_size_y-size_y_SI) * 1e6);

        % Update progress GUI
        %------------------------------------------------------------------
        progressGUI.structure = i;
        progressGUI.pb_local_percentage = i/length(measurement_raw.Z);
        progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
        progressGUI.update();
        %------------------------------------------------------------------

    end
    
end


