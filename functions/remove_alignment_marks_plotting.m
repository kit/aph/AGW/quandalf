function [] = remove_alignment_marks_plotting(i,measurement_raw,Z_data_filtered,mask_structures_final,mask_alignment_marks_bw_final,userinput,progressGUI)   
%========================================================================== 
%                      REMOVE ALIGNMENT MARKS (PLOTTING)
%==========================================================================
% function to generate a control plot after the automated detection and
% removal of alignment marks and unwanted objects (dust, etc) on the
% substrate around the printed structure(s).
%--------------------------------------------------------------------------
% written for: QUANDALF V1.4
% last modified on: June 04, 2023
% last modified by: Sebastian Kalt
%--------------------------------------------------------------------------
% INPUTS:
% - i                  [integer] cell index
% - measurement_raw    [cell of single-matrices] offset- and tilt-corrected
%                                                confocal data
% - Z_data_filtered               [single matrix] filtered heigth profile
% - mask_structures_final         [logical matrix] mask (structures)
% - mask_alignment_marks_bw_final [logical matrix] mask (removed objects)
% - userinput          [object] class object containing information about
%                               design, exports etc.
% - progressGUI        [class object] containing everything GUI-related
%--------------------------------------------------------------------------
% FIGURE OUTPUTS:
% - filtering process control figure
%==========================================================================


    % open figure
    f1 = figure('Color','w','Position',progressGUI.figuresize_large,'Name','Alignment marker removal');
    tiledlayout(2,2, 'Padding', 'none', 'TileSpacing', 'compact')
    
    % subplot 01: initial heigth profile
    ax1 = nexttile;
    imagesc(measurement_raw.x{i}.*1e3,measurement_raw.y{i}.*1e3,measurement_raw.Z{i}.*1e3);
    axis 'image';
    xlabel('x / mm')
    ylabel('y / mm')
    z = colorbar;
    ylabel(z,'z / mm')
    title('initial measurement data')
    
    % subplot 02: filtered height profile (without alignment marks)
    ax2 = nexttile;
    imagesc(measurement_raw.x{i}.*1e3,measurement_raw.y{i}.*1e3,Z_data_filtered.*1e3);
    axis 'image';
    xlabel('x / mm')
    ylabel('y / mm')
    z = colorbar;
    ylabel(z,'z / mm')
    title('heigth profile alter removing unwanted objects')
    
    % subplot 03: mask covering the detected structures to keep
    ax3 = nexttile;
    imagesc(measurement_raw.x{i}.*1e3,measurement_raw.y{i}.*1e3,mask_structures_final);
    axis 'image';
    xlabel('x / mm')
    ylabel('y / mm')
    title('detected structure(s)')
    
    % subplot 04: mask covering the objects marked for removal
    ax4 = nexttile;
    imagesc(measurement_raw.x{i}.*1e3,measurement_raw.y{i}.*1e3,mask_alignment_marks_bw_final);
    axis 'image';
    xlabel('x / mm')
    ylabel('y / mm')
    title('objects marked for removal')

    % link the scaling of all four subplots
    linkaxes([ax1 ax2 ax3 ax4],'xy')
    
    % export the control figure (if enabled)
    if userinput.saveplots
        exportgraphics(f1,strcat(userinput.exportpath,userinput.analysis_filename{i},'_object_removal.png'),'BackgroundColor','w');
    end


    