function difference_choice = choosedifferences(difference, measurement_structure, userinput, progressGUI)
%========================================================================== 
%                       DIFFERENCE CHOICE   
%==========================================================================
% In this function the differences between the measurements and the design
% are plotted and the user can decide individualy, wheather a measurement
% should be considered or ignored for further investigation. This enables
% the possibility to ignore measurements with obviouse measurement errors.
%--------------------------------------------------------------------------
% written for: QUANDALF V1.4
% last modified on: June 06, 2023
% last modified by: S.Kalt
%--------------------------------------------------------------------------
% INPUTS:
% - difference:               [cell (single arrays)]  contains the
%                           differences to the design for each measurement
% - measurement_structure:    [cell (single arrays)]  measurement data
% - userinput:                [object] class object containing information
%                                    about design, exports etc.
% - progressGUI:              [class object] containing everything GUI-related
%-------------------------------------------------------------------------
% OUTPUTS:
% - difference_choice:        [logical array]  difference data for
%                               measurements to be used (value '1') or 
%                               unused (value '0')
%==========================================================================
    
    % update progress GUI
    %----------------------------------------------------------------------
    progressGUI.tool_main           = 'difference';
    progressGUI.tool_sub            = 'sel_eval_diff';
    progressGUI.status              = 'plotting difference data overview';
    progressGUI.structure           = 0;
    progressGUI.n_structures        = length(difference);
    progressGUI.pb_local_percentage = 0;
    pb_global_nfun_initial          = 16;
    progressGUI.pb_global_nfun      = pb_global_nfun_initial;
    progressGUI.update();
    %----------------------------------------------------------------------

    % Plotting all difference data in single figure
    for i = 1:length(difference)
        
        plotdifference_core(i, measurement_structure, difference, userinput, progressGUI);
        
        measurement_structure_temp.x = {measurement_structure.x{i}};
        measurement_structure_temp.y = {measurement_structure.y{i}};
        measurement_structure_temp.Z = {measurement_structure.Z{i}};
        
        % immitate user choice for evaluatedifference function, so that
        % only this measurement set is considered inside the function
        choice = zeros(length(difference),1);
        choice(i) = 1;
        
        
        fprintf(1,'===== %s (%i/%i) =====\n', userinput.analysis_filename{i}, i, length(difference));
        progressGUI.update_enabled = 0;
        [mean_difference, std_difference] = evaluatedifference(difference{i}, measurement_structure, choice, userinput, progressGUI, i);
        progressGUI.update_enabled = 1;
        numhighgradientpixels = couthighgradientpixels(difference{i}, 1e-6); %Number of pixels with a gradient higher than 1 µm
        fprintf(1, 'Number of high gradient pixels: %d \n\n', numhighgradientpixels);

        if userinput.saveplots
            fprintf(userinput.informationfileID(i), 'Mean deviation of structure:                %1.3f nm\n', mean_difference * 1e9);
            fprintf(userinput.informationfileID(i), 'Standard deviation of structure difference: %1.3f nm\n', std_difference * 1e9);
            fprintf(userinput.informationfileID(i), 'Number of high gradient pixels: %d\n', numhighgradientpixels);
        end


        % Update progress GUI
        %------------------------------------------------------------------
        progressGUI.structure = i;
        progressGUI.pb_local_percentage = i/(length(difference)+1);
        progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
        progressGUI.update();
        %------------------------------------------------------------------
        
    end
  
    %---------------------------------------------------------------------%
    %                        Data selection window                        %
    %---------------------------------------------------------------------%
    
    % Update progress GUI
    %------------------------------------------------------------------
    progressGUI.status = 'select difference data to continue with';
    progressGUI.status_message.FontColor = [1,0,0];
    progressGUI.update();
    %------------------------------------------------------------------
    
    % calculate and set required selection gui size
    buttonheight = 30;             
    uiheight = 60 + length(difference) * 25 + 10 + buttonheight; % Total height of the UI window in pixels
    selUI_size = progressGUI.guisize;

    if (progressGUI.guisize(2) - uiheight - 40) < 60
        selUI_size(2) = 60;
        uiheight = progressGUI.guisize(2) - 40 - 60; 
        selUI_size(4) = uiheight;
        pnl_scrollable = 1;
    else
        selUI_size(2) = progressGUI.guisize(2) - uiheight - 40;
        selUI_size(4) = uiheight;
        pnl_scrollable = 0;
    end

    pnl_size = [20 40+buttonheight selUI_size(3)-40 uiheight-60-buttonheight];

    % generare selection gui
    figurehandle_1 = uifigure('Position',selUI_size,'Name','Selection of difference data','Icon',progressGUI.guiicon);
    pnl = uipanel('Parent', figurehandle_1, 'Position', pnl_size);

    if pnl_scrollable
        pnl.Scrollable = 'on';
    end

    % Cell array of checkboxes
    cbx = cell(length(difference),1);
    
    % create a checkbox for each measurement data set
    for i = 1:length(difference)
        cbx{i} = uicheckbox('Parent', pnl, 'Position',[20 10+(i-1)*25 pnl_size(3)-60 15],'Text',userinput.analysis_filename{i}, 'Value', 1);
    end
    
    % Add the "Confirm difference data" button at the lower end of the UI
    % window
    btn = uibutton(figurehandle_1, 'state', 'Text', 'Confirm difference data', 'Value', false, 'Position', [(selUI_size(3)-150)/2 20 150 buttonheight]);
           

    % Wait until user presses "Confirm difference data" button
    waitfor(btn, 'Value');
    
    % Extract the marked checkboxes
    difference_choice = cellfun(@(x) x.Value, cbx);
    
    % Close the UI window
    close(figurehandle_1)


    % Update progress GUI
    %------------------------------------------------------------------
    progressGUI.pb_local_percentage = 1;
    progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
    progressGUI.status_message.FontColor = [0,0,0];
    progressGUI.status = 'difference data selected';
    progressGUI.update();
    %------------------------------------------------------------------
    
end