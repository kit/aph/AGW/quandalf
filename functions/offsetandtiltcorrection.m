function [Z_corrected] = offsetandtiltcorrection(measurement_raw,progressGUI)
%========================================================================== 
%                       OFFSET AND TILT CORRECTION
%==========================================================================
% function to subtract the offset from the confocal height profile and
% correct a sample tilt
%--------------------------------------------------------------------------
% written for: QUANDALF V1.2
% last modified on: February 22, 2023
% last modified by: Pascal Kiefer
%--------------------------------------------------------------------------
% INPUTS:
% - measurement_raw:      [cell of single-matrices] uncorrected confocal data
% - progressGUI:          [class object] containing everything GUI-related
%--------------------------------------------------------------------------
% OUTPUTS:
% - Z_corrected:          [cell of single-matrices]  corrected confocal data 
%==========================================================================

    %---------------------------------------------------------------------%
    % update progress GUI
    %---------------------------------------------------------------------%
    progressGUI.tool_main           = 'alignment';
    progressGUI.tool_sub            = 'offset_tilt';
    progressGUI.status              = 'substrate offset and tilt correction';
    progressGUI.structure           = 0;
    progressGUI.n_structures        = length(measurement_raw.Z);
    progressGUI.pb_local_percentage = 0;
    pb_global_nfun_initial          = 4;
    progressGUI.pb_global_nfun      = pb_global_nfun_initial;
    progressGUI.update();
    %---------------------------------------------------------------------%

    n_gap  = 10; % number of pixels from the edge of the image 
    n_band = 10; % width of the band in pixles around the sructure
    n_in   = n_gap + n_band;
    
    Z_corrected = cell(length(measurement_raw.Z),1);

    for i = 1:length(measurement_raw.Z)
        % calculate mask to determine glass surface
        [Ny, Nx] = size(measurement_raw.Z{i});
        outer = zeros(Ny, Nx);
        inner = zeros(Ny, Nx);
        outer(n_gap : Ny-n_gap, n_gap : Nx-n_gap) = 1;
        inner(n_in : Ny-n_in, n_in : Nx-n_in) = 1;
        mask     = outer - inner;
        mask(mask == 0) = NaN;

        % create temporary measurement_raw where there are no cell
        % arras but only the current array. With that, we dont have to
        % change "substractplane.m".
        measurement_raw_temp.x = measurement_raw.x{i};
        measurement_raw_temp.y = measurement_raw.y{i};
        measurement_raw_temp.Z = measurement_raw.Z{i};

        % substract offset
        Z_corrected{i} = subtractplane(measurement_raw_temp,mask);

        clear measurement_raw_temp

        % Update progress GUI
        %------------------------------------------------------------------
        progressGUI.structure = i;
        progressGUI.pb_local_percentage = i/length(measurement_raw.Z);
        progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
        progressGUI.update();
        %------------------------------------------------------------------

    end

end

