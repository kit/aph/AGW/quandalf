function measurement_corrected = subtractplane(measurement_raw, mask)
%========================================================================== 
%                           SUBTRACT PLANE
%==========================================================================
% function that subtracts a fitted plane from the confocal measurement data
% to correct for a sample tilt
%--------------------------------------------------------------------------
% written for: QUANDALF V1.0
% last modified on: February 06, 2023
% last modified by: Jannis Weinacker
%--------------------------------------------------------------------------
% INPUTS:
% - measurement_raw:         [single-matrix] tilted confocal data
% - mask:                    [matrix] containes "1" (fitted points) and
%                                     "NaN" (excluded points)
%-------------------------------------------------------------------------
% OUTPUTS:
% - measurement_corrected:   [single-matrix] leveled confocal data 
%==========================================================================

    Z = measurement_raw.Z .* mask;

    [X, Y] = meshgrid(measurement_raw.x, measurement_raw.y);

    x_NaN = X(:);
    y_NaN = Y(:);
    z_NaN = Z(:);

    x = x_NaN(not(isnan(z_NaN)));
    y = y_NaN(not(isnan(z_NaN)));
    z = z_NaN(not(isnan(z_NaN)));
    
    x = double(x);
    y = double(y);
    z = double(z);
    sf = fit([x, y], z, 'poly11');

    measurement_corrected = measurement_raw.Z - sf.p00 - sf.p10*X - sf.p01*Y;
    measurement_corrected = single(measurement_corrected);

end