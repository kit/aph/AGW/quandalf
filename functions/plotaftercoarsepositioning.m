function plotaftercoarsepositioning(design, measurement_raw, ROI, userinput,progressGUI)
    
     % updating progress GUI
    %---------------------------------------------------------------------%
    progressGUI.tool_main           = 'alignment';
    progressGUI.tool_sub            = 'coarse_pos';
    progressGUI.status              = 'plotting heigth profiles after coarse positioning';
    progressGUI.structure           = 0;
    progressGUI.n_structures        = length(measurement_raw.Z);
    progressGUI.pb_local_percentage = 0;
    pb_global_nfun_initial          = 8;
    progressGUI.pb_global_nfun      = pb_global_nfun_initial;
    progressGUI.update();
    %---------------------------------------------------------------------%

    if userinput.generalplots

        for i = 1:length(measurement_raw.Z)

            measurement_plot.x = imcrop(measurement_raw.x{i},[1 ROI{i}(1) 1 ROI{i}(3)]);
            measurement_plot.x = measurement_plot.x - mean(measurement_plot.x);
            measurement_plot.y = imcrop(measurement_raw.y{i},[1 ROI{i}(2) 1 ROI{i}(4)]);
            measurement_plot.y = measurement_plot.y - mean(measurement_plot.y);
            measurement_plot.Z = imcrop(measurement_raw.Z{i},ROI{i});

            ROIbxcorr = figure('Color','w','WindowStyle','normal','Position',progressGUI.figuresize_wide,'Name','Measurement data after coarse positioning');

            subplot(1,2,1)
            imagesc(design.x*1e3,design.y*1e3,design.Z*1e6);
            axis 'image';
            h = colorbar;
            h.FontSize = 12;
            ylabel(h,'{\it z} in µm');
            xlabel('{\it x} in mm');
            ylabel('{\it y} in mm');
            caxis([0,max(design.Z(:))*1e6]);
            title('Design data')

            subplot(1,2,2)
            imagesc(measurement_plot.x*1e3,measurement_plot.y*1e3,measurement_plot.Z*1e6);
            axis 'image';
            h = colorbar;
            h.FontSize = 12;
            ylabel(h,'{\it z} in µm');
            xlabel('{\it x} in mm');
            ylabel('{\it y} in mm');
            caxis([0,max(measurement_plot.Z(:))*1e6]);
            title('Measurement data after coarse positioning')

            if userinput.saveplots

                exportgraphics(ROIbxcorr,strcat(userinput.exportpath,userinput.analysis_filename{i},'_overviewplot_after_coarsepos.png'),'BackgroundColor','w');

            end

            % Update progress GUI
            %--------------------------------------------------------------
            progressGUI.structure = i;
            progressGUI.pb_local_percentage = i/length(measurement_raw.Z);
            progressGUI.pb_global_nfun = pb_global_nfun_initial + progressGUI.pb_local_percentage;
            progressGUI.update();
            %--------------------------------------------------------------

        end
            
    else
        
        fprintf(1,'Skipping plot after coarse positioning due to user decision.\n')
        
    end
    
end